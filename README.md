#  tilting-point-front-end

### Prerequisites

- [NodeJS](http://nodejs.org) version 6.3.1 or newer.
- [NVM](https://github.com/creationix/nvm) can be used for a better experience managing NodeJS releases.

### Get Started

Start playing with it using:
- `npm install` to install the dependencies
- `npm start -s` to start the app
