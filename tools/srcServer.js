import browserSync from 'browser-sync';
import historyApiFallback from 'connect-history-api-fallback';
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
const config =  require('../webpack-configs/webpack.config.dev');
import proxy from 'http-proxy-middleware';

const bundler = webpack(config);

const ProxyMiddleware = proxy(['/authentication', '/api/v1'],{
  target: 'http://localhost:3030',
  changeOrigin: true
});

browserSync({
  port: 3000,
  ui: {
    port: 3001
  },
  serveStatic: ['src/resources'],
  server: {
    baseDir: 'src/client',
    middleware: [
      ProxyMiddleware,
      historyApiFallback(),

      webpackDevMiddleware(bundler, {
        publicPath: config.output.publicPath,
        noInfo: false,
        quiet: false,
        stats: {
          assets: false,
          colors: true,
          version: false,
          hash: true,
          timings: false,
          chunks: true,
          chunkModules: false
        }
      }),
      webpackHotMiddleware(bundler)
    ]
  },
  files: ['src/client/*.html']
});
