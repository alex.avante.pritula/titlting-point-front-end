import browserSync from 'browser-sync';
import historyApiFallback from 'connect-history-api-fallback';
import {chalkProcessing} from './chalkConfig';

/* eslint-disable no-console */

console.log(chalkProcessing('Opening production build...'));
import proxy from 'http-proxy-middleware';

const ProxyMiddleware = proxy(['/authentication', '/api/v1'],{
  target: 'http://34.228.56.40:3030',
  changeOrigin: true
});

// Run Browsersync
browserSync({
  port: 3000,
  ui: {
    port: 3001
  },
  server: {
    baseDir: 'dist',
    middleware: [ProxyMiddleware]
  },

  files: ['src/*.html'],

  middleware: [historyApiFallback()]
});
