import {Record} from 'immutable';
import FormData from 'form-data';

import {
  CREATE_DEFAULT_ITEM,
  CHANGE_SELECTED_ITEM_TYPE,

  UPLOAD_TEMP_ITEM_FILE,
  CHANGE_DATA_ITEM_FIELD_VALUE,

  GET_ITEM_DESCRIPTION_FAIL,
  GET_ITEM_DESCRIPTION_START,
  GET_ITEM_DESCRIPTION_SUCCESS,

  HANDLE_OPEN_ITEM_DIALOG,
  HANDLE_CLOSE_ITEM_DIALOG,

  ADD_LINK_TO_LIST_OF_LINKS,
  REMOVE_LINK_FROM_LIST_OF_LINKS,
  UPDATE_LINK_FROM_LIST_OF_LINKS,

  UPLOAD_TEMP_NEWS_ITEM_FILE,

  CREATE_DEFAULT_LINK_START,
  CREATE_DEFAULT_LINK_SUCCESS,
  CREATE_DEFAULT_LINK_FAIL,

  CREATE_NEWS_START,
  CREATE_NEWS_SUCCESS,
  CREATE_NEWS_FAIL,

  REMOVE_ITEM_START,
  REMOVE_ITEM_SUCCESS,
  REMOVE_ITEM_FAIL,

  CREATE_LIST_OF_LINKS_START,
  CREATE_LIST_OF_LINKS_SUCCESS,
  CREATE_LIST_OF_LINKS_FAIL,

  SET_EDITING_STATUS,
  SET_VALIDATION_ERRORS,

  UPDATE_DEFAULT_LINK_START,
  UPDATE_DEFAULT_LINK_SUCCESS,
  UPDATE_DEFAULT_LINK_FAIL,

  UPDATE_NEWS_START,
  UPDATE_NEWS_SUCCESS,
  UPDATE_NEWS_FAIL,
 
  UPDATE_LIST_OF_LINKS_START,
  UPDATE_LIST_OF_LINKS_SUCCESS,
  UPDATE_LIST_OF_LINKS_FAIL

} from '../constants/Dashboard';

import {
  USER_ROLES,
  DASHBOARD_BLOCK_ITEM_TYPES
} from '../constants/General';

const ITEM_TYPES_NAMES = Object.keys(DASHBOARD_BLOCK_ITEM_TYPES);

const defaultVideoLink = new Record({
  url: '',
  name: '',
  linkDescription: '',
  validationErrors: [],  
  file: new FormData(),
});

const simpleLink = new Record({
  url: '',
  title: ''
});

const existSimpleLink = new Record({
  id: null,
  url: '',
  title: ''
});

const defaultListOfLinks = new Record({
  name: '',
  links: [],
  validationErrors: []
});

const defaultFileLink = new Record({
  url: '',
  name: '',
  validationErrors: [],
  file: new FormData()
});

const defaultNews = new Record({
  name: '',
  newsDescription: '',
  validationErrors: [],
  file: new FormData()
});

const existFileLink = new Record({
  id: null,
  url: '',
  name: '',
  createdAt: null,
  ownerName: '',
  ownerRole: '',
  validationErrors: [],
  file: new FormData()
});

const existListOfLinks = new Record({
  url: '',
  name: '',
  links: [],
  ownerName: '',
  ownerRole: '',
  createdAt: null,
  removedLinks: [],
  editedLinks: [],
  validationErrors: [],
});

const existVideoLink = new Record({
  id: null,
  url: '',
  name: '',
  ownerName: '',
  ownerRole: '',
  createdAt: null,
  linkDescription: '',
  validationErrors: [],
  file: new FormData()
});

const existNews = new Record({
  id: null,
  name: '',
  createdAt: null,
  newsDescription: '',
  firstImageId: null,
  firstImageUrl: '',
  lastImageId: null,
  lastImageUrl: '',
  ownerName: '',
  ownerRole: '',
  validationErrors: [],
  file: new FormData()
});

const InitialState = Record({
  error: null,
  itemIndex: null,
  blockIndex: null,
  isEditing: false,
  isFetching: false,
  selectedItemId: null,
  selectedBlockId: null,
  selectedItemData: null,
  selectedItemTypeId: null
});

const defaultCreator = {
   'NEWS_BLOCK': ()=>{ return new defaultNews(); },
   'NEW_DOCUMENT': ()=> { return new defaultFileLink(); },
   'TEXTAREA': ()=>{ return new defaultNews(); },
   'LIST_OF_LINKS': ()=>{ return new defaultListOfLinks(); },
   'VIDEO': ()=>{ return new defaultVideoLink(); },
   'IMAGE_LINK': ()=> { return new defaultFileLink(); }
};

function getUserRoleTitle(roleId){
  let role = USER_ROLES.find((role)=>{
    return (role.id === roleId);
  });
  return role['title'];
}
const existCreator = {
  
   'NEWS_BLOCK': (res)=>{ 
      const {owner, name, createdAt, news} = res;
      const {id, description, newsImages} = news;
      const firstImage = newsImages[0];
      const lastImage = newsImages[1];

      let data = {
        id,
        name, 
        createdAt, 
        ownerName: owner['name'],
        ownerRole: getUserRoleTitle(owner['role'].id),
        newsDescription: description
      };
      if(firstImage){
         data.firstImageId = firstImage.id;
         data.firstImageUrl = firstImage.imageUrl;
      }

      if(lastImage){
         data.lastImageId = lastImage.id;
         data.lastImageUrl = lastImage.imageUrl;
      }
      return new existNews(data);
   },

   'NEW_DOCUMENT': (res)=> { 
     const {owner, createdAt, links, name} = res;
     const {id, url} = links[0];
     let data = {
       id, 
       createdAt, 
       name, 
       url,
       ownerName: owner['name'],
       ownerRole: getUserRoleTitle(owner['role'].id)
    };
     return new existFileLink(data);
   },

   'TEXTAREA': (res)=>{ 
      const {owner, name, createdAt, news} = res;
      const {id, description, newsImages} = news;
      const firstImage = newsImages[0];
      const lastImage = newsImages[1];

      let data = {
        id,
        name, 
        createdAt, 
        ownerName: owner['name'],
        ownerRole: getUserRoleTitle(owner['role'].id),
        newsDescription: description
      };

      if(firstImage){
         data.firstImageId = firstImage.id;
         data.firstImageUrl = firstImage.imageUrl;
      }

      if(lastImage){
         data.lastImageId = lastImage.id;
         data.lastImageUrl = lastImage.imageUrl;
      }

      return new existNews(data);
   },

   'LIST_OF_LINKS': (res)=>{
      const {owner, name, createdAt, links} = res;
      let data = {
        name,
        createdAt,
        ownerName: owner['name'],
        ownerRole: getUserRoleTitle(owner['role'].id),
        links: links.map((link)=>{
          const {id, title, url} = link;
          return new existSimpleLink({id, title, url});
        })
      };
      return new existListOfLinks(data);
    },

   'VIDEO': (res)=>{ 
      const {owner, createdAt, links, name} = res;
      const {id, url, description} = links[0];
      let data = {
        id,
        createdAt, 
        name, 
        url, 
        ownerName: owner['name'],
        ownerRole: getUserRoleTitle(owner['role'].id),
        linkDescription: description
      };
      return new existVideoLink(data);
    },

   'IMAGE_LINK': (res)=> { 
     const {owner,createdAt, links, name} = res;
     const {id, url} = links[0];
     let data = {
       id, 
       createdAt, 
       name, 
       url,
       ownerRole: getUserRoleTitle(owner['role'].id),
       ownerName: owner['name'],
      };
     return new existFileLink(data);
   }
};

const initialState = new InitialState;

export default function(state = initialState, action) {

  const {type} = action;
  switch (type) {

    case UPDATE_LIST_OF_LINKS_START: {
      return state.withMutations((ctx) => {
          ctx.set('isFetching', true)
             .set('error', null);      
      });
    }

    case UPDATE_LIST_OF_LINKS_SUCCESS: {
      return state.withMutations((ctx) => {
          ctx.set('isFetching', false);      
      });
    }

    case UPDATE_LIST_OF_LINKS_FAIL: {
      return state.withMutations((ctx) => {
          const {error} = action;
          ctx.set('isFetching', false)
             .set('error', error);      
      });
    }

    case SET_VALIDATION_ERRORS: {
     return state.withMutations((ctx) => {
        const {errors} = action;
        ctx.setIn(['selectedItemData', 'validationErrors'], errors);
     });
    }

    case UPDATE_NEWS_START: {
     return state.withMutations((ctx) => {
        ctx.set('isFetching', true)
           .set('error', null);
     });
    }

    case UPDATE_NEWS_SUCCESS: {
     return state.withMutations((ctx) => {
        ctx.set('isFetching', false);
     });
    }

    case UPDATE_NEWS_FAIL: {
     return state.withMutations((ctx) => {
        const {error} = action;
        ctx.set('isFetching', false)
           .set('error', error);
     });
    }

    case UPDATE_DEFAULT_LINK_START: {
     return state.withMutations((ctx) => {
        ctx.set('isFetching', true)
           .set('error', null);
     });
    }

    case UPDATE_DEFAULT_LINK_SUCCESS: {
     return state.withMutations((ctx) => {
        ctx.set('isFetching', false);
     });
    }

    case UPDATE_DEFAULT_LINK_FAIL: {
     return state.withMutations((ctx) => {
        const {error} = action;
        ctx.set('isFetching', false)
           .set('error', error);
     });
    }
    
    case SET_EDITING_STATUS: {
     return state.withMutations((ctx) => {
       const {isEditing} = action;
       ctx.set('isEditing', isEditing);
     });
    }

    case CREATE_LIST_OF_LINKS_START: {
     return state.withMutations((ctx) => {
        ctx.set('isFetching', true)
            .set('error', null);
     });
    }

    case CREATE_LIST_OF_LINKS_SUCCESS: {
     return state.withMutations((ctx) => {
        ctx.set('isFetching', false);
     });
    }

    case CREATE_LIST_OF_LINKS_FAIL: {
     return state.withMutations((ctx) => {
       const {error} = action;
       ctx.set('isFetching', false)
          .set('error', error);
     });
    }

    case UPLOAD_TEMP_NEWS_ITEM_FILE: {
     return state.withMutations((ctx) => {
        const {file, position} = action;
        const files = ctx.getIn(['selectedItemData', 'file']);
        const id = ctx.getIn(['selectedItemData', 'id']);
        const first = files.get('files[0]');
        const last =  files.get('files[1]');
        let formData = new FormData();       

        if(position === 0) {
          formData.append('files[0]', file);
          formData.append('files[1]', last);
          if(id) { ctx.setIn(['selectedItemData', 'firstImageUrl'], ''); }
        } else {
          formData.append('files[0]', first);
          formData.append('files[1]', file);
          if(id) { ctx.setIn(['selectedItemData', 'lastImageUrl'], ''); }
        }

        ctx.setIn(['selectedItemData', 'file'], formData);
     }); 
    }

    case REMOVE_ITEM_START: {
      return state.withMutations((ctx) => {
        ctx.set('isFetching', true)
            .set('error', null);
      });
    }

    case REMOVE_ITEM_SUCCESS: {
      return state.withMutations((ctx) => {
        ctx.set('isFetching', false)
            .set('selectedBlockId', null)
            .set('selectedItemData', null)
            .set('selectedItemTypeId', null)
            .set('selectedItemId', null);
      });
    }

    case REMOVE_ITEM_FAIL: {
      return state.withMutations((ctx) => {
        const {error} = action;
        ctx.set('isFetching', false)
            .set('error', error);
      });
    }

    case CREATE_NEWS_START: {
     return state.withMutations((ctx) => {
         ctx.set('isFetching', true)
            .set('error', null);
     }); 
    }

    case CREATE_NEWS_SUCCESS: {
     return state.withMutations((ctx) => {
         ctx.set('isFetching', false);
     }); 
    }

    case CREATE_NEWS_FAIL: {
     return state.withMutations((ctx) => {
       const {error} = action;
       ctx.set('isFetching', false)
          .set('error', error);
     }); 
    } 

    case CREATE_DEFAULT_LINK_START: {
     return state.withMutations((ctx) => {
         ctx.set('isFetching', true)
            .set('error', null);
     }); 
    }

    case CREATE_DEFAULT_LINK_SUCCESS: {
     return state.withMutations((ctx) => {
         ctx.set('isFetching', false);
     }); 
    }

    case CREATE_DEFAULT_LINK_FAIL: {
     return state.withMutations((ctx) => {
       const {error} = action;
       ctx.set('isFetching', false)
          .set('error', error);
     }); 
    } 

    case ADD_LINK_TO_LIST_OF_LINKS: {
      return state.withMutations((ctx) => {   
        let link = new simpleLink();
        let linkArray = [...ctx.getIn(['selectedItemData', 'links'])];
        linkArray.push(link);
        ctx.setIn(['selectedItemData', 'links'], linkArray);  
     });
    }

    case REMOVE_LINK_FROM_LIST_OF_LINKS: {
      return state.withMutations((ctx) => {
        const {index} = action;
        const existId = ctx.get('selectedItemId');
        let linkArray = [...ctx.getIn(['selectedItemData', 'links'])];

        if(existId) {
          let linkId = linkArray[index].get('id');
          let removedLinks = [...ctx.getIn(['selectedItemData', 'removedLinks'])];
          let editedLinks = [...ctx.getIn(['selectedItemData', 'editedLinks'])];
          let isExistLink = removedLinks.findIndex((id)=>{ return (linkId === id); });
          let isAlreadyEdited = editedLinks.findIndex((id)=>{ return (linkId === id); });

          if(isExistLink === -1) {
            removedLinks.push(linkId);
            ctx.setIn(['selectedItemData', 'removedLinks'], removedLinks);  

            if(isAlreadyEdited !== -1) {
              editedLinks.splice(isAlreadyEdited, 1);
              ctx.setIn(['selectedItemData', 'editedLinks'], editedLinks);  
            }
          }

        }
        linkArray.splice(index, 1);
        ctx.setIn(['selectedItemData', 'links'], linkArray);  
     });
    }

    case UPDATE_LINK_FROM_LIST_OF_LINKS: {
      return state.withMutations((ctx) => {
        const {index, field, value} = action;
        const existId = ctx.get('selectedItemId');
        const links = [...ctx.getIn(['selectedItemData', 'links'])];
        let selectedLink = links[index];
        const existLinkId = selectedLink.get('id');
        let linkType = (existLinkId ? existSimpleLink : simpleLink);
        let link = new linkType();

        if(field === 'title'){
          link = new linkType({
            id: existLinkId,
            url: selectedLink.get('url'),
            title: value
          });
        }

        if(field === 'url'){
          link = new linkType({
            id: existLinkId,
            title: selectedLink.get('title'),
            url: value
          });
        }

        links[index] = link;
        if (existId) {
           let editedLinks = [...ctx.getIn(['selectedItemData', 'editedLinks'])];
           const linkId = selectedLink.get('id');
           let isExistLink = editedLinks.findIndex((id)=>{ return (linkId === id); });
           if(isExistLink === -1) { 
             editedLinks.push(linkId); 
             ctx.setIn(['selectedItemData','editedLinks'], editedLinks);
           }
        }
        ctx.setIn(['selectedItemData', 'links'], links);  
     });
    }

    case CHANGE_DATA_ITEM_FIELD_VALUE: {
      return state.withMutations((ctx) => {
        const {field, value} =  action;
        ctx.setIn(['selectedItemData', field], value);
        if (field === 'url') {
            let formData = new FormData();
            ctx.setIn(['selectedItemData','file'], formData);
        }

        let validationErrors =  ctx.getIn(['selectedItemData', 'validationErrors']);
        let hasErrorIndex = validationErrors.findIndex((error)=> { return (error === field);} );
        if (hasErrorIndex !== -1){
          let newValidationErrors = [...validationErrors];
          newValidationErrors.splice(hasErrorIndex, 1);
          ctx.setIn(['selectedItemData','validationErrors'], newValidationErrors);
        }
     });
    }

    case HANDLE_OPEN_ITEM_DIALOG: {
      return state.withMutations((ctx) => {
        const {
          itemId, 
          blockId, 
          itemTypeId, 
          itemIndex, 
          blockIndex
        } = action;

         ctx.set('isFetching', false)
            .set('selectedBlockId', blockId)
            .set('selectedItemId', itemId)
            .set('selectedItemTypeId', itemTypeId)
            .set('itemIndex', itemIndex)
            .set('blockIndex',blockIndex)
            .set('error', null);
      });
    }

    case HANDLE_CLOSE_ITEM_DIALOG: {
      return state.withMutations((ctx) => {
         ctx.set('isFetching', false)
            .set('selectedBlockId', null)
            .set('selectedItemData', null)
            .set('selectedItemTypeId', null)
            .set('selectedItemId', null)
            .set('itemIndex', null)
            .set('blockIndex',null)
            .set('error', null);
      });
    }

    case UPLOAD_TEMP_ITEM_FILE: {
      return state.withMutations((ctx) => {
          const {file} = action;
          let formData = new FormData();
          formData.append('files', file);
          ctx.setIn(['selectedItemData', 'file'], formData)
             .setIn(['selectedItemData', 'url'], '') ;
     });
    }

    case GET_ITEM_DESCRIPTION_FAIL: {
      return state.withMutations((ctx) => {
         const {error} = action;
         ctx.set('isFetching', false)
            .set('error', error);
      });
    }

    case CHANGE_SELECTED_ITEM_TYPE: {
      return state.withMutations((ctx) => {
         const {typeId} = action;
         let type = ITEM_TYPES_NAMES.find((key)=>{
             let { id }= DASHBOARD_BLOCK_ITEM_TYPES[key];
             return (typeId === id);
         });

         ctx.set('selectedItemData', defaultCreator[type]())
            .set('selectedItemTypeId', typeId)
            .set('isFetching', false)
            .set('selectedItemId', null);
      });
    }

    case GET_ITEM_DESCRIPTION_START: {
      return state.withMutations((ctx) => {
         ctx.set('isFetching', true)
            .set('error', null);
      });
    }

    case GET_ITEM_DESCRIPTION_SUCCESS: {
      return state.withMutations((ctx) => {
        const {data} = action;
        const {id, itemTypeId} = data;
        let type = ITEM_TYPES_NAMES.find((key)=>{
           let { id } = DASHBOARD_BLOCK_ITEM_TYPES[key];
           return (itemTypeId === id);
        });

        ctx.set('selectedItemData', existCreator[type](data))
           .set('isFetching', false)
           .set('isEditing', false)
           .set('selectedItemId', id);
       });
    }

    case CREATE_DEFAULT_ITEM: {
      return state.withMutations((ctx) => {
          const {selectedItemTypeId} = ctx;
          let type = ITEM_TYPES_NAMES.find((key)=>{
             let {id} = DASHBOARD_BLOCK_ITEM_TYPES[key];
             return (selectedItemTypeId === id);
          });

          ctx.set('selectedItemData', defaultCreator[type]())
             .set('isFetching', false)
             .set('isEditing', true)
             .set('selectedItemId', null);
      });
    }

    default: { return state; }
  }
}