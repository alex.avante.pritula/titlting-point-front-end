import {Record} from 'immutable';

import {
  USER_MANAGEMENT_GET_USERS_REQUEST,
  USER_MANAGEMENT_GET_USERS_RECEIVE,
  USER_MANAGEMENT_GET_USERS_FAIL,
  USER_MANAGEMEN_INVITE_DIALOG_TOGGLE
} from '../../actions/admin/users/userManagementActions';

const InitialState = Record({
  users: [],
  count: 0,
  error: null,
  isFetching: false,
  isInviteDialogOpened: false
});

const initialState = new InitialState;

function userManagement(state = initialState, action) {
  const {type} = action;

  switch(type) {

    case USER_MANAGEMENT_GET_USERS_REQUEST: {
      return state.withMutations((ctx) => {
        ctx
          .set('isFetching', true)
          .set('error', null);
      });
    }

    case USER_MANAGEMENT_GET_USERS_RECEIVE: {
      return state.withMutations((ctx) => {
        const {users, count} = action;
       /* const oldUsers = [...ctx.users];
        let newUsers = oldUsers.concat(users);

        console.warn(newUsers, count);*/
        
        ctx.set('isFetching', false)
          .set('users', users)
          .set('count', count);
      });
    }

    case USER_MANAGEMENT_GET_USERS_FAIL: {
      return state.withMutations((ctx) => {
        const {error} = action;
        ctx
          .set('isFetching', false)
          .set('error', error);
      });
    }

    case USER_MANAGEMEN_INVITE_DIALOG_TOGGLE: {
      return state.withMutations((ctx) => {
        const {isOpened} = action;
        ctx.set('isInviteDialogOpened', isOpened);
      });
    }

    default: { return state; }
  }
}

export default userManagement;
