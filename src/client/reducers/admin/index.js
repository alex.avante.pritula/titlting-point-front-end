import {combineReducers} from "redux";
import usersManagement from './usersManagementReducer';

const adminReducer = combineReducers({
  usersManagement
});

export default adminReducer;
