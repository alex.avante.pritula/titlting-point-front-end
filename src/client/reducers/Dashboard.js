import {Record} from 'immutable';

import {
  GET_DAHBOARD_SUCCESS,
  GET_DAHBOARD_FAIL,
  GET_DAHBOARD_START,

  REMOVE_BLOCK_START,
  REMOVE_BLOCK_FAIL,
  REMOVE_BLOCK_SUCCESS,

  RENAME_BLOCK_START,
  RENAME_BLOCK_FAIL,
  RENAME_BLOCK_SUCCESS,

  MOVE_BLOCK,
  MOVE_ITEM,
  START_DRAGGING_BLOCK,
  START_DRAGGING_ITEM,

  CHANGE_BLOCK_POSITION_START,
  CHANGE_BLOCK_POSITION_SUCCESS,
  CHANGE_BLOCK_POSITION_FAIL,

  UPDATE_BLOCK_TITLE,
  SET_DASHBOARD_QUERY_TEXT
} from '../constants/Dashboard';

const InitialState = Record({
  id: null,
  text: '',
  blocks: [],
  name: null,
  error: null,
  dragBlocks: [],
  blocksCount: 0,
  isFetching: false,
  isDragging: false,
  dragItemPosition: null,
  dragBlockPosition: null
});

const initialState = new InitialState;

export default function(state = initialState, action) {

  const {type} = action;

  switch (type) {
    case SET_DASHBOARD_QUERY_TEXT: {
      return state.withMutations((ctx) => {
        const {text} = action;
        ctx.set('text', text);
      });
    }

    case UPDATE_BLOCK_TITLE: {
      return state.withMutations((ctx) => {
         const {blockIndex, name} = action;
         let newBlocks = [...ctx.get('blocks')];
         let block = newBlocks[blockIndex];
         let newBlock = Object.assign({}, block, {name});
         newBlocks[blockIndex] = newBlock;
         ctx.set('blocks', newBlocks);
      });
    }

    case CHANGE_BLOCK_POSITION_START: {
      return state.withMutations((ctx) => {
         ctx.set('isDragging', false)
            .set('isFetching', true);
      });
    }

    case CHANGE_BLOCK_POSITION_SUCCESS: {
       return state.withMutations((ctx) => {
         ctx.set('isDragging', false)
            .set('isFetching', false);
      });
    }

    case CHANGE_BLOCK_POSITION_FAIL: {
       return state.withMutations((ctx) => {
         const {error} = action;
         const {dragBlocks} = ctx;
         const oldBlocks = [...dragBlocks];
         ctx.set('isFetching', false)
            .set('dragBlockPosition', null)
            .set('dragBlocks', [])
            .set('blocks', oldBlocks)
            .set('error', error);
      });
    }

    case START_DRAGGING_BLOCK: {
       return state.withMutations((ctx) => {
         const {position} = action;
         let blocks = [...ctx.blocks];
         ctx.set('isDragging', true)
            .set('dragBlocks', blocks)
            .set('dragItemPosition', null)
            .set('dragBlockPosition', position);
      });
    }

    case START_DRAGGING_ITEM: {
      return state.withMutations((ctx)=>{
         const {position} = action;
         let blocks = [...ctx.blocks];
         ctx.set('isDragging', true)
            .set('dragBlocks', blocks)
            .set('dragItemPosition', position)
            .set('dragBlockPosition', null);
      });
    }

    case RENAME_BLOCK_START: {
       return state.withMutations((ctx) => {
        ctx.set('isFetching', true)
           .set('error', null);
      });
    }

   case RENAME_BLOCK_SUCCESS: {
       return state.withMutations((ctx) => {
        ctx.set('isFetching', false);
      });
    }

   case RENAME_BLOCK_FAIL: {
       return state.withMutations((ctx) => {
        const {error} = action;
        ctx.set('isFetching', false)
           .set('error', error);
      });
    }

    case REMOVE_BLOCK_START: {
       return state.withMutations((ctx) => {
        ctx.set('isFetching', true)
           .set('error', null);
      });
    }

   case REMOVE_BLOCK_SUCCESS: {
       return state.withMutations((ctx) => {
        ctx.set('isFetching', false);
      });
    }

   case REMOVE_BLOCK_FAIL: {
       return state.withMutations((ctx) => {
        const {error} = action;
        ctx.set('isFetching', false)
           .set('error', error);
      });
    }

    case GET_DAHBOARD_START: {
       return state.withMutations((ctx) => {
        ctx.set('isFetching', true)
           .set('isDragging', false)
           .set('dragBlocks', [])
           .set('dragItemPosition', null)
           .set('dragBlockPosition', null)
           .set('error', null);
      });
    }

    case GET_DAHBOARD_SUCCESS: {
      return state.withMutations((ctx) => {
        const {dashboard, reload} = action;
        const {id, blocks, title, blocksCount} = dashboard;
        let newBlocks = [...blocks];

        if(!reload) {
          const oldBlocks = [...ctx.blocks];
          newBlocks = oldBlocks.concat(newBlocks);
        }

        ctx.set('isFetching', false)
           .set('blocks', newBlocks)
           .set('blocksCount', blocksCount)
           .set('name', title)
           .set('id', id);
      });
    }

    case GET_DAHBOARD_FAIL: {
      return state.withMutations((ctx) => {
        const {error} = action;
        ctx.set('isFetching', false)
           .set('error', error);
      });
    }

    case MOVE_BLOCK: {
     return state.withMutations((ctx) => {
        const {dragIndex, hoverIndex} = action;
        let blocks = [...state.blocks];
        let tempBlock = blocks[hoverIndex];
        blocks[hoverIndex] = blocks[dragIndex];
        blocks[dragIndex] = tempBlock;
        ctx.set('blocks', blocks);
      });
    }

    case MOVE_ITEM: {
      return state.withMutations((ctx) => {
        const {dragIndex, hoverIndex, blockIndex} = action;
        let blocks = [...state.blocks];
        let tempBlock = blocks[blockIndex];
        let tempItems = [...tempBlock.items];
        let tempItem = tempItems[hoverIndex];
        tempItems[hoverIndex] = tempItems[dragIndex];
        tempItems[dragIndex] = tempItem;
        blocks[blockIndex] = Object.assign({}, tempBlock, {items: tempItems});
        ctx.set('blocks',blocks);
      });
    }

    default: { return state; }
  }
}

