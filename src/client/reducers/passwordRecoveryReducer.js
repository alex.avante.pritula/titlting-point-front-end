import {Record} from 'immutable';

import {
  ACTION_PASSWORD_RECOVERY_REQUEST,
  ACTION_PASSWORD_RECOVERY_RECEIVE,
  ACTION_PASSWORD_RECOVERY_FAIL
} from '../actions/User/passwordRecoveryActions';

import {
  ACTION_PASSWORD_CHANGE_REQUEST,
  ACTION_PASSWORD_CHANGE_RECEIVE,
  ACTION_PASSWORD_CHANGE_FAIL
} from '../actions/User/passwordChangeActions';

const InitialState = Record({
  error: null,
  isFetching: false,
  isEmailSended: false,
  isRecoverSucceed: false,
  statusMessage: 'Please enter e-mail used for registration Instructions will be sent there.'
});

const initialState = new InitialState;

function recoveryPassword(state = initialState, action) {
  const {type} = action;

  switch(type) {

    case ACTION_PASSWORD_RECOVERY_REQUEST: {
      return state.withMutations((ctx) => {
        ctx
          .set('isFetching', true)
          .set('error', null);
      });
    }

    case ACTION_PASSWORD_RECOVERY_RECEIVE: {
      return state.withMutations((ctx) => {
        ctx
          .set('isFetching', false)
          .set('isEmailSended', true)
          .set('statusMessage', 'Almost done. You will receive email with further instructions.');
      });
    }

    case ACTION_PASSWORD_RECOVERY_FAIL: {
      return state.withMutations((ctx) => {
        const {error} = action;
        ctx
          .set('isFetching', false)
          .set('error', error);
      });
    }

    case ACTION_PASSWORD_CHANGE_REQUEST:
      return state.withMutations((ctx) => {
        ctx.set('isFetching', true);
      });
    case ACTION_PASSWORD_CHANGE_RECEIVE:
      return state.withMutations((ctx) => {
        ctx
          .set('isFetching', false)
          .set('isRecoverSucceed', true)
          .set('statusMessage', '');
      });
    case ACTION_PASSWORD_CHANGE_FAIL:
      return state.withMutations((ctx) => {
        const { error } = action;
        ctx
          .set('isFetching', false)
          .set('error', error);
      });

    default: {
      return state;
    }
  }
}

export default recoveryPassword;
