import {Record} from 'immutable';

import {
  DEPARTMENTS_GET_LIST_REQUEST,
  DEPARTMENTS_GET_LIST_RECEIVE,
  DEPARTMENTS_GET_LIST_FAIL
} from '../actions/admin/departmentActions';

const InitialState = Record({
  departments: [],
  count: 0,
  error: null,
  isFetching: false
});

const initialState = new InitialState;

function departments(state = initialState, action) {
  const {type} = action;

  switch(type) {

    case DEPARTMENTS_GET_LIST_REQUEST: {
      return state.withMutations((ctx) => {
        ctx
          .set('isFetching', true)
          .set('error', null);
      });
    }

    case DEPARTMENTS_GET_LIST_RECEIVE: {
      return state.withMutations((ctx) => {
        const {
          departments,
          count
        } = action;

        ctx
          .set('isFetching', false)
          .set('departments', departments)
          .set('count', count);
      });
    }

    case DEPARTMENTS_GET_LIST_FAIL: {
      return state.withMutations((ctx) => {
        const {error} = action;
        ctx
          .set('isFetching', false)
          .set('error', error);
      });
    }

    default: {
      return state;
    }
  }
}

export default departments;
