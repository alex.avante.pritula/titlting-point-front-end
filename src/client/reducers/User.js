import {Record} from 'immutable';

import {
    LOG_IN_FAIL,
    LOG_IN_START,
    LOG_IN_SUCCESS,

    LOG_OUT_FAIL,
    LOG_OUT_START,
    LOG_OUT_SUCCESS,

    GET_PROFILE_FAIL,
    GET_PROFILE_START,
    GET_PROFILE_SUCCESS,

    CHECK_AUTHORIZATION,

    USER_PROFILE_GET_BY_CODE_REQUEST,
    USER_PROFILE_GET_BY_CODE_SUCCESS,
    USER_PROFILE_GET_BY_CODE_FAIL,

    USER_PROFILE_REGISTRATION_REQUEST,
    USER_PROFILE_REGISTRATION_SUCCESS,
    USER_PROFILE_REGISTRATION_FAIL,

    USER_PROFILE_CHANGE_AVATAR_REQUEST,
    USER_PROFILE_CHANGE_AVATAR_SUCCESS,
    USER_PROFILE_CHANGE_AVATAR_FAIL,

    UPDATE_CURRENT_USER_REQUEST,
    UPDATE_CURRENT_USER_SUCCESS,
    UPDATE_CURRENT_USER_FAIL,

    UPDATE_CURRENT_USER_PASSWORD_REQUEST,
    UPDATE_CURRENT_USER_PASSWORD_SUCCESS,
    UPDATE_CURRENT_USER_PASSWORD_FAIL

} from '../constants/User';

const InitialState = Record({
  error: null,
  loggedIn: false,
  profile: {},
  registrationProfie: {},
  isFetching: false,
  isRegistrationFetching: false,
  registrationSuccess: false,
  shouldRedirect: false,
  accessToken: null
});

const initialState = new InitialState;

export default function(state = initialState, action) {
  const {type} = action;

  switch(type) {

    case UPDATE_CURRENT_USER_PASSWORD_REQUEST: {
      return state.withMutations((ctx) => {
        ctx.set('isFetching', true)
           .set('error', null);
      });
    }

    case UPDATE_CURRENT_USER_PASSWORD_SUCCESS: {
      return state.withMutations((ctx) => {
        ctx.set('isFetching', false);
      }); 
    }

    case UPDATE_CURRENT_USER_PASSWORD_FAIL: {
      return state.withMutations((ctx) => {
        const {error} = action;
        ctx.set('isFetching', false)
           .set('error', error);
      });
    }

    case USER_PROFILE_CHANGE_AVATAR_REQUEST: {
      return state.withMutations((ctx) => {
        ctx.set('isFetching', true)
           .set('error', null);
      });
    }

    case USER_PROFILE_CHANGE_AVATAR_SUCCESS: {
      return state.withMutations((ctx) => {
        ctx.set('isFetching', false);
      }); 
    }

    case USER_PROFILE_CHANGE_AVATAR_FAIL: {
      return state.withMutations((ctx) => {
        const {error} = action;
        ctx.set('isFetching', false)
           .set('error', error);
      });  
    }

    case CHECK_AUTHORIZATION: {
      return state.withMutations((ctx) => {
          const {loggedIn} = action;
          ctx.set('loggedIn', loggedIn);
      });  
    }

    case LOG_IN_START: {
      return state.withMutations((ctx) => {
          ctx.set('isFetching', true)
             .set('shouldRedirect', false)
             .set('error', null);
      });
    }

    case LOG_OUT_START: {
      return state.withMutations((ctx) => {
          ctx.set('isFetching', true)
             .set('shouldRedirect', false)
             .set('error', null);
      });
    }

    case GET_PROFILE_FAIL: {
      return state.withMutations((ctx) => {
          const {error} = action;
          ctx.set('isFetching', false)
             .set('error', error);
      });
    }

    case GET_PROFILE_START: {
      return state.withMutations((ctx) => {
          ctx.set('isFetching', true)
             .set('error', null);
      });
    }

    case GET_PROFILE_SUCCESS: {
      return state.withMutations((ctx) => {
          const {profile} = action;
          ctx.set('isFetching', false)
             .set('profile', profile)
             .set('error', null);
      });
    }

    case LOG_IN_SUCCESS: {
      return state.withMutations((ctx) => {
          ctx.set('loggedIn', true)
             .set('profile', {})
             .set('isFetching', false)
             .set('shouldRedirect', true)
             .set('error', null);
      });
    }

    case LOG_IN_FAIL: {
       return state.withMutations((ctx) => {
          const {error} = action;
          ctx.set('loggedIn', false)
             .set('isFetching', false)
             .set('shouldRedirect', false)
             .set('error', error);
      });
    }

    case LOG_OUT_SUCCESS: {
      return state.withMutations((ctx) => {
          ctx.set('loggedIn', false)
             .set('profile', {})
             .set('isFetching', false)
             .set('shouldRedirect', true)
             .set('error', null );
      });
    }

    case LOG_OUT_FAIL: {
      return state.withMutations((ctx) => {
         const {error} = action;
         ctx.set('isFetching', false)
            .set('shouldRedirect', false)
            .set('error', error);
      });
    }

    case USER_PROFILE_GET_BY_CODE_REQUEST: {
      return state.withMutations(ctx => {
        ctx.set('isFetching', true);
      });
    }

    case USER_PROFILE_GET_BY_CODE_SUCCESS: {
      return state.withMutations(ctx => {
        ctx.set('isFetching', false)
           .set('registrationProfie', action.payload);
      });
    }

    case USER_PROFILE_GET_BY_CODE_FAIL: {
      return state.withMutations(ctx => {
        const { error } = action;
        ctx.set('isFetching', false)
           .set('error', error);
      });
    }

    case USER_PROFILE_REGISTRATION_REQUEST: {
      return state.withMutations(ctx => {
        ctx.set('isRegistrationFetching', true);
      });
    }

    case USER_PROFILE_REGISTRATION_SUCCESS: {
      return state.withMutations(ctx => {
        ctx.set('isRegistrationFetching', false)
           .set('registrationSuccess', true);
      });
    }

    case USER_PROFILE_REGISTRATION_FAIL: {
      return state.withMutations(ctx => {
        const { error } = action;
        ctx.set('isRegistrationFetching', false)
           .set('error', error);
      });
    }

    case UPDATE_CURRENT_USER_REQUEST: {
      return state.withMutations((ctx) => {
        ctx.set('isFetching', true)
           .set('error', null);
      });
    }

    case UPDATE_CURRENT_USER_SUCCESS: {
      return state.withMutations((ctx) => {
        ctx.set('isFetching', false);
      });
    }

    case UPDATE_CURRENT_USER_FAIL: {
      return state.withMutations((ctx) => {
       const {error} = action;
        ctx.set('isFetching', false)
           .set('error', error);
      });
    }
    default: { return state; }
  }
}
