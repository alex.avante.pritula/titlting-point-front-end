import {Record} from 'immutable';

import {
  GET_SELECTED_USER_PROFILE_FAIL,
  GET_SELECTED_USER_PROFILE_REQUEST,
  GET_SELECTED_USER_PROFILE_SUCCESS,

  GET_USERS_LIST_REQUEST,
  GET_USERS_LIST_SUCCESS,
  GET_USERS_LIST_FAIL,

  UPDATE_SELECTED_USER_REQUEST,
  UPDATE_SELECTED_USER_SUCCESS,
  UPDATE_SELECTED_USER_FAIL,

  SEELCTED_USER_PROFILE_CHANGE_AVATAR_REQUEST,
  SELECTED_USER_PROFILE_CHANGE_AVATAR_SUCCESS,
  SELECTED_USER_PROFILE_CHANGE_AVATAR_FAIL
} from '../constants/User';

const userProfile = Record({
   id: null,
   roleId: null,
   departmentId: null,
   job: '',
   name: '',
   phone: '',
   email: '',
   avatarUrl: '',
   biography: ''
});

const InitialState = Record({
  list: [],
  error: null,
  totalCount: 0,
  isFetching: false,
  selectedUser: new userProfile
});

const initialState = new InitialState;

function userDirectory(state = initialState, action) {

  const {type} = action;

  switch(type) {

    case UPDATE_SELECTED_USER_REQUEST: {
      return state.withMutations((ctx) => {
        ctx.set('isFetching', true)
           .set('error', null);
      });
    }

    case UPDATE_SELECTED_USER_SUCCESS: {
      return state.withMutations((ctx) => {
        ctx.set('isFetching', false);
      });
    }

    case UPDATE_SELECTED_USER_FAIL: {
      return state.withMutations((ctx) => {
       const {error} = action;
        ctx.set('isFetching', false)
           .set('error', error);
      });
    }
    
    case SEELCTED_USER_PROFILE_CHANGE_AVATAR_REQUEST: {
      return state.withMutations((ctx) => {
        ctx.set('isFetching', true)
           .set('error', null);
      });
    }

    case SELECTED_USER_PROFILE_CHANGE_AVATAR_SUCCESS: {
      return state.withMutations((ctx) => {
         ctx.set('isFetching', false);
      });
    }

    case SELECTED_USER_PROFILE_CHANGE_AVATAR_FAIL: {
      return state.withMutations((ctx) => {
        const {error} = action;
        ctx.set('isFetching', false)
           .set('error', error);
      });
    }

    case GET_SELECTED_USER_PROFILE_FAIL: {
      return state.withMutations((ctx) => {
        const {error} = action;
        ctx.set('isFetching', false)
           .set('selectedUser', new userProfile)
           .set('error', error);
      });
    }
    
    case GET_SELECTED_USER_PROFILE_REQUEST: {
      return state.withMutations((ctx) => {
        ctx.set('isFetching', true)
          .set('error', null);
      });
    }

    case GET_SELECTED_USER_PROFILE_SUCCESS: {
      return state.withMutations((ctx) => {
        let {profile} = action;
        const {role} = profile;
        const {id} = role;
        if(id){profile.roleId = id;}
        let selectedUser = new userProfile(profile);
        ctx.set('selectedUser', selectedUser)
           .set('isFetching', false);
      });
    }

    case GET_USERS_LIST_REQUEST: {
      return state.withMutations((ctx) => {
        ctx.set('isFetching', true)
            .set('error', null);
      });
    }

    case GET_USERS_LIST_SUCCESS: {
      return state.withMutations((ctx) => {
        const {count, users, reload} = action;
        let newUsers = users.map((user)=>{
          return new userProfile(user);
        });

        if(!reload) {
          const oldUsers = [...ctx.list];
          newUsers = oldUsers.concat(newUsers);
        }
        
        ctx.set('isFetching', false)
           .set('totalCount', count)
           .set('list', newUsers);
      });
    }

    case GET_USERS_LIST_FAIL: {
      return state.withMutations((ctx) => {
        const {error} = action;
        ctx.set('isFetching', false)
           .set('list', [])
           .set('totalCount', 0)
           .set('error', error);
      });
    }

    default: { return state; }
  }
}

export default userDirectory;