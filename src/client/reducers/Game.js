import {Record} from 'immutable';
import FormData from 'form-data';

import { 
    GET_GAMES_START,
    GET_GAMES_FAIL,
    GET_GAMES_SUCCESS,

    GAME_OPEN_MODAL,
    GAME_CLOSE_MODAL,

    GET_ALL_GENRES_REQUEST,
    GET_ALL_GENRES_SUCCESS,
    GET_ALL_GENRES_FAIL,

    GET_SELECTED_GAME_FAIL,
    GET_SELECTED_GAME_REQUEST,
    GET_SELECTED_GAME_SUCCESS,

    UPLOAD_SELECTED_GAME_PREVIEW,
    TOOGLE_SELECTED_GAME_EDITING_MODE,
    UPDATE_SIMILAR_GAME_STATUS,

    REMOVE_SELECTED_GAME_FAIL,
    REMOVE_SELECTED_GAME_REQUEST,
    REMOVE_SELECTED_GAME_SUCCESS,

    GET_ALL_EXIST_GAMES_FOR_SIMILAR_FAIL,
    GET_ALL_EXIST_GAMES_FOR_SIMILAR_REQUEST,
    GET_ALL_EXIST_GAMES_FOR_SIMILAR_SUCCESS,

    REMOVE_SELECTED_GAME_GENRE,
    ADD_SELECTED_GAME_GENRE,

    CREATE_NEW_GAME_FAIL,
    CREATE_NEW_GAME_REQUEST,
    CREATE_NEW_GAME_SUCCESS,

    UPDATE_SELECTED_GAME_FAIL,
    UPDATE_SELECTED_GAME_REQUEST,
    UPDATE_SELECTED_GAME_SUCCESS
  
} from '../constants/Game';


import {USER_ROLES} from '../constants/General';

const Genre = Record({
  id: null,
  genre: ''
});

const Game = Record({
  id: '',
  name: '',
  genres: [],
  imageUrl: '',
  ownerName: '',
  ownerRole: '',
  description: '',
  similarGames: [],
  isChecked: false,
  publishDate: null,
  genresForUpdate: []
});

const InitialState = Record({
  games: [],
  genres: [],
  count: 0,  
  error: null,
  isEditing: false,
  isFetching: false,
  selectedGameId: null,
  isOpenedModal: false,
  allGamesForCompairing: [],
  selectedGameData: new Game,
  selectedGamePreview: new FormData
});

const initialState = new InitialState;

export default function(state = initialState, action) {

  const {type} = action;
  
  console.warn(type, '111111111111111111111111');

  switch(type) {

    case CREATE_NEW_GAME_FAIL: {
      return state.withMutations((ctx) => {
         const {error} = action;
         ctx.set('isFetching', false)
            .set('error', error);
      });
    }

    case CREATE_NEW_GAME_REQUEST: {
      return state.withMutations((ctx) => {
         ctx.set('isFetching', true)
            .set('error', null);
      });
    }

    case CREATE_NEW_GAME_SUCCESS: {
      return state.withMutations((ctx) => {
         ctx.set('isFetching', false);
      });
    }

    case UPDATE_SELECTED_GAME_FAIL: {
      return state.withMutations((ctx) => {
         const {error} = action;
         ctx.set('isFetching', false)
            .set('error', error);
      });
    }

    case UPDATE_SELECTED_GAME_REQUEST: {
      return state.withMutations((ctx) => {
         ctx.set('isFetching', true)
            .set('error', null);
      });
    }

    case UPDATE_SELECTED_GAME_SUCCESS: {
      return state.withMutations((ctx) => {
         ctx.set('isFetching', false);
      });
    }

    case REMOVE_SELECTED_GAME_GENRE: {
      return state.withMutations((ctx) => {
        const {index} = action;
        let genres = [...ctx.getIn(['selectedGameData', 'genresForUpdate'])];
        genres.splice(index, 1);
        ctx.setIn(['selectedGameData', 'genresForUpdate'], genres);
      });
    }

    case ADD_SELECTED_GAME_GENRE: {
      return state.withMutations((ctx) => {
        const {index} = action;
        const allGenres = ctx.get('genres');

        const existIndex = allGenres.findIndex((item)=>{
          return (item.id === index);
        });
        let newGenre = new Genre(allGenres[existIndex]);
        let genres = [...ctx.getIn(['selectedGameData', 'genresForUpdate'])];
        genres.push(newGenre);
        ctx.setIn(['selectedGameData', 'genresForUpdate'], genres);
      });
    }

    case UPLOAD_SELECTED_GAME_PREVIEW: {
      return state.withMutations((ctx) => {
        const {file} = action;
        let selectedGamePreview = new FormData();
        selectedGamePreview.append('files', file);
        ctx.set('selectedGamePreview', selectedGamePreview);
      });
    }

    case UPDATE_SIMILAR_GAME_STATUS: {
     return state.withMutations((ctx) => {
        const {index, status} = action;
        let newSimilarGames = [...ctx.get('allGamesForCompairing')];
        let oldGame = newSimilarGames[index];
        const {              
          id, 
          name, 
          imageUrl, 
          description, 
          publishDate
        } = oldGame;

        let newGame = new Game({
          id,
          name,
          imageUrl,
          description,
          publishDate,
          isChecked: status
        });

        newSimilarGames.splice(index, 1, newGame);
        ctx.set('allGamesForCompairing', newSimilarGames);
      }); 
    }

    case GET_ALL_EXIST_GAMES_FOR_SIMILAR_FAIL: {
     return state.withMutations((ctx) => {
         const {error} = action;
         ctx.set('isFetching', false)
            .set('allGamesForCompairing', [])
            .set('error', error);
     });
    }

    case GET_ALL_EXIST_GAMES_FOR_SIMILAR_REQUEST: {
     return state.withMutations((ctx) => {
         ctx.set('isFetching', true)
            .set('error', null);
     });
    }

    case GET_ALL_EXIST_GAMES_FOR_SIMILAR_SUCCESS: {
     return state.withMutations((ctx) => {

        const {games} = action;
        const similarGames = ctx.getIn(['selectedGameData', 'similarGames']) || [];

        let gameItems = games.map((game) => {
            const {
              id, 
              name, 
              imageUrl, 
              description, 
              publishDate
            } = game;
            
            let hasSimilarIndex = similarGames.findIndex((similarGame)=>{
                return (similarGame.id === id);
            }); 
            let isChecked = (hasSimilarIndex !== -1);

            return new Game({
              id, 
              name, 
              imageUrl,
              isChecked, 
              description, 
              publishDate
            });
        });

        ctx.set('isFetching', false)
           .set('allGamesForCompairing', gameItems);   
     });
    }

    case REMOVE_SELECTED_GAME_FAIL: {
      return state.withMutations((ctx) => {
         const {error} = action;
         ctx.set('isFetching', false)
            .set('error', error);
      });
    }

    case REMOVE_SELECTED_GAME_REQUEST: {
      return state.withMutations((ctx) => {
         ctx.set('isFetching', true)
            .set('error', null);
      });
    }

    case REMOVE_SELECTED_GAME_SUCCESS: {
      return state.withMutations((ctx) => {
        ctx.set('isEditing', false)
           .set('isFetching', false)
           .set('isOpenedModal', false)
           .set('selectedGameId', null)
           .set('selectedGamePreview', new FormData)
           .set('allGamesForCompairing', [])
           .set('selectedGameData', new Game);
      });
    }

    case TOOGLE_SELECTED_GAME_EDITING_MODE: {
      return state.withMutations((ctx) => {
        const {isEditing} = ctx;
        ctx.set('isEditing', !isEditing);
      });
    }

    case GET_ALL_GENRES_REQUEST: {
      return state.withMutations((ctx) => {
        ctx.set('isFetching', true)
           .set('error', null);
      });
    }
  
    case GET_ALL_GENRES_SUCCESS: {
      return state.withMutations((ctx) => {
        const {genres} = action;
        const genreItems = genres.map((item)=>{
          const {genre, id} = item;
          return new Genre({genre, id});
        });
        ctx.set('isFetching', false)
           .set('genres', genreItems);
      });
    }
  
    case GET_ALL_GENRES_FAIL: {
      return state.withMutations((ctx) => {
        const {error} = action;
        ctx.set('isFetching', false)
           .set('error', error);
      });
    }

    case GAME_OPEN_MODAL: {
      return state.withMutations((ctx) => {
        ctx.set('isOpenedModal', true)
           .set('isEditing', true);
      });
    }

    case GAME_CLOSE_MODAL: {
       return state.withMutations((ctx) => {
        ctx.set('isEditing', false)
           .set('isOpenedModal', false)
           .set('selectedGameId', null)
           .set('selectedGamePreview', new FormData)
           .set('allGamesForCompairing', [])
           .set('selectedGameData', new Game);
      });
    }

    case GET_SELECTED_GAME_REQUEST: {
      return state.withMutations((ctx) => {
        ctx.set('isFetching', true)
           .set('error', null);
      });
    }

    case GET_SELECTED_GAME_SUCCESS: {
      return state.withMutations((ctx) => {
        const {game} = action;
        const {
          id,
          name,
          owner,
          genres,
          imageUrl,
          description,
          publishDate,
          similarGames
        } = game;

        let genreItems = genres.map((item)=>{
            const {id, genre} = item;
            return new Genre({id, genre});
        });

        let similarGamesItems = similarGames.map((similar)=>{
            const {
              id, 
              name, 
              imageUrl, 
              description, 
              publishDate
            } = similar;

            return new Game({
              id, 
              name,
              imageUrl,
              description,
              publishDate
            });
        });

        let ownerName = '';
        let ownerRole = '';

        if(owner) {
          ownerName = owner.name;
          let currentOwnerRoleId = owner['role'].id;
          let existRoleIndex = USER_ROLES.findIndex((role)=>{
            return (role.id === currentOwnerRoleId);
          });
          ownerRole = USER_ROLES[existRoleIndex].title;
        }

        let selectedGameData = new Game({
          id,
          name,
          imageUrl,
          ownerName,
          ownerRole,
          description,
          publishDate,
          genres: genreItems,
          similarGames: similarGamesItems,
          genresForUpdate: [...genreItems]
        });

        ctx.set('isEditing', false)
           .set('isOpenedModal', true)
           .set('isFetching', false)
           .set('selectedGameId', id)
           .set('selectedGameData', selectedGameData);
      });
    }

    case GET_SELECTED_GAME_FAIL: {
      return state.withMutations((ctx) => {
        const {error} = action;
        ctx.set('isFetching', false)
           .set('error', error)
           .set('selectedGameId', null)
           .set('selectedGameData', new Game);
      });
    }

    case GET_GAMES_START: {
       return state.withMutations((ctx) => {
        ctx.set('isFetching', true)
           .set('error', null);
      });
    }

    case GET_GAMES_SUCCESS: {
       return state.withMutations((ctx) => {
        const {games, count, reload} = action;
        let newGames = [...games];

        if(!reload) { 
          newGames = [...ctx.games].concat(newGames); 
        }
        ctx.set('isFetching', false)
           .set('games', newGames)
           .set('count', count);
      });
    }
    
    case GET_GAMES_FAIL: {
       return state.withMutations((ctx) => {
        const {error} = action;
        ctx.set('isFetching', false)
           .set('error', error);
      });
    }

    default: { return state; }
  }
}