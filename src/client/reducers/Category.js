import {Record} from 'immutable';

import { 
    GET_ALL_CATEGORIES_SUCCESS,
    GET_ALL_CATEGORIES_START,
    GET_ALL_CATEGORIES_FAIL 
} from '../constants/Category';

const InitialState = Record({
  list: [],
  error: null,
  isFetching: false
});

const initialState = new InitialState;

export default function(state = initialState, action) {

  const {type} = action;

  switch(type) {

    case GET_ALL_CATEGORIES_START: {
        return state.withMutations((ctx) => {
          ctx.set('isFetching', true)
             .set('error', null);
        });
    }

    case GET_ALL_CATEGORIES_SUCCESS: {
      return state.withMutations((ctx) => {
          const {categories} = action;
          ctx.set('list', categories)
             .set('isFetching', false)
             .set('error', null);
      });
    }

    case GET_ALL_CATEGORIES_FAIL: {
       return state.withMutations((ctx) => {
          const {error} = action;
          ctx.set('list', [])
             .set('isFetching', false)          
             .set('error', error);
      });   
    }

    default: { return state; }
  }
}