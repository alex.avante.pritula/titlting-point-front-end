import {Record} from 'immutable';

import {
    SET_IS_MOBILE_STATUS,
    LEFT_NAVIGATION_MENU_TOOGLE
} from '../constants/Navigation';

const InitialState = Record({
    isNavBarOpened: false,
    isMobile: false
});

const initialState = new InitialState;

export default function(state = initialState, action) {
  const {type} = action;

  switch(type) {

    case LEFT_NAVIGATION_MENU_TOOGLE: {
      return state.withMutations((ctx) => {
        const {isNavBarOpened} = ctx;
        ctx.set('isNavBarOpened', (!isNavBarOpened));
      });
    }

    case SET_IS_MOBILE_STATUS: {
      return state.withMutations((ctx) => {
        const {isMobile} = action;
        ctx.set('isMobile', isMobile);
      });
    }

    default: { return state; }
  }
}