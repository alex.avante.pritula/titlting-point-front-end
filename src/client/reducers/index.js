import {combineReducers} from "redux";
import {routerReducer} from "react-router-redux";
import { reducer as formReducer } from 'redux-form';

import user from './User';
import game from './Game';
import item from './Item';
import dashboard from './Dashboard';
import categories from './Category';
import navigation from './Navigation';
import userDirectory from './UserDirectory';
import departments from './departmentsReducer';
import recoveryPassword from './passwordRecoveryReducer';

import adminReducer from './admin/index';

const rootReducer = combineReducers({
  form: formReducer,
  routing: routerReducer,
  admin: adminReducer,
  navigation,
  categories,
  dashboard,
  user,
  item,
  game,
  departments,
  userDirectory,
  recoveryPassword
});

export default rootReducer;
