import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

class GridViewItem extends Component {

  static propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    onEdit: PropTypes.func.isRequired,
    genres: PropTypes.array.isRequired,
    imageUrl: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    publishDate: PropTypes.string.isRequired,
    similarGames: PropTypes.array.isRequired
  };

  constructor(props) {
    super(props);
  }

  render() {
    const {
      id,
      name,
      genres,
      onEdit,
      imageUrl,
      publishDate,
      similarGames
    } = this.props;

    let date = moment(publishDate).format('YYYY/MM/DD');
    let similarGameItems = similarGames.map((game, index)=>{
       const {id, name, imageUrl} = game;
       return (
          <div key={index} className="similar-item" onClick={()=>{ onEdit(id); }}>
            <img alt={name} className="thumbnail" src={imageUrl} />
          </div>
      );
    });

    let genreItems = genres.map((item, index)=>{
       const {genre} = item;
       return (<div key={index} className="genre">{genre}</div>);
    });

    return (
      <div className="item-box">
        <div className="item-main-block">
          <div className="image-container">
            <img className="thumbnail" src={imageUrl} />
          </div>
          <div className="info-container">
            <div className="item-actions">
              <div className="item-action-box">
                <span onClick={()=>{onEdit(id); }}
                      className="icons clickable ic-edit-inactive"/>
              </div>
              <div className="item-action-box">
                <span className="icons clickable ic-view-in-creative-gallery-small" />
              </div>
            </div>
            <div className="game-information">
              <p className="game-title">Title:</p>
              <p className="game-name">{name}</p>
            </div>
          </div>
        </div>
        <div className="chips">
          <div className="date">{date}</div>
          {genreItems}
        </div>
        <div className="similar-items">
          <p className="">Similar Titles:</p>
        </div>
        <div className="similar-items-container">
          {similarGameItems}
        </div>
      </div>
    );
  }
}

export default GridViewItem;
