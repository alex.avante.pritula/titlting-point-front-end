import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

class ListViewItem extends Component {
   
   static propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    onEdit: PropTypes.func.isRequired,
    genres: PropTypes.array.isRequired,
    imageUrl: PropTypes.string.isRequired,
    isMobile: PropTypes.boolean.isRequired,
    description: PropTypes.string.isRequired,
    publishDate: PropTypes.string.isRequired,
    similarGames: PropTypes.array.isRequired
  };

  constructor(props) {
    super(props);
  }

  render() {
    const {       
      id,
      name,
      genres,
      onEdit,
      imageUrl,
      isMobile,
      description,
      publishDate,
      similarGames
    } = this.props;

    let date = moment(publishDate).format('YYYY/MM/DD');
    let similarGameItems = similarGames.map((game, index)=>{
       const {id, name, imageUrl} = game;
       return (
          <div key={index} className="similar-item" onClick={()=>{ onEdit(id); }}>
            <img alt={name} className="thumbnail" src={imageUrl} />
          </div>
      );
    });

    let genreItems = genres.map((item, index)=>{
       const {genre} = item;
       return (<div key={index} className="genre">{genre}</div>);
    });

    if(isMobile) {
      return (
        <div className="item-box mobile">
            <div className="bounding-box">
              <div className="item-main-block">
                <div className="image-container">
                  <img className="thumbnail" src={imageUrl} />
                </div>
                <div className="info-container">
                  <div className="item-actions">
                    <div className="item-action-box">
                      <span onClick={()=>{ onEdit(id); }} 
                        className="icons clickable ic-edit-inactive" />
                    </div>
                    <div className="item-action-box">
                      <span className="icons clickable ic-view-in-creative-gallery-small" />
                    </div>
                  </div>
                  <div className="game-information">
                    <p className="game-title">Title:</p>
                    <p className="game-name">{name}</p>
                  </div>
                </div>
              </div>
              <hr className="line-separator-1"/>
              <div className="game-description">
                <p className="title">Description:</p>
                <p className="text">{description}</p>
              </div>
              <hr className="line-separator-2"/>
              <div className="chips">
                <div className="date">{date}</div>
                {genreItems}
              </div>
              <hr className="line-separator-3"/>
            </div>
            <div className="bounding-box">
              <div className="similar-items">
                <p className="">Similar Titles:</p>
              </div>
              <div className="similar-items-container">
                {similarGameItems}
              </div>
            </div>
          </div>
       );
    }

    return (
        <div className="item-box desktop">
          <div className="bounding-box main-item">
            <div className="item-main-block">
              <div className="image-container">
                <img className="thumbnail" src={imageUrl} />
              </div>
            </div>
            <div className="item-right-block">
              <div className="item-right-block-top">
                <div>
                  <div className="game-information">
                    <p className="game-title">Title:</p>
                    <p className="game-name">{name}</p>
                  </div>
                  <div className="game-description">
                    <p className="title">Description:</p>
                    <p className="text">{description}</p>
                  </div>
                </div>
                <div>
                  <div className="info-container">
                    <div className="item-actions">
                      <div className="item-action-box">
                        <span onClick={()=>{ onEdit(id); }} 
                          className="icons clickable ic-edit-inactive" />
                      </div>
                      <div className="item-action-box">
                        <span className="icons clickable ic-view-in-creative-gallery" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="chips">
                <div className="date">{date}</div>
                {genreItems}
              </div>
            </div>
          </div>
          <div className="bounding-box additional-item">
            <div className="similar-items">
              <p className="">Similar Titles:</p>
            </div>
            <div className="similar-items-container">
              {similarGameItems}
            </div>
          </div>
        </div>
      );
  }
}

export default ListViewItem;
