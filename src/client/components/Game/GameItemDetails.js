import {Spinner} from 'react-mdl';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import React, {Component} from 'react';
import Input from '../UIComponents/Input';
import moment from 'moment';
import TagsEditor from '../UIComponents/TagEditor';
import renderDateTimePicker from '../UIComponents/DatePicker';

import {reduxForm, Field, submit} from 'redux-form';
import DropZone from '../General/DropZoneContainer';
import GameCheckboxItem from '../UIComponents/GameCheckboxItem';

import {
    getGames,
    closeModalDialog,
    uploadGamePreviewCall,
    addSelectedGameGenreCall,
    toogleGameEditingModeCall,
    updateGameCompleteAPICall,
    getAllGenresCompleteAPICall,
    updateSimilarGameStatusCall,
    removeSelectedGameGenreCall,
    createNewGameCompleteAPICall,
    getSelectedGameCompleteAPICall,
    closeModalDialogCompleteApiCall,
    getAllExistGamesCompleteAPICall,
    removeSelectedGameCompleteAPICall,
} from '../../actions/Game/Game';

class GameItemDetails extends Component {
    
    static propTypes = {
        removeSelectedGameCompleteAPICall: PropTypes.func.isRequired,
        getAllExistGamesCompleteAPICall: PropTypes.func.isRequired, 
        getSelectedGameCompleteAPICall: PropTypes.func.isRequired,
        createNewGameCompleteAPICall: PropTypes.func.isRequired,
        getAllGenresCompleteAPICall: PropTypes.func.isRequired,
        updateSimilarGameStatusCall: PropTypes.func.isRequired,
        updateGameCompleteAPICall: PropTypes.boolean.isRequred,
        toogleGameEditingModeCall: PropTypes.boolean.isRequred,
        removeSelectedGameGenreCall: PropTypes.func.isRequired,
        addSelectedGameGenreCall: PropTypes.func.isRequired,
        genresForAutoComplete:  PropTypes.array.isRequired,
        allGamesForCompairing: PropTypes.array.isRequired,
        uploadGamePreviewCall: PropTypes.func.isRequired,
        selectedGamePreview: PropTypes.object.isRequired,
        genresForUpdate: PropTypes.array.isRequired,
        closeModalDialog: PropTypes.func.isRequred,
        isOpenedModal: PropTypes.boolean.isRequred,
        selectedGameId: PropTypes.number.isRequred,
        initialValues: PropTypes.object.isRequired,
        similarGames: PropTypes.array.isRequired,
        isFetching: PropTypes.boolean.isRequred,
        nextItemId: PropTypes.number.isRequred,
        prevItemId: PropTypes.number.isRequred,
        isEditing: PropTypes.boolean.isRequred,
        handleSubmit: PropTypes.func.isRequred,
        sortType: PropTypes.object.isRequired,     
        ownerName: PropTypes.string.isRequred,
        ownerRole: PropTypes.string.isRequred,
        imageUrl: PropTypes.string.isRequred,
        genres: PropTypes.array.isRequired,
        getGames: PropTypes.func.isRequred,
        count: PropTypes.number.isRequred,
        games: PropTypes.array.isRequired 
    }

    constructor(props){
        super(props);
        this.state = {currentField: null};   
        this.handleEditMode = this.handleEditMode.bind(this);
        this.handleAddTag = this.handleAddTag.bind(this);
        this.handleRemoveTag = this.handleRemoveTag.bind(this);
        this.handleClickNext = this.handleClickNext.bind(this);
        this.handleClickPrev = this.handleClickPrev.bind(this);
        this.handleRemoveGame = this.handleRemoveGame.bind(this);
        this.handleCloseDialog = this.handleCloseDialog.bind(this);
        this.handleSelectField = this.handleSelectField.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
        this.handleChangeGamePreview = this.handleChangeGamePreview.bind(this);
        this.handleChangeSimilarGameStatus = this.handleChangeSimilarGameStatus.bind(this);
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentDidUpdate(prevProps) {
        const oldIsOpenedModalMode = prevProps.isOpenedModal;
        const oldSelectedGameId = prevProps.selectedGameId;

        const {
           count, 
           isOpenedModal,
           selectedGameId,
           getAllGenresCompleteAPICall,
           getAllExistGamesCompleteAPICall
        } = this.props;

        if ((isOpenedModal && !oldIsOpenedModalMode) ||
            (oldSelectedGameId !== selectedGameId && oldSelectedGameId !== null && selectedGameId !== null)
        ){ 
            getAllGenresCompleteAPICall();
            getAllExistGamesCompleteAPICall(count); 
        }
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleChangeSimilarGameStatus(index, status){
        const {updateSimilarGameStatusCall} = this.props;
        updateSimilarGameStatusCall(index, status);
    }

    handleChangeGamePreview(file){
        const {uploadGamePreviewCall} = this.props;
        uploadGamePreviewCall(file);
    }

    handleRemoveTag(tagIndex){
       const {removeSelectedGameGenreCall} = this.props;
       removeSelectedGameGenreCall(tagIndex);
    }

    handleEditMode(){
        const {toogleGameEditingModeCall} = this.props;
        toogleGameEditingModeCall();
    }

    handleSelectField(field){
        const {currentField} = this.state;
        if(currentField == field){ this.setState({currentField: null}); }
        else{ this.setState({currentField: field}); }
    }

    handleClickPrev(){
        const {prevItemId, getSelectedGameCompleteAPICall} = this.props;
        getSelectedGameCompleteAPICall(prevItemId);
    }

    handleClickNext(){
        const {nextItemId, getSelectedGameCompleteAPICall} = this.props;
        getSelectedGameCompleteAPICall(nextItemId);
    }

    handleClickOutside(event){
        const {target} = event;
        const {/*closeModalDialog,*/ isFetching} = this.props;
        if (!isFetching && this.GameModalDialog && 
            !this.GameModalDialog.contains(target)) {  
            //closeModalDialog(); 
        }
    }

    handleCloseDialog(){
        const {closeModalDialog} = this.props;
        closeModalDialog();
    }

    handleRemoveGame(){
        const {
            games,
            sortType,
            getGames,
            selectedGameId, 
            closeModalDialog,
            removeSelectedGameCompleteAPICall
        } = this.props;

        removeSelectedGameCompleteAPICall(selectedGameId).then(()=>{
            getGames(sortType, 0, games.length, true);
            closeModalDialog();
        });
    }

    handleAddTag(tagIndex){
        const {addSelectedGameGenreCall} = this.props;
        addSelectedGameGenreCall(tagIndex);
    }

    render(){
       const {  
          imageUrl,
          ownerName, 
          ownerRole,
          isEditing,
          prevItemId,
          nextItemId,
          isFetching, 
          handleSubmit,
          isOpenedModal,
          selectedGameId,
          genresForUpdate,
          selectedGamePreview,
          allGamesForCompairing,
          genresForAutoComplete
       } = this.props;

       if(!isOpenedModal){ return null; }

       const {currentField} = this.state;
       const file = selectedGamePreview.get('files') ;
       let preview = (file ? null : imageUrl);
       let smallPreview = 'assets/svg/icon-new.svg';

       if(file){ smallPreview = file.preview; }
       else if (imageUrl){ smallPreview = imageUrl; }

       let hasPrev = (prevItemId !== -1);
       let hasNext = (nextItemId !== -1);
       let navigation = null;
       let ownerDescription = null;
    
       let similarGameItems = allGamesForCompairing.map((gameForCompaire, index)=>{
            const {
              id, 
              name, 
              imageUrl,
              isChecked
            } = gameForCompaire;

            return (
              <GameCheckboxItem 
                id={id}
                key={index} 
                name={name}
                index={index}
                image={imageUrl}
                disabled={!isEditing || isFetching}
                onChange={this.handleChangeSimilarGameStatus}
                isChecked={isChecked} 
            />);
       });

       if(ownerName && ownerRole){
           ownerDescription = (
            <div className="game-item-description-container-general-info-field added-by-form-section"> 
                <div>    
                    <div className="game-item-description-container-general-info-field-title">
                        <label>Added by:</label>
                    </div>
                    <div className="game-item-description-container-general-info-field-input">
                            <span>{ownerName}</span>
                            <span>{ownerRole}</span>
                    </div>
                </div>
                <div />
            </div>
           );
       }

        if(selectedGameId){

        let removePartOfNavigation = null;
        if(isEditing){
            removePartOfNavigation =(
                <button type="button" disabled={isFetching} onClick={this.handleRemoveGame} 
                    className="item-description-table__header__icon_right_second">
                    <span className="icons ic-delete-active" />
                </button>
            );
        }
        
        navigation = (
            <div>
                {removePartOfNavigation}
                <button type="button" disabled={isFetching} onClick={this.handleEditMode} >
                    <span className={"icons ic-edit-"+ (isEditing ? "in": "") +"active"} />
                </button>
                <button type="button" disabled={isFetching} 
                    className="item-description-table__header__icon_right_third">
                    <span className="icons ic-send-invite" />
                </button>
            </div>
        );
       }

        let footerViewing = (
            <div className="game-item-description-container-footer viewing">
                <button type="button" style={{opacity: (hasPrev ? 1: 0)}} 
                    disabled={!hasPrev} 
                    onClick={this.handleClickPrev}>
                    <span className="icons ic-prev-inactive" />
                </button>
                <button type="button" style={{opacity: (hasNext ? 1: 0)}} 
                    disabled={!hasNext} 
                    onClick={this.handleClickNext}>
                    <span className="icons ic-next-inactive" />
                </button>
            </div>
        );
        
        let footerEditing = (
            <div className="game-item-description-container-footer">
                <button type="button" disabled={isFetching} onClick={this.handleCloseDialog}>
                    <p>cancel</p>
                </button>
                <button disabled={isFetching} type="submit">
                { isFetching ? <Spinner singleColor /> : <p>pin it</p> }
                </button>
            </div>
        ); 

        return (
         <div className="game-item-description-body-containter">
            <form onSubmit={handleSubmit}>
            <div ref={(node)=>{this.GameModalDialog = node;}} 
                className="game-item-description-container">
                <div className="game-item-description-container-header">
                    <div className="game-item-description-short-title">
                        <img src={smallPreview} />
                        <div>
                            <label></label>
                            <span></span>
                        </div>
                    </div>
                    {navigation}     
                </div>
                <div className="game-item-description-container-body">
                    <div className="game-item-description-container-general-info">
                        <DropZone   
                            disabled={!isEditing || isFetching}                  
                            uploadType={'IMAGE'} 
                            defaultPreview={preview}
                            onChangeData={this.handleChangeGamePreview}
                            previewContent={
                            <span>
                                Image box
                                <br/>
                                drag and drop
                                <br/>
                                main image 
                                <br/>
                                for new game
                            </span>}/>
                        <div className="game-item-description-container-general-info-body">
                            <div className="game-item-description-container-general-info-field title-form-section">
                                <div>
                                    <div className="game-item-description-container-general-info-field-title">
                                        <label>Title of new item:</label>
                                    </div>
                                    <div className="game-item-description-container-general-info-field-input">
                                        <Field 
                                            name="name"
                                            label=""
                                            rows={2}
                                            component={Input}
                                            disabled={!isEditing || isFetching}  
                                            onFocus={()=>{ this.handleSelectField("name"); }}
                                            onBlur={()=>{ this.handleSelectField("name"); }}
                                        />
                                    </div>
                                </div>
                                <div>
                                    <span className={"icons ic-edit-" + (currentField === "name" ? "" : "in") + "active"} />
                                </div>
                            </div>

                            <div className="spetific-info-field">
                                <div className="game-item-description-container-general-info-field date-form-section">
                                    <div>
                                        <div className="game-item-description-container-general-info-field-title">
                                            <label>Publication date:</label>
                                        </div>
                                        <div className="game-item-description-container-general-info-field-input">  
                                             <Field name="publishDate"
                                                showTime={false}
                                                disabled={!isEditing || isFetching}  
                                                component={renderDateTimePicker}
                                             />
                                        </div> 
                                    </div>
                                    <div>
                                        <span className={"icons ic-edit-" + (currentField === "publishDate" ? "" : "in") + "active"} />
                                    </div>
                                </div>
                                {ownerDescription}
                            </div>

                            <div className="game-item-description-container-general-info-field">
                                <div>
                                    <div className="game-item-description-container-general-info-field-title">
                                        <label>Categories:</label>
                                    </div>
                                    <div className="game-item-description-container-general-info-field-input">
                                        <TagsEditor 
                                            title={'Write new game category...'}
                                            disabled={!isEditing || isFetching}
                                            valueIndex="id"
                                            dataIndex="genre"
                                            existItems={genresForUpdate} 
                                            itemsForAutoComplete={genresForAutoComplete} 
                                            onRemoveTag={this.handleRemoveTag}
                                            onAddNewTag={this.handleAddTag}
                                        />
                                    </div>  
                                </div>
                                <div>
                                   <span className={"icons ic-edit-" + (currentField === "genres" ? "" : "in") + "active"} />
                                </div>
                            </div>
                            
                            <div className="game-item-description-container-general-info-field">
                                <div>
                                    <div className="game-item-description-container-general-info-field-title">
                                        <label>Description:</label>
                                    </div>
                                    <div className="game-item-description-container-general-info-field-input">
                                        <Field 
                                            name= "description"
                                            label=""
                                            component={Input} 
                                            disabled={!isEditing || isFetching}  
                                            onFocus={()=>{ this.handleSelectField("description"); }}
                                            onBlur={()=>{ this.handleSelectField("description"); }}
                                        />
                                    </div>   
                                </div>
                                <div>
                                   <span className={"icons ic-edit-" + (currentField === "description" ? "" : "in") + "active"} />
                                </div>          
                            </div>
                        </div>
                    </div>
                    <div className="game-item-description-container-similar-titles">
                        <div className="game-item-description-container-similar-titles-header">
                            <p>Similar titles:</p>
                        </div>    
                        <div className="game-item-description-container-similar-titles-body">
                            {similarGameItems}
                        </div> 
                    </div>
                {isEditing ? footerEditing: footerViewing}
                </div>
            </div>
            </form>
        </div>
      );
    }
}

const gameReduxForm = {
  form: 'game-edit-form',
  enableReinitialize: true,
  validate: (values, props) => {
    const {dirty} = props;
    const errors = {};

    if (!dirty){
       return errors;
    }

    const {name, description} = values;

    if (!name){
        errors.name = 'Name is empty';
    }

    if (!description){
       errors.description = 'Description is empty';
    }
    return errors;
  },

  onSubmit:(values, dispatch, props)=>{
      const {
        name, 
        publishDate,
        description
      } = values;

      const {
        games,
        sortType,
        oldGameGenres,
        similarGames,
        selectedGameId,
        genresForUpdate,
        selectedGamePreview,
        allGamesForCompairing
      } = props;

      let similarGamesForRemove = [];
      let similarGamesForAdd = [];


      allGamesForCompairing.map((game)=>{
         const {id, isChecked} = game; 
         let foundIndex = similarGames.findIndex((similarGame)=>{
            return (similarGame.id === id);
         });
         let alreadyExist = (foundIndex !== -1);
         if (!alreadyExist && isChecked) {
            similarGamesForAdd.push(id);
         }

         if (alreadyExist && !isChecked){
            similarGamesForRemove.push(id);
         }
      });


      let file = selectedGamePreview.get('files');
      let date = moment(publishDate).valueOf();

      if(!selectedGameId) {
        let data = {
            name,
            description,
            publishDate: date,
            genres: genresForUpdate.map((item)=>{
                const {genre} = item;
                return genre.toLowerCase();
            }),
            similarGames: similarGamesForAdd
        };

        createNewGameCompleteAPICall(data, file, dispatch).then(()=>{
            closeModalDialogCompleteApiCall(sortType, 0 , games.length, dispatch); 
        });    
        return;
      }

      let data = {
        name,
        description,
        publishDate: date
      };


      let genresForRemove = oldGameGenres.filter((oldGenre)=>{
          let isExist = genresForUpdate.findIndex((genreForUpdate)=>{
            return (oldGenre.genre === genreForUpdate.genre);
          });
          return (isExist === -1);
      });


      let genresForAdd = genresForUpdate.filter((genreForUpdate)=>{
          let isExist = oldGameGenres.findIndex((oldGenre)=>{
            return (oldGenre.genre === genreForUpdate.genre);
          });
          return (isExist === -1);
      });
     
     updateGameCompleteAPICall(
        selectedGameId,
        data, 
        file, 
        genresForAdd, 
        genresForRemove, 
        similarGamesForAdd, 
        similarGamesForRemove, 
        dispatch
     ).then(()=>{
        closeModalDialogCompleteApiCall(sortType, 0 , games.length, dispatch); 
     }); 
  }
};

function mapStateToProps(state, ownProps) {
    const {game} = state;
    const {sortType} = ownProps;

    const {
      games,
      count,
      genres,
      isFetching,   
      isEditing,
      isOpenedModal,
      selectedGameId,
      selectedGameData,
      selectedGamePreview,
      allGamesForCompairing
    } = game;
    
    const {
      name,
      imageUrl,
      ownerName,
      ownerRole,
      description,
      publishDate,
      similarGames,
      genresForUpdate
    } = selectedGameData;

    let genresForAutoComplete = genres.filter((generalGenre)=>{
        let existIndex = genresForUpdate.findIndex((genreForUpdate)=>{
            return (genreForUpdate.genre === generalGenre.genre);
        });
        return (existIndex === -1);
    });

    let initialValues = {
        name,
        description,
        publishDate
    };

    let nextItemId = -1;
    let prevItemId = -1;

    if (selectedGameId !== null){

       let currentGameIndex = games.findIndex((gameItem)=>{
           return (selectedGameId === gameItem.id);
       });

       if(currentGameIndex !== 0){
          prevItemId = games[currentGameIndex - 1].id;
       }

       if(currentGameIndex !== games.length - 1){
          nextItemId = games[currentGameIndex + 1].id;
       }
    }
    
    return {
      games,
      count,
      genres,
      sortType,
      imageUrl,
      ownerRole,
      ownerName,
      isEditing,
      nextItemId,
      prevItemId,
      isFetching,
      similarGames,
      isOpenedModal,
      initialValues,
      selectedGameId,
      genresForUpdate,
      selectedGamePreview,
      genresForAutoComplete,
      allGamesForCompairing,
      oldGameGenres: selectedGameData.genres
    };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    submit,
    getGames,
    closeModalDialog,
    uploadGamePreviewCall,
    addSelectedGameGenreCall,
    toogleGameEditingModeCall,
    updateGameCompleteAPICall,
    getAllGenresCompleteAPICall,
    updateSimilarGameStatusCall,
    removeSelectedGameGenreCall,
    createNewGameCompleteAPICall,
    getSelectedGameCompleteAPICall,
    getAllExistGamesCompleteAPICall,
    closeModalDialogCompleteApiCall,
    removeSelectedGameCompleteAPICall
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(reduxForm(gameReduxForm)(GameItemDetails));