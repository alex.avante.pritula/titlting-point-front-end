import {connect} from 'react-redux';
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import InfiniteScroll from 'redux-infinite-scroll';
import {MenuItem, SelectField, Option} from 'react-mdl-extra';

/** --- CUSTOM COMPONENTS --- **/
import Breadcrumb from '../UIComponents/Breadcrumb';
import MainActionButton from '../UIComponents/MainActionButton';
import SortViewChanger from '../UIComponents/SortViewChanger';
import ListViewItem from './views/ListViewItem';
import GridViewItem from './views/GridViewItem';
import GameItemDetails from './GameItemDetails';

import {
    getGames, 
    openModalDialog,
    getSelectedGameCompleteAPICall
} from '../../actions/Game/Game';

import {
    SORT_TYPES, 
    VIEW_LIST, 
    LIMIT
} from '../../constants/General';

const sortTypes = Object.keys(SORT_TYPES);

class GameGallery extends Component {
    static propTypes = {
        games: PropTypes.array.isRequired,
        count: PropTypes.number.isRequired,
        getGames: PropTypes.func.isRequired,
        isMobile: PropTypes.boolean.isRequired,
        openModalDialog: PropTypes.boolean.isRequired,
        getSelectedGameCompleteAPICall: PropTypes.func.isRequired
    };

    constructor(props){
        super(props);
        this.state = {
            isOpened: false,
            visualType: VIEW_LIST,
            sortType: sortTypes[0],
            isOpenCreateDialog: false
        };

        this.handleEdit = this.handleEdit.bind(this);
        this.handleViewType = this.handleViewType.bind(this);
        this.handleScrollDown = this.handleScrollDown.bind(this);
        this.handleSortingType = this.handleSortingType.bind(this);
        this.handleOpenNavigation = this.handleOpenNavigation.bind(this);
        this.handleOpenNavigation = this.handleOpenNavigation.bind(this);
        this.handleOpenCreateDialog = this.handleOpenCreateDialog.bind(this);
        this.handleCloseCreateDialog = this.handleCloseCreateDialog.bind(this);
        this.handleToogleCreateDialog = this.handleToogleCreateDialog.bind(this);
        this.handleNavigationClickOutside = this.handleNavigationClickOutside.bind(this);
    }

    componentWillMount(){
        const {sortType} = this.state;
        this.props.getGames(sortType, 0, LIMIT);
    }

    handleEdit(id){
        const {getSelectedGameCompleteAPICall} = this.props;
        getSelectedGameCompleteAPICall(id);
    }

    handleScrollDown(){
        const {games, getGames} = this.props;
        const {sortType} = this.state;
        getGames(sortType, games.length, LIMIT);
    }

    handleSortingType(newSortType){
        const {sortType} = this.state;
        if (newSortType === sortType){ return; }
        const {games, getGames} = this.props;
        this.setState({sortType: newSortType});  
        getGames(newSortType, 0, games.length, true);
    }

    handleViewType(type){
      this.setState({visualType: type});
    }

    handleOpenCreateDialog(){
        this.setState({isOpenCreateDialog: true});
    }

    handleCloseCreateDialog(){
        this.setState({isOpenCreateDialog: false});
    }

    handleToogleCreateDialog(){
        const {isOpenCreateDialog} = this.state;
        this.setState({isOpenCreateDialog: !isOpenCreateDialog});
    }

    handleOpenNavigation(){
        const {isOpened} = this.state;
        this.setState({isOpened: !isOpened});
    }

    handleNavigationClickOutside(){
        this.setState({isOpened: false});
    }

    render(){
        const {
            isOpenCreateDialog, 
            sortType, 
            visualType
        } = this.state;

        const {
            games, 
            count, 
            isMobile, 
            openModalDialog
        } = this.props;

        let currentType = SORT_TYPES[sortType];
        let needToLoad = (count <= games.length);
        let gameItemElement = null;

        let sortItems = sortTypes.map((type, index)=>{
          return <Option key={index} value={type}>{SORT_TYPES[type]}</Option>;
        });

        if (visualType === VIEW_LIST){ gameItemElement = ListViewItem;} 
        else { gameItemElement = GridViewItem; }


        let creativeItems = games.map((game, index)=>{
            const {
                id,
                name,
                genres,
                imageUrl,
                publishDate,
                description,
                similarGames
            } = game;

            return React.createElement(gameItemElement, {
                id,
                name,
                index,
                genres,
                isMobile,
                imageUrl,
                key: index,
                description,
                publishDate,
                similarGames,
                onEdit: this.handleEdit
            });
        });

        let createNavigation = null;

        if (isOpenCreateDialog) {
          createNavigation = (
            <ul className="block_add_items_list">
                  <MenuItem className="tgpoint--dashboard-dropdown-item"
                     closeMenu={()=>{}}
                     onClick={()=>{
                         this.handleCloseCreateDialog();
                         openModalDialog();
                     }}>
                       Add New Game
                 </MenuItem>
                 <MenuItem className="tgpoint--dashboard-dropdown-item"
                     closeMenu={()=>{}}
                     onClick={()=>{}}>
                       Add New Creative
                 </MenuItem>
                 <MenuItem className="tgpoint--dashboard-dropdown-item"
                     closeMenu={()=>{}}
                     onClick={()=>{}}>
                       Add New Asset Folder
                 </MenuItem>
                 <MenuItem className="tgpoint--dashboard-dropdown-item"
                     closeMenu={()=>{}}
                     onClick={()=>{}}>
                       Add New Asset
                 </MenuItem>
              </ul>
            );
        }

        let breadcrumbComponents = (
          <MainActionButton action={this.handleToogleCreateDialog} >
             <p>ADD NEW ITEM</p>
              {createNavigation}
          </MainActionButton>
        );

        return (
          <div className="tgpoint-content-body game-gallery">
            <GameItemDetails sortType={sortType} />   
            <Breadcrumb pageTitle="GAME GALLERY" components={breadcrumbComponents} />
            <div className="tgpoint-gallery-container">
              <div className="actions">
                <div className="action-item sortable-filter">
                  <p className="sort-by">Sort by</p>
                  <SelectField id={'game-sorting'}
                               bsStyle={'link'} noCaret
                               className={'game-sorting-drop-down'}
                               label={currentType}
                               onChange={type => { this.handleSortingType(type); }}>
                    {sortItems}
                  </SelectField>
                </div> 
                <SortViewChanger type={visualType} toggleSwitchViewMode={this.handleViewType} />
              </div>
              <div className="tgpoint-gallery">
                <InfiniteScroll className={classNames('items-view', visualType === VIEW_LIST ? 'list' : 'grid')}
                  loader={null}
                  loadingMore={needToLoad}
                  elementIsScrollable={true}
                  loadMore={this.handleScrollDown} >
                  {creativeItems}
                </InfiniteScroll>
              </div>
            </div>
          </div>
        );
    }
}

function mapStateToProps(state) {
    const {game, navigation} = state;
    const {games, count} = game;
    const {isMobile} = navigation;
    return {
      games, 
      count, 
      isMobile
    };
}

const mapDispatchToProps = ({
    getGames,
    openModalDialog,
    getSelectedGameCompleteAPICall
});

export default connect(mapStateToProps, mapDispatchToProps)(GameGallery);
