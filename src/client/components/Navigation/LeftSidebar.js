import {connect} from 'react-redux';
import {Link} from 'react-router';
import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {navigationMenuToogle} from  '../../actions/Navigation';
import {getAllCategories} from '../../actions/Category';
import {getCurrentUserProfile} from '../../actions/User/Profile';

class LeftSidebar extends Component {

  static propTypes = {
    list: PropTypes.array.isRequired,
    profile: PropTypes.object.isRequired,
    loggedIn: PropTypes.boolean.isRequired,
    currentRoute: PropTypes.object.isRequired,
    getAllCategories: PropTypes.func.isRequired,
    navigationMenuToogle: PropTypes.func.isRequired,
    getCurrentUserProfile: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {isCategoriesOpened: false};
    this.handleCategoriesToggle = this.handleCategoriesToggle.bind(this);
  }

  componentDidUpdate(prevProps) {
    const {
      loggedIn,
      getAllCategories,
      getCurrentUserProfile
    } = this.props;

    if (loggedIn && !prevProps.loggedIn) {
      getAllCategories();
      getCurrentUserProfile();
    }
  }

  handleCategoriesToggle() {
    const {isCategoriesOpened} = this.state;
    this.setState({isCategoriesOpened: !isCategoriesOpened});
  }

  render() {
    const {/*list,*/ profile, navigationMenuToogle/*isNavBarOpened,*/} = this.props;
    const {role, name, avatarUrl} = profile;
   // const {isCategoriesOpened} = this.state;

    const links = [{
        link: 'dashboard/1',
        icon: 'ic-dashboard',
        title: 'Dashboard'
      },{
       link: 'user-directory',
       icon: 'ic-user',
       title: 'User Directory'
    },{
        link: 'creatives',
        icon: 'ic-creatives',
        title: 'Creatives'
    }].map((item, index) => {
      const {link, icon, title} = item;
      return (
        <li className="menu-item" key={index} onClick={navigationMenuToogle}>
          <Link className={'link'} activeClassName={'active'} to={'/' + link}>
            <span className={'icons ' + icon}/>
            <span className="text">{title}</span>
          </Link>
        </li>
      );
    });
  //  let categoryItems = [];
    let image = avatarUrl || '/assets/svg/profile-icon-big.svg';
   // let categoriesClasses = 'sidebar-nav-menu-categories ' + ((isCategoriesOpened) ? 'open' : 'close');

  /*  if (list) {
      categoryItems = list.map((parentCategory) => {
        const {id, category, subCategories} = parentCategory;
        let subCategoryItems = subCategories.map((subCategory) => {
          const {id, category} = subCategory;
          return (
            <li key={id}>
              <span>{category}</span>
            </li>
          );
        });
        return (
          <li key={id}>
            <span>{category}</span>
            <ul>{subCategoryItems}</ul>
          </li>
        );
      });
    }*/

    return (
      <div className="left-sidebar-container">
        <div className="tgpoint--logo">
          <Link to={'/dashboard/1'}>
            <img src={'/assets/svg/logo.svg'}/>
          </Link>
        </div>
        <div className="tgpoint--sidebar-profile">
          <div className="tgpoint--sidebar-profile-item avatar">
            <img src={image}/>
          </div>
          <div className="tgpoint--sidebar-profile-item info">
            <span className="role">{role ? role.replace(/\s/, '') : ''}</span>
            <span className="name">{name}</span>
          </div>
          <div className="tgpoint--sidebar-profile-item settings">
            <Link onClick={navigationMenuToogle} className="sidebar-nav-profile-config" to={'/profile'} />
          </div>
        </div>
        <div className="sidebar-admin-nav-menu">
          <Link to={'/admin/users'} activeClassName={'is-active'} >
            MANAGE USERS
          </Link>
        </div>
        <ul className="sidebar-nav-menu">
          {links}
          <li className="menu-item">
            <a className="link sidebar-nav-menu-bottom-border" onClick={this.handleCategoriesToggle}>
              <span className="icons ic-categories"/>
              <span className="text">Categories</span>
            </a>
            {/*<ul className={categoriesClasses}>*/}
              {/*{categoryItems}*/}
            {/*</ul>*/}
          </li>
        </ul>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const {categories, user, navigation} = state;
  const {list} = categories;
  const {profile, loggedIn} = user;
  const {isNavBarOpened} = navigation;

  return {
    list,
    profile,
    loggedIn,
    isNavBarOpened
  };
};

const mapDispatchToProps = ({
  getAllCategories,
  navigationMenuToogle,
  getCurrentUserProfile
});

export default connect(mapStateToProps, mapDispatchToProps)(LeftSidebar);
