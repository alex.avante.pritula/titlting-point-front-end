import {connect} from 'react-redux';
import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {ENTER} from '../../constants/General';

import {logOut} from '../../actions/User/Auth';

import {navigationMenuToogle} from '../../actions/Navigation';
import {setDashboardQueryCall} from '../../actions/Dashboard/Dashboard';

const ROUTES = {
  DASHBOARD: 'dashboard',
  USER_DIRECTORY: 'user-directory',
  CREATIVES: 'creatives'
};
const ROUTE_KEYS = Object.keys(ROUTES);

class TopNavigation extends Component {

  static propTypes = {
    logOut: PropTypes.func.isRequired,
    isMobile: PropTypes.bool.isRequired,
    shouldRedirect: PropTypes.bool.isRequired,
    currentRoute: PropTypes.string.isRequired,
    navigationMenuToogle: PropTypes.func.isRequired,
    setDashboardQueryCall: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {isOpenSearch: false};
    this.handleSearch = this.handleSearch.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.handleOnChange = this.handleOnChange.bind(this);
  }

  handleSearch() {
    const {isMobile} = this.props;
    const {isOpenSearch} = this.state;
    if (isMobile) {
      this.setState({isOpenSearch: !isOpenSearch});
    }
  }

  handleKeyDown(evt) {
    const {keyCode} = evt;
    const {isMobile} = this.props;

    if (keyCode === ENTER) {
      if (isMobile) {
        this.setState({isOpenSearch: false});
      }
    }
  }

  handleOnChange(/*evt*/){
  //  const {value} = evt.target;
    const {
      currentRoute,
    //  setDashboardQueryCall
    } = this.props;

    let routeIndex = ROUTE_KEYS.findIndex((key)=>{
      return (currentRoute.indexOf(ROUTES[key]) !== -1);
    });
    
    switch(ROUTE_KEYS[routeIndex]){
      case 'DASHBOARD': {
       /* const {} = this.props;
        setDashboardQueryCall(value).then(()=>{
           // return searchInDashboardCompleteApiCall(text, 0);
        });
        console.warn('2222222222222222', value, currentRoute);*/
      }

      /*default: {
        setDashboardQueryCall('');
      }*/
    }
  }

  render() {
    const {
      logOut, 
      navigationMenuToogle
    } = this.props;

    const {isOpenSearch} = this.state;
    let inputSearchClasses = '';

    if (isOpenSearch) {
      inputSearchClasses = 'top-navbar-search-input-container';
    }

    return (
      <div className={'top-navbar-container'}>
        <div>
          <div className={'top-navbar-search-container'}>
            <button className="btn" onClick={navigationMenuToogle} id={'toogle-left-sidebar-button'}>
              <span className="icons ic-menu"/>
            </button>
            <div className={inputSearchClasses}>
              <input type={'text'}
                  id={'top-navbar-search-input'}
                  onKeyDown={this.handleKeyDown}
                  onChange={this.handleOnChange}
                  defaultValue={''} 
                  placeholder={'Search...'}
              />
            </div>
            <button className="btn" onClick={this.handleSearch} id={'top-navbar-search-button'}>
              <span className="icons ic-search"/>
            </button>
          </div>
        </div>
        <div>
          <div className="top-navbar-logo-image"/>
          <div className="top-navbar-report-navigation">
            <label className="top-navbar-report-navigation-title">
              Creative reports
            </label>
            <hr/>

            <button className="btn">
              <span className="icons ic-creative-report"/>
            </button>
          </div>
          <div className="top-navbar-logout">
            <button className="btn" onClick={logOut}>
              <span className="icons ic-logout"/>
            </button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const {
    user, 
    navigation
  } = state;

  const {isMobile} = navigation;
  return {
    user, 
    isMobile
  };
};

const mapDispatchToProps = ({
  logOut,
  navigationMenuToogle,
  setDashboardQueryCall
});

export default connect(mapStateToProps, mapDispatchToProps)(TopNavigation);
