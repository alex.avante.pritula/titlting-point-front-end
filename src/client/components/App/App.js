import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import {checkAuth} from '../../actions/User/Auth';
import {setMobileSizeStatus} from '../../actions/Navigation';

import TopNavigation from "../Navigation/TopNavigationNavBar.js";
import LeftSidebar from "../Navigation/LeftSidebar.js";

import {MOBILE_WIDTH} from "../../constants/General";

function mapStateToProps(state) {
  const {user, navigation} = state;
  const {isNavBarOpened, isMobile} = navigation;
  const {loggedIn} = user;
  return {
    isNavBarOpened,
    isMobile,
    loggedIn
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({checkAuth, setMobileSizeStatus}, dispatch);
}

@connect(mapStateToProps, mapDispatchToProps)
class App extends Component {

  static propTypes = {
    children: PropTypes.element,
    location: PropTypes.object.isRequired,
    loggedIn: PropTypes.boolean.isRequired,
    checkAuth: PropTypes.element.isRequired,
    isNavBarOpened: PropTypes.boolean.isRequired,
    setMobileSizeStatus: PropTypes.func.isRequired,
  };

  constructor(props){
      super(props);
      this.detectViewState = this.detectViewState.bind(this);
  }

  componentWillMount(){
     this.props.checkAuth();
     this.detectViewState({ target: window });
     window.addEventListener('resize', this.detectViewState);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.detectViewState);
  }

  detectViewState(e) {
    const { innerWidth } = e.target;
    const { setMobileSizeStatus } = this.props;
    if (innerWidth < MOBILE_WIDTH) { setMobileSizeStatus(true);} 
    else { setMobileSizeStatus(false);}
  }

  render(){
     const { children, isNavBarOpened, loggedIn, location } = this.props;
     return (
        <div className="tgpoint--container">
          <div className="tgpoint--main">
            <section className="tgpoint--main-head">
              <TopNavigation currentRoute={location.pathname}/>
            </section>
            <section className={classnames('tgpoint--main-content', !loggedIn ? 'not-authorized' : null)}>
              {children}
            </section>
          </div>
          <div className={classnames('tgpoint-navigation', isNavBarOpened ? 'mobile-opened' : null)}>
            <section className="side-bar">
              <LeftSidebar currentRoute={location.pathname}/>
            </section>
          </div>
          { loggedIn ? null : (<div className="interface-block-container"/>) }
        </div>
      );
  }
}


export default App;
