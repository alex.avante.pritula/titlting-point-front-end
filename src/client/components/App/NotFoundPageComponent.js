import React, { Component } from 'react';

class NotFoundPageComponent extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <div><h4>404 Page not found</h4></div>;
  }
}

export default NotFoundPageComponent;
