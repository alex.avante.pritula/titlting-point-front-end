import PropTypes from "prop-types";
import Dropzone from "react-dropzone";
import ReactPlayer from "react-player";
import React, {Component} from "react";

import {
    UPLOAD_TYPES,
    UPLOAD_MAX_SIZE,
    TILTING_POINT_AWS_URL,
    DASHBOARD_BLOCK_ITEM_TYPES
} from '../../constants/General';

class DropZoneContainer extends Component {

    static propTypes = {
        disabled: PropTypes.boolean.isRequired,
        onChangeData: PropTypes.func.isRequired,
        uploadType: PropTypes.string.isRequired,
        defaultPreview: PropTypes.string.isRequired,
        previewContent: PropTypes.string.isRequired
    };

  constructor(props){
      super(props);
      this.state = {
        file: null,
        error: null,
      };
      this.handleOnDrop = this.handleOnDrop.bind(this);
      this.onDropRejected = this.onDropRejected.bind(this);
  }

  componentWillReceiveProps(nextProps){
     const {file} = this.state;
     const {defaultPreview} = nextProps;
     if(defaultPreview && file){ this.setState({file: null}); }
  }

  handleOnDrop(acceptedFiles) {
     const file = acceptedFiles[0];
     this.setState({
         error: null,
         file
     });
     const {onChangeData} = this.props;
     onChangeData(file);
  }

  onDropRejected(acceptedFiles){
    const file = acceptedFiles[0];
    const {size} = file;
   
    if(size > UPLOAD_MAX_SIZE){
      this.setState({ error: "Warning: File size is more than 50mb" });
    } else {
      this.setState({ error: "Drop rejected error" });     
    }
  }

  render(){
      const {
        file, 
        error
      } = this.state;

      const {
        disabled, 
        uploadType, 
        previewContent,
        defaultPreview
      } = this.props;
      
      const {accept} = UPLOAD_TYPES[uploadType];
      let defaultPreviewComponent = null;
      let title = (error ? <label className="drop-zone-error-message">{error}</label>: previewContent);

      if (defaultPreview) {
        switch(uploadType){
            case 'IMAGE': {
                let styles = { backgroundImage: `url(${defaultPreview})` };
                defaultPreviewComponent = (<span className="drop-zone-preview-image" style={styles} />);
                break;
            }
            case 'VIDEO':{
                defaultPreviewComponent = (
                    <ReactPlayer className="drop-zone-preview-video" 
                     url={defaultPreview} controls={true} />
                );
                break;
            }

            case 'NEW_DOCUMENT': {
                const {preview} = DASHBOARD_BLOCK_ITEM_TYPES['NEW_DOCUMENT'];
                let previewItem = preview['default'];

                let ownLinkMarker = defaultPreview.indexOf(TILTING_POINT_AWS_URL);

                if (ownLinkMarker !== -1) {
                    let typeMarkerIndex = defaultPreview.lastIndexOf('.');
                    if (typeMarkerIndex !== -1 && typeMarkerIndex !== defaultPreview.length - 1){
                        let documentType = defaultPreview.substring(typeMarkerIndex + 1).toLowerCase();
                        let tempPreview  = preview[documentType];
                        if(tempPreview){ previewItem = tempPreview; }
                    }
                }
                defaultPreviewComponent = (<span className={`icons-file-manage default ${previewItem}`} />);
            }
        }
      } else if (file) {
        switch(uploadType){
            case 'IMAGE': {
                const {preview} = file;
                let styles = { backgroundImage: `url(${preview})` };
                defaultPreviewComponent = (<span className="drop-zone-preview-image" style={styles} />);
                break;
            }
            case 'VIDEO': {
                const {preview} = file;
                defaultPreviewComponent = (
                    <ReactPlayer className="drop-zone-preview-video" 
                      controls={true} url={preview} />
                );
                break;
            }

            case 'NEW_DOCUMENT': { 
                const {type} = file;
                let {preview} = DASHBOARD_BLOCK_ITEM_TYPES['NEW_DOCUMENT'];
                let previewItem = preview['default'];
                let typeName = type.substring(type.indexOf('/') + 1).toLowerCase();
                let tempPreview =  preview[typeName];
                if (tempPreview) { previewItem = tempPreview; }
                defaultPreviewComponent = (<span className={`icons-file-manage default ${previewItem}`} />);
            }
        }
    }

      return (
        <div className={'dropzone-container'}>
            <div className={'drop-zone-preview-container'}  onClick={()=>{ if(!disabled) { this.dropzoneRef.open();}  }}>
              <div className={'drop-zone-preview-border-box left-top'}/>
              <div className={'drop-zone-preview-border-box right-top'}/>
              <div className={'drop-zone-preview-border-box left-bottom'}/>
              <div className={'drop-zone-preview-border-box right-bottom'}/>
              <div className={'drop-zone-preview-text-description'}> 
                  {defaultPreviewComponent ? null : title}
              </div>
                 <Dropzone className={'drop-zone-component'}
                    ref={(node) => { this.dropzoneRef = node; }} 
                    accept={accept}
                    multiple={false}
                    disabled={disabled}
                    disableClick={true}
                    maxSize={UPLOAD_MAX_SIZE}
                    onDrop={this.handleOnDrop}
                    onDropRejected={this.onDropRejected} > {
                        defaultPreviewComponent ?  defaultPreviewComponent :
                        <span className="drop-zone-preview-default-image" />
                    }
                 </Dropzone>
           </div>
        </div>
      );
  }
}

export default DropZoneContainer;
