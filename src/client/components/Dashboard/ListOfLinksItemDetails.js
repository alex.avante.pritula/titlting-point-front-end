import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {addLinkToListOfLinks}  from '../../actions/Dashboard/Item';

import ListOfLinks from './ListOfLinks';

function mapStateToProps(state) {
  const {item} = state;
  const {isEditing} = item;
  return {isEditing};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({addLinkToListOfLinks}, dispatch);
}

@connect(mapStateToProps, mapDispatchToProps)
class ListOfLinksItemDetails extends Component {

    static propTypes = {
        data: PropTypes.object.isRequired,
        isEditing: PropTypes.boolean.isRequired,
        addLinkToListOfLinks: PropTypes.func.isRequired
    };

    constructor(props){
       super(props);
        this.handleAddLinkItem = this.handleAddLinkItem.bind(this);
    }

    handleAddLinkItem(){
        const {addLinkToListOfLinks} = this.props;
        addLinkToListOfLinks();
    }

    render(){
      const {isEditing} = this.props;
      let addLinkModalButton = null;
      if (isEditing) {
        addLinkModalButton = (
         <div className="list-of-links-item-details-container__add_link_container">
            <div className="list-of-links-item-details-container__add_link_container__text">
              <p>Add</p>
              <p>New Link</p>
              <button onClick={this.handleAddLinkItem}>
                <span className="icons ic-add-item" />
              </button>
            </div>
          </div>
        );
      }
      return (
        <div className="list-of-links-item-details-container">
          <div><ListOfLinks /></div>
          {addLinkModalButton}
        </div>
      );
    }
}

export default ListOfLinksItemDetails;
