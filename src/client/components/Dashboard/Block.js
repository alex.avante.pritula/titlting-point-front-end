import classNames from 'classnames';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {findDOMNode} from 'react-dom';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {DragSource, DropTarget} from 'react-dnd';
import {MenuItem} from 'react-mdl-extra';

import RemoveBlockToolTip from './RemoveBlockToolTip';
import MainActionButton from '../UIComponents/MainActionButton';
import ScrollableItemsContainer from './ScrollableItemsContainer';

import {BLOCK_DRAGGABLE_TYPE} from '../../constants/Dashboard';
import {ENTER,DASHBOARD_BLOCK_ITEM_TYPES} from '../../constants/General';

import {getDashboard} from '../../actions/Dashboard/Dashboard';
import {handleOpenItemDialog}  from '../../actions/Dashboard/Item';
import {updateBlock, removeBlock, updateBlockTitle} from '../../actions/Dashboard/Block';

const blockSource = {

  beginDrag(props) {
    const {
      id,
      index,
      position,
      startDraggingBlock
    } = props;

    startDraggingBlock(position);
    return {id, index};
  },

  endDrag(props){
    const {
      id,
      index,
      refreshBlocks,
      endDraggingBlock,
      blockDraggablePosition
    } = props;

    let newPosition = index + 1;
    let needToChange = (blockDraggablePosition() !== newPosition);

    endDraggingBlock(id, newPosition, needToChange)
      .then(()=>{ 
        if(needToChange){ 
          refreshBlocks(); 
        } 
      });
  }
};

const blockTarget = {
  hover(props, monitor, component) {
    const monitorItem = monitor.getItem();
    if(monitorItem.blockIndex != null) { return; }
    const dragIndex = monitorItem.index;
    const hoverIndex = props.index;
    if (dragIndex === hoverIndex) { return; }
    const hoverBoundingRect = findDOMNode(component).getBoundingClientRect();
    const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;
    const clientOffset = monitor.getClientOffset();
    const hoverClientY = clientOffset.y - hoverBoundingRect.top;
    if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) { return; }
    if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) { return; }
    props.moveBlock(dragIndex, hoverIndex);
    monitorItem.index = hoverIndex;
  }
};

function mapStateToProps(state) {
    const {dashboard} = state;
    const {isFetching, blocks, id} = dashboard;
    return {
      blocks,
      isFetching,
      dashboardId: id
    };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    updateBlock,
    removeBlock,
    getDashboard,
    updateBlockTitle,
    handleOpenItemDialog
  }, dispatch);
}

@DropTarget(BLOCK_DRAGGABLE_TYPE, blockTarget, connect => ({
  connectDropTarget: connect.dropTarget(),
}))
@DragSource(BLOCK_DRAGGABLE_TYPE, blockSource, (connect, monitor) => ({
  connectDragSource: connect.dragSource(),
  isDragging: monitor.isDragging(),
}))
@connect(mapStateToProps, mapDispatchToProps)
class Block extends Component {

   static propTypes = {
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
      items: PropTypes.array.isRequired,
      index: PropTypes.number.isRequired,
      blocks: PropTypes.array.isRequired,
      moveBlock: PropTypes.func.isRequired,
      position: PropTypes.number.isRequired,
      isDragging: PropTypes.bool.isRequired,
      updateBlock: PropTypes.func.isRequired,
      removeBlock: PropTypes.func.isRequired,
      getDashboard: PropTypes.func.isRequired,
      refreshBlocks: PropTypes.func.isRequired,
      dashboardId: PropTypes.number.isRequired,
      isFetching: PropTypes.boolean.isRequired,
      blockTypeId: PropTypes.number.isRequired,
      updateBlockTitle: PropTypes.func.isRequired,
      endDraggingBlock: PropTypes.func.isRequired,
      connectDragSource: PropTypes.func.isRequired,
      connectDropTarget: PropTypes.func.isRequired,
      startDraggingBlock: PropTypes.func.isRequired,
      handleOpenItemDialog: PropTypes.func.isRequired,
      blockDraggablePosition: PropTypes.func.isRequired
  };

  constructor(props){
    super(props);
    this.state = {
      onFocus: false,
      openModal: false,
      openRemoveModal: false,
      dropdownMenuOpened: false
    };

    this.blockRemove = this.blockRemove.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.handleEditMode = this.handleEditMode.bind(this);
    this.handleSelected = this.handleSelected.bind(this);
    this.openDropdownMenu = this.openDropdownMenu.bind(this);
    this.closeDropdownMenu = this.closeDropdownMenu.bind(this);
    this.openDropdownMenu = this.openDropdownMenu.bind(this);
    this.closeDropdownMenu = this.closeDropdownMenu.bind(this);
    this.handleBlockRemove = this.handleBlockRemove.bind(this);
    this.handleOpenNavigation = this.handleOpenNavigation.bind(this);
    this.handleUpdateBlockTitle = this.handleUpdateBlockTitle.bind(this);
    this.handleNavigationClickOutside = this.handleNavigationClickOutside.bind(this);
    this.emitClick = this.emitClick.bind(this);
  }

  componentDidMount() {
    this.menuContainer.addEventListener('click', this.openDropdownMenu);
  }

  componentWillUnmount() {
    document.body.removeEventListener('click', this.closeDropdownMenu);
    clearTimeout(this.closeMenuTimeout);
    clearTimeout(this.closeMenuTimeout);
  }

  handleUpdateBlockTitle(evt){
     const {value} = evt.target;
     const {index, updateBlockTitle} = this.props;
     updateBlockTitle(index, value);
  }

  handleKeyDown(event){
     if(event.keyCode == ENTER){
       const {value} = event.target;
       const {
         id,
         blocks,
         updateBlock,
         dashboardId,
         blockTypeId,
         getDashboard
       } = this.props;

       updateBlock(id, value, dashboardId, blockTypeId).then(()=>{
          getDashboard(dashboardId, true, 0, blocks.length);
       });
     }
  }

  handleEditMode(onFocus){
    this.setState({onFocus: onFocus});
  }

  handleBlockRemove(){
    const {openRemoveModal} = this.state;
    this.setState({openRemoveModal: (!openRemoveModal)});
  }

  blockRemove(){
    const {
      id,
      blocks,
      removeBlock,
      dashboardId,
      getDashboard
    } = this.props;

    removeBlock(id).then(()=>{
       getDashboard(dashboardId, true, 0, blocks.length);
       this.setState({openRemoveModal: false});
    });
  }

  handleOpenNavigation(){
    this.setState({openModal: !this.state.openModal});
  }

  handleNavigationClickOutside(){
    this.setState({openModal: false});
  }

  handleSelected(itemTypeId){
    this.setState({openModal: false});
    const {id, handleOpenItemDialog} = this.props;
    handleOpenItemDialog(id, itemTypeId);
  }

  openDropdownMenu() {
    this.setState({ dropdownMenuOpened: true });
    this.closeMenuTimeout = setTimeout(() => {
      this.menuContainer.removeEventListener('click', this.openDropdownMenu);
      document.body.addEventListener('click', this.closeDropdownMenu);
    }, 100);
  }

  emitClick() {
    this.menuContainer.click();
  }

  closeDropdownMenu() {
    document.body.removeEventListener('click', this.closeDropdownMenu);
    this.closeMenuTimeout = setTimeout(() => {
      this.menuContainer.addEventListener('click', this.openDropdownMenu);
      this.setState({ dropdownMenuOpened: false });
    }, 100);
  }

  render() {
    const {
      id,
      name,
      index,
      isDragging,
      isFetching,
      connectDragSource,
      connectDropTarget
    } = this.props;

    const {
      onFocus,
      openRemoveModal
    } = this.state;

    let menuItems = Object.keys(DASHBOARD_BLOCK_ITEM_TYPES).map((item)=>{
      const data = DASHBOARD_BLOCK_ITEM_TYPES[item];
      return (
        <MenuItem className="tgpoint--dashboard-dropdown-item"
          key={data.id} value={data.id}
          closeMenu={()=>{ }}
          onClick={() => this.handleSelected(data.id)}>
          {data.name}
        </MenuItem>
      );
    });

    const isDisabled = {opacity: (isDragging ? 0 : 1)};
    const {dropdownMenuOpened} = this.state;

    let menuContainer = 'icons clickable ic-add';
    let removeToolTip = null;
    if (openRemoveModal) {
     removeToolTip = (
        <RemoveBlockToolTip name={name}
            onClose={this.handleBlockRemove}
            isFetching={isFetching}
            sectionTitle={'block'}
            onRemove={this.blockRemove}/>
     );
    }

    if (dropdownMenuOpened) {
      menuContainer += ' opened';
    }

    return connectDragSource(connectDropTarget(
      <div style={isDisabled} className={'block-container'}>
          <div className={'block-header'}>
            <div className="block-header-item">
              <MainActionButton className="draggable" onClick={this.handleBlockDrag}>
                <span className="icons clickable ic-arrows-inactive" />
              </MainActionButton>
              <input
                type={'text'}
                value={name}
                className={classNames('block-header-title-input', onFocus ? 'focused' : null)}
                placeholder={'Enter block title'}
                onKeyDown={this.handleKeyDown}
                onChange={this.handleUpdateBlockTitle}
                onFocus={()=>{this.handleEditMode(true);}}
                onBlur={()=>{this.handleEditMode(false);}}
              />
            </div>
            <div className="block-header-item">
              <MainActionButton onClick={this.handleBlockRemove}>
                <span className="icons clickable ic-delete-inactive" />
              </MainActionButton>
              {removeToolTip}
              <span className={menuContainer} ref={(div) => { this.menuContainer = div; }}>
                <ul className="block_add_items_list">
                  {menuItems}
                </ul>
              </span>
            </div>
          </div>
          <ScrollableItemsContainer
            id={id} index={index}
            openDropdownMenu={this.emitClick}
            handleOpenNavigation={this.handleOpenNavigation} />
      </div>
    ));
  }
}

export default Block;
