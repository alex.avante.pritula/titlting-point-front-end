import PropTypes from "prop-types";
import {connect} from "react-redux";
import {findDOMNode} from "react-dom";
import ReactPlayer from "react-player";
import React, {Component} from "react";
import {bindActionCreators} from "redux";
import {DragSource, DropTarget} from "react-dnd";

import {
  TILTING_POINT_AWS_URL,
  DASHBOARD_BLOCK_ITEM_TYPES
} from '../../constants/General';
import {BLOCK_DRAGGABLE_TYPE} from '../../constants/Dashboard';
import {handleOpenItemDialog}  from '../../actions/Dashboard/Item';
const ITEM_TYPES_NAMES = Object.keys(DASHBOARD_BLOCK_ITEM_TYPES);

const blockSource = {
  beginDrag(props) {
    const {
      id,
      index,
      blockId,
      position,
      blockIndex,
      startDraggingItem
    } = props;

    startDraggingItem(position);
    return {
      id,
      index,
      blockId,
      blockIndex
    };
  },

  endDrag(props){
    const {
      id,
      index,
      refreshBlocks,
      endDraggingItem,
      itemDraggablePosition
    } = props;

    let newPosition = index + 1;
    let needToChange = (itemDraggablePosition() !== newPosition);

    endDraggingItem(id, newPosition, needToChange)
      .then(()=>{
        if(needToChange){
          refreshBlocks();
        }
      });
  }
};

const blockTarget = {
  hover(props, monitor, component) {
    const monitorItem = monitor.getItem();
    const dragIndex = monitorItem.index;
    const dragBlockIndex = monitorItem.blockIndex;
    const {blockIndex, index} = props;
    if (dragBlockIndex === null){ return;}
    if (dragIndex === index) { return; }
    if (dragBlockIndex !== blockIndex){ return; }
    const currentItemRect = findDOMNode(component).getBoundingClientRect();
    const clientOffset = monitor.getClientOffset();
    const hoverMiddleX = (currentItemRect.right - currentItemRect.left) / 2;
    const hoverClientX = clientOffset.x - currentItemRect.left;
    if (dragIndex < index && hoverClientX < hoverMiddleX) { return; }
    if (dragIndex > index && hoverClientX > hoverMiddleX) { return; }
    props.moveItem(dragIndex, index, blockIndex);
    monitorItem.index = index;
  },
};

function mapStateToProps() { return {}; }

function mapDispatchToProps(dispatch) {
  return bindActionCreators({handleOpenItemDialog}, dispatch);
}

@DropTarget(BLOCK_DRAGGABLE_TYPE, blockTarget, connect => ({
  connectDropTarget: connect.dropTarget(),
}))
@DragSource(BLOCK_DRAGGABLE_TYPE, blockSource, (connect, monitor) => ({
  connectDragSource: connect.dragSource(),
  isDragging: monitor.isDragging(),
}))
@connect(mapStateToProps, mapDispatchToProps)
class Item extends Component {

  static propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    item: PropTypes.object.isRequired,
    index: PropTypes.number.isRequired,
    moveItem: PropTypes.func.isRequired,
    blockId: PropTypes.number.isRequired,
    isDragging: PropTypes.bool.isRequired,
    itemTypeId: PropTypes.number.isRequired,
    blockIndex: PropTypes.number.isRequired,
    description: PropTypes.string.isRequired,
    endDraggingItem: PropTypes.func.isRequired,
    connectDragSource: PropTypes.func.isRequired,
    connectDropTarget: PropTypes.func.isRequired,
    startDraggingItem: PropTypes.func.isRequired,
    handleOpenItemDialog: PropTypes.func.isRequired
  };

  constructor(props){
    super(props);
    this.handleOpenModalDialog = this.handleOpenModalDialog.bind(this);
  }

  handleOpenModalDialog(){
     const {
       id, 
       index,
       blockId,
       blockIndex,
       itemTypeId, 
       handleOpenItemDialog
    } = this.props;

    handleOpenItemDialog(
      blockId, 
      itemTypeId, 
      id, 
      blockIndex, 
      index
    );
  }

  render() {
    const {
      name,
      item,
      itemTypeId,
      isDragging,
      connectDragSource,
      connectDropTarget
    } = this.props;

    const isDisabled = { opacity: isDragging ? 0 : 1 };
    let itemPreview = null;

    let type = ITEM_TYPES_NAMES.find((key)=>{
        let {id} = DASHBOARD_BLOCK_ITEM_TYPES[key];
        return (itemTypeId === id);
    });

    if(['NEWS_BLOCK','TEXTAREA','LIST_OF_LINKS'].indexOf(type) !== -1) {
        const {preview} = DASHBOARD_BLOCK_ITEM_TYPES[type];
        itemPreview = (<span className={preview} />);
    }

    if(type === 'NEW_DOCUMENT') {
      const {links} = item;
      const {url} = links[0]; 

      const {preview} = DASHBOARD_BLOCK_ITEM_TYPES[type];
      let previewItem = preview['default'];

      let ownLinkMarker = url.indexOf(TILTING_POINT_AWS_URL);

      if (ownLinkMarker !== -1) {
          let typeMarkerIndex = url.lastIndexOf('.');
          if (typeMarkerIndex !== -1 && typeMarkerIndex !== url.length - 1){
              let documentType = url.substring(typeMarkerIndex + 1).toLowerCase();
              let tempPreview  = preview[documentType];
              if(tempPreview){ previewItem = tempPreview; }
          }
      }
      itemPreview = (<span className={`icons-file-manage default ${previewItem}`} />);
    }

    if(type === 'VIDEO') {
      const {links} = item;
      const {url} = links[0];
      itemPreview = (<ReactPlayer className="block-item-video-preview" controls={true}  url={url} />);
    }

    if(type === 'IMAGE_LINK') {
      const {links} = item;
      const {url} = links[0];
      itemPreview = (<img src={url} />);
    }

    return connectDragSource(connectDropTarget(
      <div onClick={this.handleOpenModalDialog} 
          style={isDisabled} className="block-item-container">
          <div className="block-item-container-icon">
               {itemPreview}
            </div>
          <div className="block-item-container-description">
              <label className="block-item-container-name" >{name}</label>
          </div>
        </div>
    ));
  }
}

export default Item;
