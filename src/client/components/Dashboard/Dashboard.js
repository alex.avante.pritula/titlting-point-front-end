import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import React, {Component} from 'react';
import PropTypes from 'prop-types';

import CreateBlockToolTip from './CreateBlockToolTip';
import ItemDescriptionContainer from './ItemDescriptionContainer';
import DraggableBlocksContainer from './DraggableBlocksContainer';
import MainActionButton from '../UIComponents/MainActionButton';

import {LIMIT} from '../../constants/General';
import {getDashboard} from '../../actions/Dashboard/Dashboard';

function mapStateToProps() { return {}; }
function mapDispatchToProps(dispatch) {
  return bindActionCreators({getDashboard}, dispatch);
}

@connect(mapStateToProps, mapDispatchToProps)
class Dashboard extends Component {

    constructor(props){
        super(props);
        this.state = {isOpened: false};
        this.handleOpen = this.handleOpen.bind(this);
        this.onCreated = this.onCreated.bind(this);
    }

    componentWillMount(){
        const {
            params,
            getDashboard
        } = this.props;

        const {id} = params;
        getDashboard(id, true, 0, LIMIT);
    }

    handleOpen(){
       const {isOpened} = this.state;
       this.setState({isOpened: !isOpened});
    }

    onCreated(){
        this.setState({isOpened: false});
    }

    render(){
        let ToolTip = null;
        const {isOpened} = this.state;
        if (isOpened){ ToolTip = (<CreateBlockToolTip onCreated={this.onCreated}/>); }
        return (
            <div className={'dashboard-container'}>
                 <div className={'dashboard-header'}>
                    <div className={'dashboard-header-title'}>
                        <span>Welcome to</span>
                        <span className={'dashboard-desctop-additional-title'}>the TILTING POINT company</span>
                        <label>DASHBOARD</label>
                    </div>
                    <div>
                        <MainActionButton action={this.handleOpen}>ADD NEW ITEM</MainActionButton>
                        {ToolTip}
                    </div>
                </div>
               <ItemDescriptionContainer />
               <DraggableBlocksContainer />
            </div>
        );
    }
}


Dashboard.propTypes = {
   id: PropTypes.number.isRequired,
   params: PropTypes.object.isRequired,
   getDashboard: PropTypes.func.isRequired
};

export default Dashboard;
