import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import DropZoneContainer from '../General/DropZoneContainer';

import {
    uploadItemFile,
    changeItemValue
}  from '../../actions/Dashboard/Item';

function mapStateToProps(state) {
  const {item} = state;
  const {
    isEditing,
    selectedItemData, 
    selectedItemId
  } = item;

  return {
      isEditing,
      id: selectedItemId,
      data: selectedItemData
    };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
      uploadItemFile,
      changeItemValue
  }, dispatch);
}

@connect(mapStateToProps, mapDispatchToProps)
class VideoEmdedeItemDetails extends Component {

    static propTypes = {
       id: PropTypes.object.isRequired,
       data: PropTypes.object.isRequired,
       isEditing: PropTypes.boolean.isRequired,
       uploadItemFile: PropTypes.func.isRequired,
       changeItemValue: PropTypes.func.isRequired
    };

    constructor(props){
       super(props);
       this.handleVideoLinkChanged = this.handleVideoLinkChanged.bind(this);
       this.handleDropDownNewVideo = this.handleDropDownNewVideo.bind(this);
       this.handleRemoveVideoLinkMode = this.handleRemoveVideoLinkMode.bind(this);
       this.handleVideoDescriptionChanged = this.handleVideoDescriptionChanged.bind(this);
       this.handleRemoveVideoDescriptionMode = this.handleRemoveVideoDescriptionMode.bind(this);
    }

    handleVideoLinkChanged(evt){
       const {value} = evt.target;
       const {changeItemValue} = this.props;
       changeItemValue('url', value);
    }

    handleVideoDescriptionChanged(evt){
       const {value} = evt.target;
       const {changeItemValue} = this.props;
       changeItemValue('linkDescription', value);
    }

    handleRemoveVideoLinkMode() {
       const {changeItemValue} = this.props;
       changeItemValue('url', '');
    }

    handleRemoveVideoDescriptionMode() {
       const {changeItemValue} = this.props;
       changeItemValue('linkDescription', '');
    }

    handleDropDownNewVideo(file){
       const {uploadItemFile} = this.props;
       uploadItemFile(file);
    }

    render(){
        const {isEditing, data} = this.props;
        const {url , linkDescription, validationErrors} = data; 
        let urlValidationClass = (validationErrors.indexOf('url') === -1) ? 'valid-input': 'invalid-input';

        let editVideoUrl = null;
        let removeVideoUrl = null;
        let editVideoDescription = null;
        let removeVideoDescription = null;

        if(isEditing){

          editVideoUrl = (
             <button>
                <span className="icons ic-edit-active" />
              </button>
          );

          removeVideoUrl = (
              <button onClick={this.handleRemoveVideoLinkMode} >
                <span className="icons ic-delete-active" />
              </button>
          );

          editVideoDescription = (
              <button>
                   <span className="icons ic-edit-active" />
              </button>
          );

          removeVideoDescription  = (
              <button onClick={this.handleRemoveVideoDescriptionMode} >
                  <span className="icons ic-delete-active" />
               </button>
          );         
                    
        }
      
        const videoLinkEditable = (
          <div className="video-embeded-item-details-description__link_container">
            <div className="video-embeded-item-details-description__link_container__text_button_container">
              <p>Enter link for video here:</p>
              {editVideoUrl}
              {removeVideoUrl}
            </div>
            <div className="video-embeded-item-details-description__link_container__link_input_container">
              <textarea
                name="videoInputLink"
                value={url}
                placeholder={'https://www.example.com'}
                onChange={this.handleVideoLinkChanged}
                className={urlValidationClass}
              />
            </div>
          </div>
        );
        const videoLinkNonEditable = (
          <div className="video-embeded-item-details-description__link_container non-editable">
            <div className="video-embeded-item-details-description__link_container__text_button_container">
              <p>Embedded link:</p>
            </div>
            <div className="video-embeded-item-details-description__link_container__link_input_container">
              <a target="_blank" rel="noopener noreferrer" href={url}>{url}</a>
            </div>
          </div>
        );
        const videoLinkDescription = (
          <div className="video-embeded-item-details-description__description_container">
            <div className="video-embeded-item-details-description__description_container__text_button_container">
              <p>Add Description to video:</p>
              {editVideoDescription}
              {removeVideoDescription}
            </div>
            <div className="video-embeded-item-details-description__description_container__description_input__container">
              <textarea
                name="videoInputLink" value={linkDescription}
                onChange={this.handleVideoDescriptionChanged}
                placeholder={'Some description about video'}
             />
            </div>
          </div>
        );
        const videoLinkDescriptionNonEditable = (
          <div className="video-embeded-item-details-description__description_container non-editable">
            <div className="video-embeded-item-details-description__description_container__text_button_container">
              <p>Description to video:</p>
            </div>
            <div className="video-embeded-item-details-description__description_container__description_input__container">
              <p>{linkDescription}</p>
            </div>
          </div>
        );
        return (
            <div className={'video-embeded-item-details-container'}>
              <div className="video-embeded-item-dropzone-section">
                <div className={'video-embeded-item-dropzone'}>
                  <DropZoneContainer uploadType={'VIDEO'} 
                    previewContent={
                      <span>
                          Video box
                          <br/>
                          drag and drop
                          <br/>
                          to add video
                       </span>
                    }
                    defaultPreview={url} disabled={!isEditing}
                    onChangeData={this.handleDropDownNewVideo} />
                </div>
              </div> 
              <div className="video-embeded-item-details-description">
                {isEditing ? videoLinkEditable : videoLinkNonEditable}
                {isEditing ? videoLinkDescription : videoLinkDescriptionNonEditable}
              </div> 
            </div>
        );
    }
}

export default VideoEmdedeItemDetails;
