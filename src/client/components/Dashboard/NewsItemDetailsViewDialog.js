import moment from 'moment';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';

import {
    setEditingStatus,
    handleOpenItemDialog,
    handleCloseItemDialog
} from '../../actions/Dashboard/Item';

function mapStateToProps(state) {
  const {item, dashboard} = state;
  const {blocks} = dashboard;

  const {   
    itemIndex,
    blockIndex,
    isEditing,  
    isFetching,
    selectedItemId,  
    selectedItemData
  } = item;

  let nextItemId = -1;
  let prevItemId = -1;
  let nextItemTypeId = -1;
  let prevItemTypeId = -1;
  const currentBlock = blocks[blockIndex];

  if (currentBlock) {
    const {items} = currentBlock;
    if(items.length > 0) {

        if(itemIndex !== 0){ 
          let prevItem = items[itemIndex - 1];
          prevItemId = prevItem.id;
          prevItemTypeId = prevItem.itemTypeId;
        }

        if(itemIndex !== items.length - 1) {
          let nextItem = items[itemIndex + 1];
          nextItemId = nextItem.id;
          nextItemTypeId = nextItem.itemTypeId;
        }
    }
  }
  return {
      isEditing,
      isFetching,
      nextItemId,
      prevItemId,
      itemIndex,
      blockIndex,
      nextItemTypeId,
      prevItemTypeId,
      id: selectedItemId,
      data: selectedItemData
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
      setEditingStatus,
      handleOpenItemDialog,
      handleCloseItemDialog
    }, dispatch);
}

class NewsItemDetailsViewDialog extends Component {

    static propTypes = {
      id: PropTypes.number.isRequired,
      data: PropTypes.object.isRequired,
      blockId: PropTypes.number.isRequired,
      itemIndex: PropTypes.number.isRequired,
      blockIndex: PropTypes.number.isRequired,
      nextItemId: PropTypes.number.isRequired,
      prevItemId: PropTypes.number.isRequired,
      isEditing: PropTypes.boolean.isRequired,
      isFetching: PropTypes.boolean.isRequired,
      setEditingStatus: PropTypes.func.isRequired,
      nextItemTypeId: PropTypes.number.isRequired,
      prevItemTypeId: PropTypes.number.isRequired,
      handleOpenItemDialog: PropTypes.func.isRequired,
      handleCloseItemDialog: PropTypes.func.isRequired
    };

    constructor(props){
       super(props);
       this.handleEditMode = this.handleEditMode.bind(this);
       this.handleClickPrev = this.handleClickPrev.bind(this);
       this.handleClickNext = this.handleClickNext.bind(this);
       this.handleSendReport = this.handleSendReport.bind(this);
       this.handleClickOutside = this.handleClickOutside.bind(this);
    }
   
    componentDidMount() {
      document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
       document.removeEventListener('mousedown', this.handleClickOutside);
    }

    handleClickOutside(event){
        const {target} = event;
        const {isFetching, handleCloseItemDialog} = this.props;

        if (!isFetching && this.modalDialog && !this.modalDialog.contains(target)) { 
            handleCloseItemDialog();
        }
    }

    handleEditMode(){
        const {
          isEditing,
          setEditingStatus
        } = this.props;

        setEditingStatus(!isEditing);
    }

    handleSendReport(){

    }

    handleClickNext(){
      const {
        blockId,
        blockIndex, 
        nextItemId,
        itemIndex,
        nextItemTypeId,
        handleOpenItemDialog
      } = this.props;

      handleOpenItemDialog(
        blockId, 
        nextItemTypeId, 
        nextItemId, 
        blockIndex, 
        itemIndex + 1
      ); 

    }

    handleClickPrev(){
        const {
            blockId,
            blockIndex, 
            prevItemId,
            itemIndex,
            prevItemTypeId,
            handleOpenItemDialog
        } = this.props;

        handleOpenItemDialog(
            blockId, 
            prevItemTypeId, 
            prevItemId, 
            blockIndex, 
            itemIndex - 1
        );
    }

    render(){

        const {
          data,
          prevItemId,
          nextItemId,
        } = this.props;

        const {
          name, 
          ownerName,
          ownerRole,
          createdAt,
          lastImageUrl,
          firstImageUrl,
          newsDescription
        } = data;

        let date = moment(createdAt).format('YYYY/MM/DD');
        let hasPrev = (prevItemId !== -1);
        let hasNext = (nextItemId !== -1);

        return (
            <div className="news-item-details-view-modal-body">
                <div ref={(node)=>{this.modalDialog = node;}}
                     className="news-item-details-view-modal-container">
                    <div className="news-item-details-view-modal-header">
                        <div className="news-item-header-section-left">
                            <span className="icons ic-news-inactive" />
                            <div>
                                <label>{name}</label>
                                <span>date: {date}</span>
                            </div>
                        </div>
                        <div className="news-item-header-section-right">
                            <button onClick={this.handleEditMode}>
                                <span className="icons ic-edit-active" />
                            </button>
                            <button onClick={this.handleSendReport}>
                                <span className="icons ic-send-invite" />
                            </button>
                        </div>
                    </div>
                    <div className="news-item-details-view-modal-content">  
                        <div className="news-item-details-image-container image-left">
                            <img src={firstImageUrl}/>
                        </div>
                        <div className="news-item-author-information">
                            <label>Article title: {name}</label>
                            <div>
                                <span>Added by: {ownerName} {ownerRole} </span>
                                <span>{date}</span>
                            </div>
                            <hr />
                        </div>  
                         <p>
                           <div className="news-item-details-image-container image-right">
                                <img src={lastImageUrl} />
                            </div> 
                            <span dangerouslySetInnerHTML={{ __html: newsDescription}} />
                         </p>      
                    </div>
                    <div className="news-item-details-view-modal-footer">
                        <button style={{opacity: (hasPrev ? 1: 0)}} 
                            disabled={!hasPrev} onClick={this.handleClickPrev}>
                            <span className="icons ic-prev-inactive" />
                        </button>
                        <button style={{opacity: (hasNext ? 1: 0)}} 
                           disabled={!hasNext} onClick={this.handleClickNext}>
                            <span className="icons ic-next-inactive" />
                        </button>
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewsItemDetailsViewDialog);