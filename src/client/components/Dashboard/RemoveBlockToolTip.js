import PropTypes from 'prop-types';
import { Spinner } from 'react-mdl';
import React, {Component } from 'react';

class RemoveBlockToolTip extends Component {

    static propTypes = {
      name: PropTypes.string.isRequired,
      onClose: PropTypes.func.isRequired,
      onRemove : PropTypes.func.isRequired,
      sectionTitle:  PropTypes.name.isRequired,
      isFetching: PropTypes.boolean.isRequired
    }

    constructor(props){
        super(props);
        this.handleClickOutside = this.handleClickOutside.bind(this);
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }
    
    handleClickOutside(event){
        const {target} = event;
        const {onClose, isFetching} = this.props;
        if (!isFetching && this.RemoveToolTip && 
            !this.RemoveToolTip.contains(target)) {  
            onClose(); 
        }
    }

    render(){
        const {
          name,
          onClose,
          onRemove,
          isFetching,
          sectionTitle,
        } = this.props;

        let title = (!name) ?  '' : `"${name}"`;
        return (
            <div ref={(node)=>{this.RemoveToolTip = node;}} 
                className="remove-block-tooltip-container">
                <label>Are you sure you want to delete {sectionTitle} {title}?</label>
                <button className="navigation-button" onClick={onClose}>
                     <p>Cancel</p>
                </button>
                 <button className="navigation-button" onClick={onRemove}>
                    {isFetching ? <Spinner/> : <p>OK</p> }
                </button>
            </div>
        );
    }
}

export default RemoveBlockToolTip;
