import React, {Component } from 'react';
import PropTypes from 'prop-types';

import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {addBlock} from '../../actions/Dashboard/Block';
import {getDashboard} from '../../actions/Dashboard/Dashboard';

import {DASHBOARD_BLOCK_TYPES, ENTER} from '../../constants/General';

function mapStateToProps(state) {
    const {dashboard} = state;
    const {id,blocks} = dashboard;
    return {
        count: blocks.length,
        dashboardId: id
    };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({addBlock, getDashboard}, dispatch);
}

@connect(mapStateToProps, mapDispatchToProps)
class CreateBlockToolTip extends Component {

    constructor(props){
        super(props);
        this.handleKeyDown = this.handleKeyDown.bind(this);
        this.createNewBlock = this.createNewBlock.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside);
    }
    
    handleClickOutside(event){
        const {target} = event;
        const {onCreated} = this.props;
        if (this.toolTip && !this.toolTip.contains(target)) { onCreated(); }
    }


    createNewBlock(){
        const {
            count,
            addBlock,
            onCreated,
            dashboardId,
            getDashboard
        } = this.props;

        const {value} = this.refs.blockTitleInput;

        addBlock(
           value,
           dashboardId,
           DASHBOARD_BLOCK_TYPES['DEFAULT']
        ).then(()=>{
            getDashboard(dashboardId, true, 0, count + 1);
            onCreated();
        });
    }

    handleKeyDown(evt){
        const {keyCode} = evt;
        if (keyCode === ENTER){ this.createNewBlock(); }
    }

    render(){
        const { onCreated } = this.props;
        return (
            <div  ref={(node)=>{this.toolTip = node;}} className="create-block-tooltip-container">
              <div className="title-input">
                <input type={'text'} onKeyDown={this.handleKeyDown} 
                       placeholder="Write title here" 
                       ref="blockTitleInput" />
              </div>
              <div className="controls">
                <button id="create-block-tooltip-cancel" onClick={onCreated}>
                  <label>Cancel</label>
                </button>
                <button id="create-block-tooltip-add" onClick={this.createNewBlock}>
                  <span className="icons ic-add-item-inactive" />
                  <label>Create</label>
                </button>
              </div>
            </div>
        );
    }
}

CreateBlockToolTip.propTypes = {
    count: PropTypes.number.isRequired,
    onCreated: PropTypes.func.isRequired,
    addBlock : PropTypes.func.isRequired,
    getDashboard : PropTypes.func.isRequired,
    dashboardId : PropTypes.number.isRequired
};


export default CreateBlockToolTip;
