import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {DragDropContext} from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import InfiniteScroll from 'redux-infinite-scroll';
import Block from './Block';

import {
  moveBlock,
  startDraggingBlock,
  endDraggingBlock
} from '../../actions/Dashboard/Block';
import {getDashboard} from '../../actions/Dashboard/Dashboard';

import {LIMIT} from '../../constants/General';

function mapStateToProps(state) {
  const {dashboard} = state;
  const {
    id,
    blocks,
    blocksCount,
    dragBlockPosition
  } = dashboard;

  return {
    blocks,
    blocksCount,
    dashboardId: id,
    dragBlockPosition
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    moveBlock,
    getDashboard,
    endDraggingBlock,
    startDraggingBlock,
  }, dispatch);
}

@connect(mapStateToProps, mapDispatchToProps)
@DragDropContext(HTML5Backend)
class DraggableBlocksContainer extends Component {
  static propTypes = {
      blocks: PropTypes.array.isRequired,
      moveBlock: PropTypes.func.isRequired,
      getDashboard: PropTypes.func.isRequired,
      dashboardId: PropTypes.number.isRequired,
      blocksCount: PropTypes.number.isRequired,
      endDraggingBlock: PropTypes.func.isRequired,
      startDraggingBlock: PropTypes.func.isRequired,
      dragBlockPosition: PropTypes.number.isRequired
  };

  constructor(props) {
    super(props);
    this.refreshBlocks = this.refreshBlocks.bind(this);
    this.handleScrollDown = this.handleScrollDown.bind(this);
    this.blockDraggablePosition = this.blockDraggablePosition.bind(this);
  }

  refreshBlocks(){
    const {
      dashboardId,
      getDashboard,
      blocks
    } = this.props;
    getDashboard(dashboardId, true, 0, blocks.length);
  }

  blockDraggablePosition(){
    return this.props.dragBlockPosition;
  }

  handleScrollDown(){
    const {
      blocks,
      dashboardId,
      getDashboard
    } = this.props;
    getDashboard(dashboardId, false, blocks.length, LIMIT);
  }

  render() {
     const {
       blocks,
       moveBlock,
       blocksCount,
       endDraggingBlock,
       startDraggingBlock
     } = this.props;

     let needToLoad = (blocksCount <= blocks.length);
     let draggableBlocks = blocks.map((block, index)=>{
        const {id, name, position, blockTypeId} = block;
        return (
          <Block
              id={id}
              key={position}
              name={name}
              index={index}
              position={position}
              moveBlock={moveBlock}
              blockTypeId={blockTypeId}
              refreshBlocks={this.refreshBlocks}
              endDraggingBlock={endDraggingBlock}
              startDraggingBlock={startDraggingBlock}
              blockDraggablePosition={this.blockDraggablePosition}
          />
        );
     });
     return (
       <InfiniteScroll
            loader={null}
            loadingMore={needToLoad}
            elementIsScrollable={true}
            loadMore={this.handleScrollDown}
            className="board-container">
        {draggableBlocks}
       </InfiniteScroll>
    );
  }
}

export default DraggableBlocksContainer;
