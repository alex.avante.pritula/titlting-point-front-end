import {findDOMNode} from 'react-dom';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
// import {Button} from "@sketchpixy/rubix";
import React, {Component } from 'react';
import PropTypes from 'prop-types';

import DraggableItemsContainer from './DraggableItemsContainer';

import {BLOCK_ITEM_WIDTH} from '../../constants/Dashboard';
import {addItem} from '../../actions/Dashboard/Item';


function mapStateToProps(state) {
   const {dashboard} = state;
   const {blocks} = dashboard;
   return {blocks};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({addItem}, dispatch);
}

@connect(mapStateToProps, mapDispatchToProps)
class ScrollableItemsContainer extends Component {

  static propTypes = {
      id: PropTypes.number.isRequired,
      index: PropTypes.number.isRequired,
      blocks: PropTypes.array.isRequired,
      addItem: PropTypes.func.isRequired,
      handleOpenNavigation: PropTypes.func.isRequired,
      openDropdownMenu: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.state = { marginLeft: 0 };
    this.moveNext = this.moveNext.bind(this);
    this.movePrev = this.movePrev.bind(this);
  }

  moveNext(){
    const {blocks, index} = this.props;
    const {items} = blocks[index];
    const {marginLeft} = this.state;
    const maxWidth = (BLOCK_ITEM_WIDTH * (items.length + 1));
    const {width} = findDOMNode(this.refs['scrollable-container']).getBoundingClientRect();

    if(maxWidth > (Math.abs(marginLeft) + width)) {
      this.setState({ marginLeft: marginLeft - BLOCK_ITEM_WIDTH });
    }
  }

  movePrev(){
    const {marginLeft} = this.state;
    if(!marginLeft){return;}
    this.setState({marginLeft: marginLeft + BLOCK_ITEM_WIDTH});
  }

  render() {
     const {blocks, index, id, openDropdownMenu} = this.props;
     const {items, name} = blocks[index];
     const {marginLeft} = this.state;
     
     let scrollableStyles = {
       width: (BLOCK_ITEM_WIDTH * (items.length + 1)) + 'px',
       marginLeft: marginLeft + 'px'
     };

     return (
       <div className="block-items-container">
         <button onClick={this.movePrev}>
           <span className="block-items-navigation prev-icon" />
         </button>
         <div className="block-items-scrollable-container" ref="scrollable-container">
           <div style={scrollableStyles} className="scrollable-box">
             <DraggableItemsContainer id={id} index={index} />
              <div className="add-new-block-item">
                  <div className="block-item-container" onClick={openDropdownMenu}>
                      <div className="block-item-container-icon block-item-container-default-add-icon"
                        onClick={this.props.handleOpenNavigation} >
                        <img src="/assets/svg/add-item.svg" />
                      </div>
                      <div className="block-item-container-description">
                          <span className="item-title">ADD NEW POINT</span>
                          <span className="item-description">Click to add new {name}</span>
                      </div>
                  </div>
              </div>
            </div>
         </div>
        <button onClick={this.moveNext}>
            <span className="block-items-navigation next-icon" />
        </button>
       </div>
    );
  }
}

export default ScrollableItemsContainer;
