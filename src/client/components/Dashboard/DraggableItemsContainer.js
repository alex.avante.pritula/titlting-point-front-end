import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import React, {Component } from 'react';
import PropTypes from 'prop-types';

import Item from './Item';

import {
  moveItem,
  endDraggingItem,
  startDraggingItem
} from '../../actions/Dashboard/Item';

import {getDashboard} from '../../actions/Dashboard/Dashboard';

function mapStateToProps(state) {
   const {dashboard} = state;
   const {id,blocks, dragItemPosition} = dashboard;
   return {
     blocks,
     dashboardId: id,
     dragItemPosition
   };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    moveItem,
    getDashboard,
    endDraggingItem,
    startDraggingItem
  }, dispatch);
}

@connect(mapStateToProps, mapDispatchToProps)
class DraggableItemsContainer extends Component {
  
  static propTypes = {
      id: PropTypes.number.isRequired,
      blocks: PropTypes.array.isRequired,
      index: PropTypes.number.isRequired,
      moveItem: PropTypes.func.isRequired,
      dashboardId: PropTypes.func.isRequired,
      getDashboard: PropTypes.func.isRequired,
      endDraggingItem: PropTypes.func.isRequired,
      startDraggingItem: PropTypes.func.isRequired,
      dragItemPosition: PropTypes.number.isRequired
  };

  constructor(props) {
    super(props);
    this.refreshBlocks = this.refreshBlocks.bind(this);
    this.itemDraggablePosition = this.itemDraggablePosition.bind(this);
  }

  refreshBlocks(){
      const {
        blocks, 
        dashboardId, 
        getDashboard
      } = this.props;
      getDashboard(dashboardId, true, 0, blocks.length);
  }

  itemDraggablePosition(){
    const {dragItemPosition} = this.props;
    return dragItemPosition;
  }

  render() {
     const {
       index,
       blocks,
       moveItem,
       endDraggingItem,
       startDraggingItem
     } = this.props;
     
     const block = blocks[index];
     const {items} = block;

     let draggableItems = items.map((item, itemIndex)=>{
        const {
          id, 
          name, 
          position, 
          description, 
          itemTypeId
        } = item;

        return (<Item
            id={id}
            key={id}
            name={name}
            item={item}
            index={itemIndex}
            blockIndex={index}
            blockId={block.id}
            moveItem={moveItem}
            position={position}
            itemTypeId={itemTypeId}
            description={description}
            endDraggingItem={endDraggingItem}
            refreshBlocks={this.refreshBlocks}
            startDraggingItem={startDraggingItem}
            itemDraggablePosition={this.itemDraggablePosition}
        />);
     });

     return (
      <div className="draggable-items-container">
          {draggableItems}
      </div>
    );
  }
}

export default DraggableItemsContainer;
