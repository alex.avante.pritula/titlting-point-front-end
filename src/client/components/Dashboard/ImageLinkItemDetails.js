import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import React, {Component} from 'react';
import PropTypes from 'prop-types';

import DropZoneContainer from '../General/DropZoneContainer';
import {
    uploadItemFile,
    changeItemValue
}  from '../../actions/Dashboard/Item';

function mapStateToProps(state) {
  const {item} = state;
  const {
    isEditing,
    selectedItemData, 
    selectedItemId,
    selectedItemTypeId
 } = item;

 
  return {
      isEditing,
      id: selectedItemId,
      data: selectedItemData,
      typeId: selectedItemTypeId

    };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
      uploadItemFile,
      changeItemValue
  }, dispatch);
}

const IMAGE_LINK_TYPE_ID = 6;

@connect(mapStateToProps, mapDispatchToProps)
class ImageLinkItemDetails extends Component {

    static propTypes = {
       id: PropTypes.object.isRequired,
       data: PropTypes.object.isRequired,
       typeId: PropTypes.number.isRequired,
       isEditing: PropTypes.boolean.isRequired,
       uploadItemFile: PropTypes.func.isRequired,
       changeItemValue: PropTypes.func.isRequired,
       validationErrors: PropTypes.array.isRequired
    };

    constructor(props){
       super(props);
       this.handleOnChange = this.handleOnChange.bind(this);
       this.handleDropDownNewImage = this.handleDropDownNewImage.bind(this);
       this.handleRemoveImageLinkMode = this.handleRemoveImageLinkMode.bind(this);
    }

    handleDropDownNewImage(file){
      const {uploadItemFile} = this.props;
      uploadItemFile(file);
    }

    handleRemoveImageLinkMode() {
      const {changeItemValue} = this.props;
      changeItemValue('url', '');
    }

    handleOnChange(evt) {
      const {value} = evt.target;
      const {changeItemValue} = this.props;
      changeItemValue('url', value);
    }

    render(){
        const {
          data, 
          typeId,
          isEditing
        } = this.props;
        const {url, validationErrors} = data;

        let dropZoneTitleByType = ((IMAGE_LINK_TYPE_ID === typeId)? 'Image': 'Document');
        let dropZoneType = ((IMAGE_LINK_TYPE_ID === typeId)? 'IMAGE': 'NEW_DOCUMENT');
        let urlValidationClass = (validationErrors.indexOf('url') === -1) ? 'valid-input': 'invalid-input';
        let embeddedType = ((IMAGE_LINK_TYPE_ID === typeId)? 'image': 'document');
        let editNavigation = null;
        let removeNavigation = null;

        if (isEditing) {
           editNavigation = (
              <button  onClick={this.handleEditImageLinkMode} >
                <span className="icons ic-edit-active" />
              </button>
           );

           removeNavigation = (
              <button  onClick={this.handleRemoveImageLinkMode} >
                <span className="icons ic-delete-active" />
              </button>
           );
        }

        let imageLinkDescription = (
          <div className="image-link-item-details-description__name_buttons_container">
            <div className="image-link-item-details-description__name_buttons_container__name">
              <p>Enter link for image here:</p>
            </div>
            {editNavigation}
            {removeNavigation}
          </div>
        );

        let imageLinkDescriptionNonEditable = (
          <div className="image-link-item-details-description__name_buttons_container non-editable">
            <div className="image-link-item-details-description__name_buttons_container__name">
              <p>Link for {embeddedType}:</p>
            </div>
          </div>
        );

        let imageLinkContainer = (
          <div className="image-link-item-details-description__input_label_container">
            <div className="image-link-item-details-description__input_label">
              <p>Enter link here:</p>
            </div>
            <div className="image-link-item-details-description__input_container">
              <textarea
                name="inputLink"
                onChange={this.handleOnChange}
                placeholder={'https://www.example.com'}
                value={url}
                className={urlValidationClass}
                />
            </div>
          </div>
        );

        let imageLinkContainerNonEditable = (
          <div className="image-link-item-details-description__input_label_container non-editable">
            <div className="image-link-item-details-description__input_label">
              <p>Link:</p>
            </div>
            <div className="image-link-item-details-description__input_container">
              <a target="_blank" rel="noopener noreferrer" href={url}>{url}</a>
            </div>
          </div>
        );
        
        return (
            <div className={'image-link-item-details-container'}>
              <div className={'image-link-item-details-description'}>
                { isEditing ? imageLinkDescription : imageLinkDescriptionNonEditable }
                { isEditing ? imageLinkContainer : imageLinkContainerNonEditable }
              </div>
              <div className="image-link-item-dropzone-section">
                <div className={'image-link-item-dropzone'}>
                  <DropZoneContainer 
                    uploadType={dropZoneType} 
                    defaultPreview={url}
                    disabled={!isEditing}
                    previewContent={
                      <span>
                          {dropZoneTitleByType} box
                          <br/>
                          drag and drop
                          <br/>
                          to add {dropZoneTitleByType.toLowerCase()}
                       </span>
                    }
                    onChangeData={this.handleDropDownNewImage} />
                </div>
              </div>
          </div>
        );
    }
}

export default ImageLinkItemDetails;
