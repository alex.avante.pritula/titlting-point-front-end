import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
    addLinkToListOfLinks,
    removeLinkFromListOfLinks,
    updateLinkFromListOfLinks
}  from '../../actions/Dashboard/Item';

function mapStateToProps(state) {
  const {item} = state;
  const {
    isEditing,
    selectedItemData, 
    selectedItemId
  } = item;
  return {
      isEditing,
      id:   selectedItemId,
      data: selectedItemData
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    addLinkToListOfLinks,
    removeLinkFromListOfLinks,
    updateLinkFromListOfLinks
  }, dispatch);
}

class ListOfLinks extends Component {
  static propTypes = {
    data: PropTypes.object.isRequired,
    isEditing: PropTypes.boolean.isRequired,
    addLinkToListOfLinks: PropTypes.func.isRequired,
    removeLinkFromListOfLinks: PropTypes.func.isRequired,
    updateLinkFromListOfLinks: PropTypes.func.isRequired
  };
    
  constructor(props) {
    super(props);
    this.handleRemoveLinkItem = this.handleRemoveLinkItem.bind(this);
    this.handleOnChangeLinkItem = this.handleOnChangeLinkItem.bind(this);
    this.handleOnChangeDescriptionItem = this.handleOnChangeDescriptionItem.bind(this);
  }
  
  handleRemoveLinkItem(index){
    const {removeLinkFromListOfLinks} = this.props;
    removeLinkFromListOfLinks(index);
  }
  
  handleOnChangeLinkItem(evt, index){
    const {value} = evt.target;
    const {updateLinkFromListOfLinks} = this.props;
    updateLinkFromListOfLinks(index, 'title', value);
  }
  
  handleOnChangeDescriptionItem(evt, index){
    const {value} = evt.target;
    const {updateLinkFromListOfLinks} = this.props;
    updateLinkFromListOfLinks(index, 'url', value);
  }
  
  render () {
    const {data, isEditing} = this.props;
    const {links} = data;
    let linkItems = [];
    if(!links) { return null; }
    
    linkItems = links.map((link, index)=>{
        const {title, url} = link;
        let visualIndex = index + 1;

        let editNavigation = null;
        let removeNavigation = null;

        if(isEditing) {
          editNavigation = (
              <button>
              <span className="icons ic-edit-active" />
            </button>
          );
          removeNavigation = (
              <button onClick={() => { this.handleRemoveLinkItem(index); }}>
                <span className="icons ic-delete-active" />
              </button>
          );
        }
        const linkTitle = (
          <div className="list-of-links-item-details-container__links_container__link_item__left_part_info">
            <div className="list-of-links-item-details-container__links_container__link_item__left_part_info__title_text">
              <p>Enter Link Title here:</p>
            </div>
            <div className="list-of-links-item-details-container__links_container__link_item__left_part_info__title_container">
              <textarea
                value={title}
                name="linkItemLinkTitle"
                placeholder={`Link Title ${visualIndex}`}
                onChange={(e)=>{ this.handleOnChangeLinkItem(e, index); }}
                />
            </div>
          </div>
        );
        const linkTitleNonEditable = (
          <div className="list-of-links-item-details-container__links_container__link_item__left_part_info non-editable">
            <div className="list-of-links-item-details-container__links_container__link_item__left_part_info__title_text">
              <p>Link Title:</p>
            </div>
            <div className="list-of-links-item-details-container__links_container__link_item__left_part_info__title_container">
              <p>{title}</p>
            </div>
          </div>
        );
        const linkText = (
          <div className="list-of-links-item-details-container__links_container__link_item__right_part__heading__link_text">
            <p>Enter Link here:</p>
            {editNavigation}
            {removeNavigation}
          </div>
        );
        const linkTextNonEditable = (
          <div className="list-of-links-item-details-container__links_container__link_item__right_part__heading__link_text non-editable">
            <p>Link:</p>
          </div>
        );
        const linkContainer = (
          <div className="list-of-links-item-details-container__links_container__link_item__right_part__link_container">
            <textarea
              value={url}
              name="linkItemLink"
              placeholder="https://www.example.com"
              onChange={(e)=>{ this.handleOnChangeDescriptionItem(e, index); }}
              />
          </div>
        );
        const linkContainerNonEditable = (
          <div className="list-of-links-item-details-container__links_container__link_item__right_part__link_container non-editable">
            <a target="_blank" rel="noopener noreferrer" href={url}>{url}</a>
          </div>
        );
        return (
          <div key={index} className="list-of-links-item-details-container__links_container">
            <div className="list-of-links-item-details-container__links_container__link_item">
              <div className="list-of-links-item-details-container__links_container__link_item__left_part">
                <div className="list-of-links-item-details-container__links_container__link_item__left_part_number">
                  <p>{visualIndex}</p>
                </div>
                { isEditing ? linkTitle: linkTitleNonEditable }
              </div>
              <div className="list-of-links-item-details-container__links_container__link_item__right_part">
                <div className="list-of-links-item-details-container__links_container__link_item__right_part__heading">
                  <div className="list-of-links-item-details-container__links_container__link_item__right_part__heading__number">
                    <p>{visualIndex}</p>
                  </div>
                  { isEditing ? linkText: linkTextNonEditable }
                </div>
                { isEditing ? linkContainer: linkContainerNonEditable }
              </div>
            </div>
          </div>
      );
    });
  
    return (
      <div>{ links ? linkItems : null }</div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ListOfLinks);