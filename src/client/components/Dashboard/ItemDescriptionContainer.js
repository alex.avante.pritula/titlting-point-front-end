import moment from 'moment';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';

import {Spinner} from 'react-mdl';
import NewsItemDetails from './NewsItemDetails';
import ImageLinkItemDetails from './ImageLinkItemDetails';
import ListOfLinksItemDetails from './ListOfLinksItemDetails';
import VideoEmdedeItemDetails from './VideoEmdedeItemDetails';
import NewsItemDetailsViewDialog from './NewsItemDetailsViewDialog';

import {DASHBOARD_BLOCK_ITEM_TYPES} from '../../constants/General';
import {getDashboard} from '../../actions/Dashboard/Dashboard';

import {
  removeItem,
  createNews,
  updateNews,  
  changeItemType,
  changeItemValue,
  setEditingStatus,
  updateDefaultLink,
  createDefaultLink,
  createListOfLinks,
  updateListOfLinks,
  getItemDescription,
  setValidationErrors,
  handleCloseItemDialog,
  handleOpenItemDialog
} from '../../actions/Dashboard/Item';

const typeKeys = Object.keys(DASHBOARD_BLOCK_ITEM_TYPES);
const itemDetailsComponents = {
  TEXTAREA: NewsItemDetails,
  NEWS_BLOCK: NewsItemDetails,
  VIDEO: VideoEmdedeItemDetails,
  IMAGE_LINK: ImageLinkItemDetails,
  NEW_DOCUMENT: ImageLinkItemDetails,
  LIST_OF_LINKS: ListOfLinksItemDetails
};

function mapStateToProps(state) {
  const {item, dashboard} = state;
  const {blocks, id} = dashboard;

  const {
    isEditing,
    isFetching,
    itemIndex,
    blockIndex,
    selectedItemId,
    selectedBlockId,
    selectedItemData,
    selectedItemTypeId
  } = item;

  let nextItemId = -1;
  let prevItemId = -1;
  let nextItemTypeId = -1;
  let prevItemTypeId = -1;

  const currentBlock = blocks[blockIndex];

  if (currentBlock) {
    const {items} = currentBlock;
    if(items.length > 0) {

        if(itemIndex !== 0){ 
          let prevItem = items[itemIndex - 1];
          prevItemId = prevItem.id;
          prevItemTypeId = prevItem.itemTypeId;
        }

        if(itemIndex !== items.length - 1) {
          let nextItem = items[itemIndex + 1];
          nextItemId = nextItem.id;
          nextItemTypeId = nextItem.itemTypeId;
        }
    }
  }

  return {
    isEditing,
    isFetching,
    nextItemId,
    prevItemId,
    itemIndex,
    blockIndex,
    nextItemTypeId,
    prevItemTypeId,
    dashboardId: id,
    id: selectedItemId,
    data: selectedItemData,
    blockId: selectedBlockId,
    typeId: selectedItemTypeId,
    blocksCount: blocks.length
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    removeItem,
    updateNews,
    createNews,
    getDashboard,
    changeItemType,
    changeItemValue,
    updateListOfLinks,
    updateDefaultLink,
    setEditingStatus,
    createDefaultLink,
    createListOfLinks,
    getItemDescription,
    setValidationErrors,
    handleCloseItemDialog,
    handleOpenItemDialog
  }, dispatch);
}

class ItemDescriptionContainer extends Component {

  static propTypes = {
    id: PropTypes.number.isRequired,
    data: PropTypes.object.isRequired,
    typeId: PropTypes.number.isRequired,
    blockId: PropTypes.number.isRequired,
    updateNews: PropTypes.func.isRequired,
    createNews: PropTypes.func.isRequired,
    removeItem: PropTypes.func.isRequired,
    itemIndex: PropTypes.number.isRequired,
    blockIndex: PropTypes.number.isRequired,
    nextItemId: PropTypes.number.isRequired,
    prevItemId: PropTypes.number.isRequired,
    isEditing: PropTypes.boolean.isRequired,
    getDashboard: PropTypes.func.isRequired,
    isFetching: PropTypes.boolean.isRequired,
    dashboardId: PropTypes.number.isRequired,
    blocksCount: PropTypes.number.isRequired,
    changeItemType: PropTypes.func.isRequired,
    changeItemValue: PropTypes.func.isRequired,
    nextItemTypeId: PropTypes.number.isRequired,
    prevItemTypeId: PropTypes.number.isRequired,
    setEditingStatus: PropTypes.func.isRequired,
    updateListOfLinks: PropTypes.func.isRequired,
    validationErrors: PropTypes.array.isRequired,
    updateDefaultLink: PropTypes.func.isRequired,
    createDefaultLink: PropTypes.func.isRequired,
    createListOfLinks: PropTypes.func.isRequired,
    getItemDescription: PropTypes.func.isRequired,
    setValidationErrors: PropTypes.func.isRequired,
    handleOpenItemDialog: PropTypes.func.isRequired,
    handleCloseItemDialog: PropTypes.func.isRequired
  };

  constructor(props){
    super(props);
    this.handleEditMode = this.handleEditMode.bind(this);
    this.handleClickPrev = this.handleClickPrev.bind(this);
    this.handleClickNext = this.handleClickNext.bind(this);
    this.handleItemClose = this.handleItemClose.bind(this);
    this.handleRemoveItem = this.handleRemoveItem.bind(this);
    this.handleClickOutside = this.handleClickOutside.bind(this);
    this.handleChangeItemType =  this.handleChangeItemType.bind(this);
    this.handleChangeItemTitle = this.handleChangeItemTitle.bind(this);
    this.handleItemDescriptionSave = this.handleItemDescriptionSave.bind(this);
  }

  componentDidMount() {
     document.addEventListener('mousedown', this.handleClickOutside);
  }

  componentWillUnmount() {
     document.removeEventListener('mousedown', this.handleClickOutside);
  }

  handleClickNext(){
    const {
      blockId,
      blockIndex, 
      nextItemId,
      itemIndex,
      nextItemTypeId,
      handleOpenItemDialog
    } = this.props;

    handleOpenItemDialog(
      blockId, 
      nextItemTypeId, 
      nextItemId, 
      blockIndex, 
      itemIndex + 1
    ); 
  }

  handleClickPrev(){
    const {
      blockId,
      blockIndex, 
      prevItemId,
      itemIndex,
      prevItemTypeId,
      handleOpenItemDialog
    } = this.props;

    handleOpenItemDialog(
      blockId, 
      prevItemTypeId, 
      prevItemId, 
      blockIndex, 
      itemIndex - 1
    );
  }

  handleChangeItemType(type){
    const {id, changeItemType} = this.props;
    if (id){ return; }
    const typeId = DASHBOARD_BLOCK_ITEM_TYPES[type].id;
    changeItemType(typeId);
  }

  handleClickOutside(event){
      const {target} = event;
      const {
        isFetching, 
        handleCloseItemDialog
      } = this.props;

      let isNotTinyMCEContainer = true;
      
      if(this.itemChild && this.itemChild.getTinyMCENode !== undefined){

        let tinyMCEItems= document.getElementsByClassName('mce-container');
        let tinyMCEContainer = this.itemChild.getTinyMCENode();

        if(tinyMCEItems) {
          for(let i in tinyMCEItems){
            if(tinyMCEItems[i].contains){
                if(tinyMCEItems[i].contains(target)){
                  isNotTinyMCEContainer = false;
                  break;
                }
            }
          }
        }

        if(tinyMCEContainer){
          if(tinyMCEContainer.contains(target)){
              isNotTinyMCEContainer = false;
          }
        }
      }

      if (!isFetching && (this.modalDialog && 
          !this.modalDialog.contains(target) && isNotTinyMCEContainer)) { 
           handleCloseItemDialog();
      }
  }

  handleEditMode(){
      const {
        isEditing,
        setEditingStatus
      } = this.props;

      setEditingStatus(!isEditing);
  }

  handleRemoveItem(){
     const {
       id, 
       removeItem,
       blocksCount,
       dashboardId,
       getDashboard,
       handleCloseItemDialog
     } = this.props;

     removeItem(id).then(()=>{
       return getDashboard(dashboardId, true, 0, blocksCount);
     }).then(()=>{
        handleCloseItemDialog();
     });
  }

  handleItemClose() {
     const {handleCloseItemDialog} = this.props;
     handleCloseItemDialog();
  }

  handleChangeItemTitle(evt){
    const {value} = evt.target;
    const {changeItemValue} = this.props;
    changeItemValue('name', value);
  }

  handleItemDescriptionSave() {

     const {
       id, 
       data, 
       typeId, 
       blockId, 
       updateNews,
       createNews,
       dashboardId,
       blocksCount,
       getDashboard,
       createDefaultLink,
       updateDefaultLink,
       createListOfLinks,
       updateListOfLinks,
       setValidationErrors,
       handleCloseItemDialog
     } = this.props;
     
    let currentType = typeKeys.find((type)=>{
        const item = DASHBOARD_BLOCK_ITEM_TYPES[type];
        if (item['id'] === typeId) { return true; }
    }); 
    
   let newItemId = null;
    Promise.resolve().then(()=>{

      if (id) {
        newItemId = id;
        if(['IMAGE_LINK','VIDEO', 'NEW_DOCUMENT'].indexOf(currentType) != -1) {
            const {url, file, name, linkDescription} = data;
            const files = file.get('files');

            let errors = [];
            if (name.length < 1) { errors.push('name'); } 
            if (url.length < 1 && !files){ errors.push('url');  }
            if(errors.length > 0){ return setValidationErrors(errors); }

            let link = {
              url,name,
              id: data.id,
              linkDescription
            };
            return updateDefaultLink(id, link, files);
        }

        if(['NEWS_BLOCK','TEXTAREA'].indexOf(currentType) != -1) {
          const {file, firstImageId, lastImageId, name, newsDescription} = data; 
          if (name.length < 1){ return setValidationErrors(['name']); }
          let news = {
              name: name,
              id: data.id,
              firstImageId,
              lastImageId,
              description: newsDescription,
            };
            return updateNews(id, news, file);
        }

              
        if(currentType === 'LIST_OF_LINKS') {
            const {name, editedLinks, removedLinks, links} = data; 
            let errors = [];
            if (name.length < 1) { errors.push('name'); } 
            // Add validation for simple link
            if (errors.length > 0){ 
              return setValidationErrors(errors); 
            }
            let linksForAdd = [];
            let linksForUpdate = [];

            links.forEach((link) => {
               const {id, url ,title} = link;
               if(!id) { linksForAdd.push({url, title}); }

               let updateindex = editedLinks.findIndex((updateId)=>{
                 return (id === updateId);
               }); 

               if(updateindex !== -1) { 
                 linksForUpdate.push({id, url, title}); 
               }
            });

            linksForAdd = {
              name, 
              blockId,
              title: name,
              itemId: id,
              itemTypeId: typeId,
              links: linksForAdd
            };
            
            return updateListOfLinks(id, name, removedLinks, linksForAdd, linksForUpdate);
        }
      } else  {

        if(['IMAGE_LINK','VIDEO', 'NEW_DOCUMENT'].indexOf(currentType) != -1) {
            const {url, file, name, linkDescription} = data;
            const files = file.get('files');

            let errors = [];
            if (name.length < 1) { errors.push('name'); } 
            if (url.length < 1 && !files){ errors.push('url');  }
            if(errors.length > 0){ return setValidationErrors(errors); }

            let link = {
                blockId,
                name: name,
                title: name,
                url: url || '',
                itemTypeId: typeId,
                linkDescription: linkDescription,
            };
            return createDefaultLink(link, files);
        }

        if(['NEWS_BLOCK','TEXTAREA'].indexOf(currentType) != -1) {
          const {file, name, newsDescription} = data; 
          if (name.length < 1) { return setValidationErrors(['name']); }
          let news = {
              blockId,
              name: name,
              title: name,
              itemTypeId: typeId,
              newsDescription: newsDescription,
            };
            return createNews(news, file);
        }
        
        if(currentType === 'LIST_OF_LINKS') {
            const {name, links} = data; 
            let errors = [];
            if (name.length < 1) { errors.push('name'); } 
            // Add list of links validation
            if(errors.length > 0){ return setValidationErrors(errors); }

            let listOfLinks = {
              blockId,
              name: name,
              title: name,
              itemTypeId: typeId,
              links: links.map((link)=>{
                const {title, url} = link;
                return {title, url};
              })
            };
            return createListOfLinks(listOfLinks);
        }
      }
    }).then((itemId)=>{
      if(!newItemId){ newItemId = itemId; }
      if(newItemId){ return getDashboard(dashboardId, true, 0, blocksCount); }
    }).then(()=>{
      if(newItemId){ handleCloseItemDialog(); }
    });
  }

  render(){
      const {
        id, 
        data, 
        typeId, 
        isEditing,
        isFetching,
        prevItemId,
        nextItemId
      } = this.props;

      if (!typeId || !data){
        return  null;
      }

      const {
        name, 
        ownerName, 
        ownerRole, 
        createdAt,
        validationErrors
      } = data;

      let navigation = null;
      let currentType = {};
      let spetificDetails = null;
      let authorDescription = null;
      let date = moment(createdAt).format('YYYY/MM/DD');

      let typeItems = typeKeys.map((type, index)=>{
          const item = DASHBOARD_BLOCK_ITEM_TYPES[type];
          
          let activeClass = '';
          if(id) { activeClass += 'item-description-disabled'; }

          if (item['id'] === typeId){
            currentType = Object.assign({type: type}, item);
            activeClass = 'item-description-active-type';
          }

           return (
            <button key={index} onClick={()=>{ if(!id) { this.handleChangeItemType(type); } }}>
               <span className={`icons  ${item['iconActive']} ${activeClass}`} />
            </button>
          );
      });
    
      if(!isEditing && id !== null && (
            currentType['type'] === 'NEWS_BLOCK' 
         || currentType['type'] === 'TEXTAREA')
      ) { return (<NewsItemDetailsViewDialog />); }

      if (ownerName && ownerRole) {
        authorDescription = (
            <div className="item-description-table__main_section__left_part__date_author_container">
              <div className="item-description-table__main_section__left_part__author">
                <div className="item-description-table__main_section__left_part__author_container__text">
                  Added by: {ownerName}
                </div>
                <div className="item-description-table__main_section__left_part__date_container_author__current_author">
                  <p>{ownerRole}</p>
                </div>
              </div>
              <div className="item-description-table__main_section__left_part__date_container">
                <div className="item-description-table__main_section__left_part__date_container_date">
                  date:
                </div>
                <div className="item-description-table__main_section__left_part__date_container__current_date">{date}</div>
              </div>
            </div>
        ); 
     }

     if (id){

       let isEditingClasses = 'item-description-table__header__icon_right_first';
       if(isEditing) { 
         isEditingClasses += ' item-description-table__header__icon_right_first_is_editing';
       }

       navigation = (
         <div>
            <button onClick={this.handleEditMode}
               className={isEditingClasses}>
              <span className="icons ic-edit-active" />
            </button>
            <button onClick={this.handleRemoveItem} 
              className="item-description-table__header__icon_right_second">
              <span className="icons ic-delete-active" />
            </button>
            <button className="item-description-table__header__icon_right_third">
              <span className="icons ic-send-invite" />
            </button>
          </div>
       );
     }
     
     spetificDetails = React.createElement(itemDetailsComponents[currentType['type']], {
        onRef: (ref) => { this.itemChild = ref; }
      });
    
     let footerEditing = (
       <div className="item-description-table__footer">
         <button disabled={isFetching} onClick={this.handleItemClose} ><p>cancel</p></button>
         <button onClick={this.handleItemDescriptionSave} disabled={isFetching}>
           { isFetching ? <Spinner singleColor /> : <p>pin it</p> }
         </button>
       </div>
     );

     let hasPrev = (prevItemId !== -1);
     let hasNext = (nextItemId !== -1);

     let footerViewing = (
       <div className="item-description-table__footer viewing">
         <button style={{opacity: (hasPrev ? 1: 0)}} disabled={isFetching || !hasPrev} 
            onClick={this.handleClickPrev}>
           <span className="icons ic-prev-inactive" />
         </button>
         <button style={{opacity: (hasNext ? 1: 0)}} disabled={isFetching || !hasNext} 
           onClick={this.handleClickNext}>
           <span className="icons ic-next-inactive" />
         </button>
       </div>
     );

     return (
        <div className={'item-description-body'} >
          <div className="item-description-table"  ref={(node)=>{this.modalDialog = node;}} >
            <div className="item-description-table__header">
              <div className="item-description-table__header__main_icon">
                <span className={`icons ${currentType['iconDefault']}`} />
              </div>
              <div className="item-description-table__header__main_info">
                <div className="item-description-table__header__main_info__name">
                  <p>{name}</p>
                </div>
                <div className="item-description-table__header__main_info__date_container">
                  <p>date:</p>
                  <div className="item-description-table__header__main_info__date_container__current_date">
                    <p>{date}</p>
                  </div>
                </div>
              </div>
              {navigation}
            </div>
            <div className="item-description-table__main_section">
              <div className="item-description-table__main_section__left_part">
                <div className="item-description-table__main_section__left_part__name_container">
                  <div className="item-description-table__main_section__left_part__name_container__item_type_name">
                    <p>{currentType['name']} title:</p>
                  </div>
                  <div className="item-description-table__main_section__left_part__name_container__item__name">
                  <p>
                    <input
                      type={'text'}
                      value={name}
                      disabled={!isEditing}
                      onChange={this.handleChangeItemTitle}
                      className={(validationErrors.indexOf('name') === -1) ? 'valid-input': 'invalid-input'}
                      placeholder={`Enter ${currentType['name']} title`}
                    />
                  </p>
                  </div>
                </div>
                 {authorDescription} 
              </div>
              <div className="item-description-table__main_section__right_part">
               <div className="item-description-table__main_section__right_part__text_container">
                 {id ? 'D' : 'Set d'}ocument type
               </div>
                <div className="item-description-table__main_section__right_part__icons_container">
                  {typeItems}
                </div>
              </div>
            </div>
            {spetificDetails}
            {isEditing ? footerEditing : footerViewing}
          </div>
       </div>
      );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ItemDescriptionContainer);
