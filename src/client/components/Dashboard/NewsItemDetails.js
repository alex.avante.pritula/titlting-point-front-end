import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';

import TinyMCE from 'react-tinymce';
import DropZoneContainer from '../General/DropZoneContainer';

import {
    changeItemValue,
    uploadNewsItemFile
}  from '../../actions/Dashboard/Item';

function mapStateToProps(state) {
  const {item} = state;
  const {
    selectedItemId,
    selectedItemData
  } = item;

  return {
      id: selectedItemId,
      data: selectedItemData
    };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
      changeItemValue,
      uploadNewsItemFile
  }, dispatch);
}

class NewsItemDetails extends Component {

    static propTypes = {
       id: PropTypes.object.isRequired,
       onRef: PropTypes.func.isRequired,
       data: PropTypes.object.isRequired,
       changeItemValue: PropTypes.func.isRequired,
       uploadNewsItemFile: PropTypes.func.isRequired
    };

    constructor(props){
       super(props);

       this.getTinyMCENode = this.getTinyMCENode.bind(this);
       this.handleEditorChange = this.handleEditorChange.bind(this);
       this.handleDropDownImage = this.handleDropDownImage.bind(this);
    }

    componentDidMount() {
        this.props.onRef(this);
    }

    componentWillUnmount() {
        this.props.onRef(null);
    }

    handleEditorChange(evt){
        let value = evt.target.getContent({format: 'raw'});
        const {changeItemValue} = this.props;
        changeItemValue('newsDescription', value);
    }

    handleDropDownImage(file, position){
        const {uploadNewsItemFile} = this.props;
        uploadNewsItemFile(file, position);
    }

    getTinyMCENode(){
        return this.tinyMCEContainer;
    }

    render(){
        const {data} = this.props;
        const {newsDescription, firstImageUrl, lastImageUrl} = data;
        let content = '<p></p>';
        if (newsDescription.length > 0) { content = newsDescription; }

        let previewContent = (
           <span>
                Image box
                <br/>
                contains main
                <br/>
                of this article
            </span>
        );

        return (
            <div className="news-item-details-container">
                <div className="news-item-details-dropzone-container">
                     <DropZoneContainer uploadType={'IMAGE'} 
                        previewContent={previewContent}
                        defaultPreview={firstImageUrl}
                        onChangeData={(file)=>{ this.handleDropDownImage(file, 0); }} />
                     <DropZoneContainer uploadType={'IMAGE'}
                        previewContent={previewContent}
                        defaultPreview={lastImageUrl}
                        onChangeData={(file)=>{ this.handleDropDownImage(file, 1); }}  />
                </div>
                <div ref={(node) => { this.tinyMCEContainer = node; }}
                     className="news-item-details-dropzone-tinymce-container">
                    <TinyMCE
                        content={content}
                        config={{
                            plugins: 'link',
                            content_style: ".mce-content-body { font-size: 14px; min-height: 250px; }",
                            toolbar: 'undo redo | bold italic | alignleft aligncenter alignright'
                        }}
                        onChange={this.handleEditorChange}
                    />
                </div>
            </div>
        );

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewsItemDetails);
