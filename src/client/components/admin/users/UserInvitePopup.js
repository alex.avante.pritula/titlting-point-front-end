import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {reduxForm, Field, reset} from 'redux-form';
import {Option} from 'react-mdl-extra';
import Spinner from 'react-mdl/lib/Spinner';

import {USER_ROLES, VALIDATION} from '../../../constants/General';
import { DEPARTMENTS_LIST } from '../../../constants/Departments';

import Input from '../../UIComponents/Input';
import Select from '../../UIComponents/Select';

import {userManagementGetUsers, userManagementInviteDialogToggle, userManagementCreateUser} from '../../../actions/admin/users/userManagementActions';

/**
 * [ User invite popup component ]
 * Used for send registration invitation to user
 */
class UserInvitePopup extends Component {

  static propTypes = {
    departments: PropTypes.array.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    pristine: PropTypes.bool.isRequired,
    submitting: PropTypes.bool.isRequired,
    close: PropTypes.func.isRequired
  };

  render() {
    const {handleSubmit, pristine, submitting, close} = this.props;

    const userRoleOptions = USER_ROLES
      .map(({id, title}, index) =>
        <Option key={index} value={id}>
          {title}
        </Option>);

    const departmentOptions = DEPARTMENTS_LIST
      .map(({id, name}, index) =>
        <Option key={index} value={id}>
          {name}
        </Option>);

    return (
      <div className="tgpoint--user-invite-popup">
        <form onSubmit={handleSubmit}>
          <section className="tgpoint--invite-head">
            <div className="avatar">
              <span className="icon-user-avatar-invite"/>
            </div>
            <div className="title">CREATING NEW USER</div>
          </section>

          <section className="tgpoint--invite-body">
            <div className="tgpoint--invite-blank-line"/>
            <div className="tgpoint--invite-data-fields">
              <div className="tgpoint--invite-data-item-block">
                <Field name="roleId"
                       label="Set role of new user"
                       floatingLabel
                       component={Select}>
                  { userRoleOptions }
                </Field>
                <Field name="departmentId"
                       label="Department"
                       floatingLabel
                       component={Select}
                >
                  { departmentOptions }
                </Field>
              </div>
              <div className="tgpoint--invite-data-item-block">
                <Field name="email"
                       label="Enter e-mail of new user"
                       floatingLabel
                       type="text"
                       autoComplete="off"
                       component={Input}
                />
              </div>
              {/* TEMPORARY DISABLE INVITE LINK GENERATION ON THE FLY */}
              {/*<div className="tgpoint--invite-data-item-block">*/}
              {/*<Field name="invite-link"*/}
              {/*label="Generated invite:"*/}
              {/*floatingLabel*/}
              {/*type="text"*/}
              {/*autoComplete="off"*/}
              {/*component={Input}*/}
              {/*/>*/}
              {/*<p className="invite-text">Invite is automaticly generated and will be sent to e-mail entered above</p>*/}
              {/*</div>*/}
            </div>
          </section>
          <hr className="line-separator"/>

          <section className="tgpoint--invite-footer">
            <button className="btn" type="button" onClick={close}>
              <p>CANCEL</p>
            </button>
            <button className="btn primary" type="submit" disabled={pristine || submitting}>
              { submitting ? <Spinner /> : <p>INVITE</p> }
            </button>
          </section>
        </form>
      </div>
    );
  }
}

const mapStateToProps = ({departments}) => ({
  departments: departments.departments
});
const mapDispatchToProps = (dispatch) => ({
  close() {
    dispatch(reset('user-invite-form'));
    dispatch(userManagementInviteDialogToggle(false));
  }
});

const userInviteReduxForm = {
  form: 'user-invite-form',
  validate: ({role, department, email}, props) => {
    const {anyTouched} = props;
    const errors = {};
    if (!anyTouched) {
      return errors;
    }
    if (role === undefined) {
      errors.role = 'Please, select a role for a new user';
    }
    if (department === undefined) {
      errors.department = 'Please, select a department for a new user';
    }
    if (!email) {
      errors.email = 'Please, enter a email for a new user';
    } else {
      if (!email.match(VALIDATION.EMAIL)) {
        errors.email = 'Invalid email';
      }
    }
    return errors;
  },
  onSubmit: userManagementCreateUser,
  onSubmitSuccess: (result, dispatch) => {
    dispatch(reset('user-invite-form'));
    dispatch(userManagementInviteDialogToggle(false));
    dispatch(userManagementGetUsers());
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(reduxForm(userInviteReduxForm)(UserInvitePopup));
