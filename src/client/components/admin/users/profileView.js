import {connect} from 'react-redux';
//import Dropzone from 'react-dropzone';
import {bindActionCreators} from 'redux';
import React, { Component/*, PropTypes*/ } from 'react';
//import { browserHistory } from 'react-router'
import { reduxForm, Field } from 'redux-form';
import { Spinner } from 'react-mdl';
import findIndex from 'lodash/findIndex';

import { VALIDATION } from '../../../constants/General';
import { DEPARTMENTS_LIST } from '../../../constants/Departments';

const {/*EMAIL,*/ PASSWORD} = VALIDATION;

import Input from '../../UIComponents/Input';
import Breadcrumb from '../../UIComponents/Breadcrumb';
import MainActionButton from '../../UIComponents/MainActionButton';

import {
  userProfileGetByCodeAPICall,
  userProfileRegistrationCompleteAPICall
} from '../../../actions/User/Profile';

import {
  //logIn
} from '../../../actions/User/Auth';

@connect(mapStateToProps, mapDispatchToProps)
class Registration extends Component {

    constructor(props){
       super(props);
       this.state = {
           bioIsOpen: false,
           contactIsOpen: false
       };

       this.toggleClass = this.toggleClass.bind(this);
    }

    componentDidMount() {
      const { userProfileGetByCodeAPICall, routeParams } = this.props;
      userProfileGetByCodeAPICall(routeParams.id);
    }

    componentWillUpdate() {
      const { registrationSuccess,/* registrationProfie,*/ loggedIn /*, error*/} = this.props;
      //const dispatch = this.props.dispatch;
      if(registrationSuccess && !loggedIn) {
        //const { email, password } = registrationProfie;
        // logIn({ email, password }, dispatch);
      }
    }

    toggleClass(param) {
      if (param == 'bio') {
        this.setState({
          bioIsOpen: !this.state.bioIsOpen
        });
      }
      if (param == 'contact') {
        this.setState(
          {
            contactIsOpen: !this.state.contactIsOpen
          }
        );
      }
    }

    render(){
      const { handleSubmit, isFetching,/* registrationProfie,*/ submitting, pristine, isRegistrationFetching, error} = this.props;

      const {bioIsOpen} = this.state;
      const {contactIsOpen} = this.state;
      let bioClass = 'register_profile__main_info__bio';
      let contactClass = 'register_profile__contact_info';
      if (bioIsOpen) { bioClass += ' opened'; }
      if (contactIsOpen) { contactClass += ' opened'; }

       return (
         <div className="tgpoint-content-body not-restricted">
          <Breadcrumb pageTitle="USER PROFILE CREATING" />
           { (error !== null) && (error.code === 400) ?
             <div className="error-container">
              <h4 className="error-item">Sorry, but you provided invalid code for registration!</h4>
             </div>
           : (!isFetching && (error === null) ? <form onSubmit={handleSubmit} autoComplete="off">
             <div className="register_profile__container">
                <div className="register_profile__header">
                  <div className="register_profile__header__icon">
                    <img src="/assets/svg/human_ico.svg" alt="Your profile"></img>
                  </div>
                  <div className="register_profile__header__text">your profile</div>
                </div>
                <div className="register_profile__main_info">
                  <div className="register_profile__main_info__left_part">
                    <div className="register_profile__main_info__photo__container">
                      <div className="register_profile__main_info__photo">
                        <img src="/assets/svg/human_ico.svg" alt="Your profile"></img>
                        <p className="mobile_photo_text">Tap to set photo</p>
                        <p className="desktop_photo_text">Click to set photo</p>
                      </div>
                    </div>
                  </div>
                  <div className="register_profile__main_info__inputs_container">
                    <div className="register_profile__main_info__full_name">
                      <div className="temporary_input">
                        <Field  name="fullname"
                          placeholder="Please enter your name"
                          label="Full name:"
                          floatingLabel
                          type="text"
                          component={Input}
                        />
                      </div>
                      <div className="edit_icon"></div>
                    </div>
                    <div className="register_profile__main_info__job">
                      <div className="temporary_input">
                        <Field  name="job"
                            placeholder="Enter your position"
                            label="Job:"
                            floatingLabel
                            type="text"
                            component={Input}
                          />
                      </div>
                      <div className="edit_icon"></div>
                    </div>
                    <div className="register_profile__main_info__job">
                      <div className="temporary_input">
                        <Field  name="department"
                            placeholder=""
                            label="Your department"
                            floatingLabel
                            type="text"
                            disabled
                            component={Input}
                          />
                      </div>
                      <div className="edit_icon non-active"></div>
                    </div>
                    <div className={bioClass}>
                      <div className="register_profile__main_info__bio__name_container"  onClick={()=>this.toggleClass('bio')} >
                        <div className="register_profile__main_info__bio__name">bio</div>
                      </div>
                      <div className="register_profile__main_info__bio__textarea">
                        <div className="temporary_input">
                          <Field  name="bio"
                            placeholder="Some words about yourself"
                            label="Bio:"
                            floatingLabel
                            type="text"
                            component={Input}
                          />
                        </div>
                        <div className="edit_icon"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className={contactClass}>
                  <div className="register_profile__contact_info__left_part" onClick={()=>this.toggleClass('contact')} >
                    <div className="text">contact info</div>
                  </div>
                  <div className="register_profile__contact_info__info_container">
                    <div className="register_profile__contact_info_email">
                      <div className="temporary_input">
                        <Field name="email"
                            placeholder="Enter your e-mail"
                            label="E-mail:"
                            floatingLabel
                            type="email"
                            disabled
                            component={Input}
                          />
                      </div>
                      <div className="edit_icon non-active"></div>
                    </div>
                    <div className="register_profile__contact_info_phone">
                      <div className="temporary_input">
                        <Field name="phone"
                            placeholder="Enter your phone number"
                            label="Phone number:"
                            floatingLabel
                            type="text"
                            component={Input}
                          />
                      </div>
                      <div className="edit_icon"></div>
                    </div>
                  </div>
                </div>

                <div className="register_profile__submit_section">
                  <MainActionButton className="btn primary" type="submit" disabled={submitting || pristine}>
                    { submitting && isRegistrationFetching ? <Spinner singleColor /> : <p>create and log in</p> }
                  </MainActionButton>
                </div>
              </div>
           </form> : <Spinner singleColor />) }
         </div>
        );
    }
}

Registration.propTypes = {};

function mapStateToProps(state) {
  const { registrationProfie, isFetching, isRegistrationFetching, registrationSuccess, loggedIn, error} = state.user;
  const departmentId = registrationProfie.departmentId;
  const findedIndex = findIndex(DEPARTMENTS_LIST, { id: departmentId });
  let department = null;

  if(findedIndex !== -1)
    department = DEPARTMENTS_LIST[findedIndex].name;

  return {
    registrationProfie,
    loggedIn,
    isFetching,
    isRegistrationFetching,
    registrationSuccess,
    initialValues: {
      email: registrationProfie.email,
      departmentId,
      department
    },
    error
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    userProfileGetByCodeAPICall,
    userProfileRegistrationCompleteAPICall
  }, dispatch);
}

const registrationReduxForm = {
  form: 'registration-form',
  enableReinitialize : true,
  validate: ({ email, department, password, bio, fullname, job, phone }, props) => {
    const errors = {};
    const { dirty } = props;
    const fieldEmpty = "Field can not be empty";

    if (!dirty) {
      return errors;
    }

    if(!bio)
      errors.bio = fieldEmpty;

    if(!fullname)
      errors.fullname = fieldEmpty;

    if(!job)
      errors.job = fieldEmpty;

    if(!phone)
      errors.phone = fieldEmpty;

    if(!password) {
      errors.password = 'Password too short';
    } else {
      if (!password.match(PASSWORD)) {
        errors.password = 'Invalid password';
      }
    }

    return errors;
  },
  onSubmit: function(values, dispatch, props) {
    const { id } = props.routeParams;
    const code = id;
    const { email, password, bio, job, departmentId, fullname, phone } = values;
    const profile = {
      biography: bio,
      department: departmentId,
      name: fullname,
      password,
      email,
      phone,
      job
    };

    dispatch(userProfileRegistrationCompleteAPICall(code, profile));
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(reduxForm(registrationReduxForm)(Registration));
