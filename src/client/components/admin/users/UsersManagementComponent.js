import {connect} from 'react-redux';
import React, {Component} from 'react';
import PropTypes from 'prop-types';

import Breadcrumb from '../../UIComponents/Breadcrumb';
import MainActionButton from '../../UIComponents/MainActionButton';
import TableHeadComponent from './table/TableHeadComponent';
import TableBodyComponent from './table/TableBodyComponent';
import UserInvitePopup from './UserInvitePopup';

import {
  userManagementGetUsers,
  userManagementInviteSend,
  userManagementInviteDialogToggle
} from '../../../actions/admin/users/userManagementActions';

import {SORTING_FIELDS} from '../../../constants/General';

class UsersManagementComponent extends Component {

  static propTypes = {
    users: PropTypes.array.isRequired,
    count: PropTypes.number.isRequired,
    isFetching: PropTypes.bool.isRequired,
    isInviteDialogOpened: PropTypes.bool.isRequired,
    userManagementGetUsers: PropTypes.func.isRequired,
    userManagementInviteDialogToggle: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);

    this.state = {
      sortType: null,
      sortOrder: null,
      filterMobileMenuOpened: false
    };

    this.handleSort = this.handleSort.bind(this);
    this.openHeaderMenu = this.openHeaderMenu.bind(this);
    this.closeHeaderMenu = this.closeHeaderMenu.bind(this);
  }

  componentDidMount() {
    this.menuContainer.addEventListener('click', this.openHeaderMenu);
  }

  componentWillUnmount() {
    document.body.removeEventListener('click', this.closeHeaderMenu);
    clearTimeout(this.openMenu);
    clearTimeout(this.closeMenu);
  }

  handleSort(type){
    const {sortType, sortOrder} = this.state;
    let newsSortType = type;
    let newSortOrder = 'asc';

    if(newsSortType === sortType){
      if(sortOrder === 'asc'){newSortOrder = 'desc';}
      else {newSortOrder = 'asc';}
    }
    
    this.setState({
      sortOrder: newSortOrder,
      sortType: newsSortType
    });

    const {userManagementGetUsers, users} = this.props;
    userManagementGetUsers(users.length, 0, sortType, sortOrder);
  }

  openHeaderMenu() {
    this.setState({filterMobileMenuOpened: true});
    this.openMenu = setTimeout(() => {
      this.menuContainer.removeEventListener('click', this.openHeaderMenu);
      document.body.addEventListener('click', this.closeHeaderMenu);
    }, 100);
  }

  closeHeaderMenu() {
    document.body.removeEventListener('click', this.closeHeaderMenu);
    this.closeMenu = setTimeout(() => {
      this.menuContainer.addEventListener('click', this.openHeaderMenu);
      this.setState({filterMobileMenuOpened: false});
    }, 100);
  }

  render() {
    const {
      isInviteDialogOpened,
      userManagementInviteDialogToggle
    } = this.props;

    const {
      sortType,
      sortOrder,
      filterMobileMenuOpened
    } = this.state;

    let breadcrumbComponents = (
      <MainActionButton action={() => userManagementInviteDialogToggle(true)}>
        <p>ADD NEW USER</p>
      </MainActionButton>
    );
    let mobileMenuClass = 'single-user-item-table__name_container__filter_button_container';

    if (filterMobileMenuOpened) { mobileMenuClass += ' active'; }

    let sortingByRegistrationClasses = "single-user-item-table__name_container__filter_button_container__filter_item ";
    let sortingByStatusClasses = "single-user-item-table__name_container__filter_button_container__filter_item ";
    let sortingByRoleClasses = "single-user-item-table__name_container__filter_button_container__filter_item ";

    switch(sortType){
      case SORTING_FIELDS['ROLE']: {
        if(sortOrder == 'asc'){sortingByRoleClasses+= "asc-sorting-direction-filter-item";}
        else {sortingByRoleClasses+= "desc-sorting-direction-filter-item";}
        break;
      }

      case SORTING_FIELDS['DATE']: {
        if(sortOrder == 'asc'){sortingByRegistrationClasses+= "asc-sorting-direction-filter-item";}
        else {sortingByRegistrationClasses+= "desc-sorting-direction-filter-item";}
        break;
      }

      case SORTING_FIELDS['STATUS']: {
        if(sortOrder == 'asc'){sortingByStatusClasses+= "asc-sorting-direction-filter-item";}
        else {sortingByStatusClasses+= "desc-sorting-direction-filter-item";}
        break;
      }
    }

    return (
      <div className="tgpoint-content-body managment-users">
        <Breadcrumb pageTitle="MANAGE USERS SCREEN" components={breadcrumbComponents}/>
        <div className="single-user-items-table">
          <div className="single-user-item-table__name_container">
            <div className="single-user-item-table__name">
              <p>list of users</p>
            </div>
            <div className={mobileMenuClass} ref={(div) => { this.menuContainer = div; }}>
              <div className="single-user-item-table__name_container__button">
                <p>sort</p>
              </div>
              <ul className="single-user-item-table__name_container__filter_button_container__filter_container">
                <li onClick={()=>{ this.handleSort(SORTING_FIELDS['DATE']); }} 
                  className={sortingByRegistrationClasses}>
                  <p>by date</p>
                </li>
                <li onClick={()=>{ this.handleSort(SORTING_FIELDS['STATUS']);}} 
                  className={sortingByStatusClasses}>
                  <p>by status</p>
                </li>
                <li onClick={()=>{ this.handleSort(SORTING_FIELDS['ROLE']); }} 
                  className={sortingByRoleClasses}>
                  <p>by role</p>
                </li>
              </ul>
            </div>
          </div>

          <TableHeadComponent sortType={sortType} sortOrder={sortOrder} handleSort={this.handleSort}/>
          <TableBodyComponent />
          { isInviteDialogOpened && <UserInvitePopup /> }
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({admin}) => {
  const {usersManagement} =  admin;
  const {users, count, isFetching,isInviteDialogOpened} = usersManagement;
  return {users, count, isFetching, isInviteDialogOpened};
};

const mapDispatchToProps = ({
  userManagementGetUsers,
  userManagementInviteSend,
  userManagementInviteDialogToggle
});

export default connect(mapStateToProps, mapDispatchToProps)(UsersManagementComponent);
