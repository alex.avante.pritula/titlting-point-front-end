import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import React, {Component} from 'react';
import InfiniteScroll from 'redux-infinite-scroll';
import SingleUserItemManageableComponent from './SingleUserItemManageableComponent';

import {LIMIT} from '../../../../constants/General';

import {
  userManagementGetUsers,
  userManagementInviteSend,
  userManagementDeleteUser
} from '../../../../actions/admin/users/userManagementActions';

class TableBodyComponent extends Component{

  static propTypes = {
    users: PropTypes.array.isRequired,
    count:  PropTypes.number.isRequired,
    editUser: PropTypes.func.isRequired,
    selectUser: PropTypes.func.isRequired,
    deleteUser: PropTypes.func.isRequired,
    isFetching: PropTypes.bool.isRequired,
    resendInvite: PropTypes.func.isRequired,
    selectedUserId: PropTypes.number.isRequired,
    userManagementGetUsers: PropTypes.func.isRequired,
    userManagementInviteSend: PropTypes.func.isRequired,
    userManagementDeleteUser: PropTypes.func.isRequired
  };

  constructor(props){
    super(props);
    this.state = {selectedUserId: null};
    this.selectUser = this.selectUser.bind(this);
    this.handleScrollDown = this.handleScrollDown.bind(this);
  }

  componentWillMount() {
    const {userManagementGetUsers} = this.props;
    userManagementGetUsers(LIMIT, 0);
  }

  handleScrollDown(){
    const {users, userManagementGetUsers} = this.props;
    userManagementGetUsers(LIMIT + users.length, 0);
  }

  selectUser(selectedUserId) {
    this.setState({selectedUserId});
  }

  render() {
    
    const {
      count,
      users,
      editUser,
      isFetching,
      userManagementInviteSend,
      userManagementDeleteUser
    } = this.props;

    const {selectedUserId} = this.state;
    const currentLength = users.length;

    let userItems  = users.map((user, index) => {
      let isOpened = (user.id === selectedUserId);
      let isSpecial = (index === currentLength - 1 && currentLength > 3);

      return (
        <SingleUserItemManageableComponent
          key={index}
          user={user}
          opened={isOpened}
          isSpecial={isSpecial}
          onUserEdit={editUser}
          isFetching={isFetching}
          onUserSelect={this.selectUser}
          onUserDelete={userManagementDeleteUser}
          onUserResendInvite={userManagementInviteSend}
        />
      );
    });

    let needToLoad = (count <= currentLength);

    return (
      <InfiniteScroll
        loader={null}
        loadingMore={needToLoad}
        elementIsScrollable={true}
        loadMore={this.handleScrollDown}
        className="single-user-item-table-body">
        {userItems}
      </InfiniteScroll>
    );
  }
}

const mapStateToProps = ({admin}) => {
  const {usersManagement} = admin;

  const {
    users, 
    count, 
    isFetching
  } = usersManagement;

  return {
    users, 
    count, 
    isFetching
  };
};

const mapDispatchToProps = ({
  userManagementGetUsers,
  userManagementInviteSend,
  userManagementDeleteUser
});

export default connect(mapStateToProps, mapDispatchToProps)(TableBodyComponent);
