import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import moment from 'moment';
import {Link} from 'react-router';

import Spinner from 'react-mdl/lib/Spinner';
import {USER_ROLES} from '../../../../constants/General';
import RemoveBlockToolTip from '../../../Dashboard/RemoveBlockToolTip';

class SingleUserItemManageableComponent extends Component {

  static propTypes = {
    user: PropTypes.object.isRequired,
    opened: PropTypes.bool.isRequired,
    isSpecial: PropTypes.bool.isRequired,
    isFetching: PropTypes.func.isRequired,
    onUserSelect: PropTypes.func.isRequired,
    onUserDelete: PropTypes.func.isRequired,
    onUserResendInvite: PropTypes.func.isRequired,
  }

  constructor(props){
    super(props);
    this.state = {isUserRemoveDialogOpen: false};
    this.handleRemoveToolTipDialog = this.handleRemoveToolTipDialog.bind(this);
    this.handleCloseRemoveToolTipDialog = this.handleCloseRemoveToolTipDialog.bind(this);
    this.handleToogleRemoveToolTipDialog = this.handleToogleRemoveToolTipDialog.bind(this);
  }

  handleToogleRemoveToolTipDialog(){
    const {isUserRemoveDialogOpen} = this.state;
    this.setState({isUserRemoveDialogOpen: !isUserRemoveDialogOpen});
  }

  handleCloseRemoveToolTipDialog(){
    this.setState({isUserRemoveDialogOpen: false});
  }

  handleRemoveToolTipDialog(){
    const {onUserDelete, user} = this.props;
    const {id} = user;

    onUserDelete(id).then(()=>{
        this.setState({isUserRemoveDialogOpen: false});
    });
  }

  render(){

    const {
      opened,
      user,
      isSpecial,
      isFetching,
      onUserSelect,
      onUserResendInvite
    } = this.props;

    const {isUserRemoveDialogOpen} = this.state;

    const {
      id,
      role,
      name,
      email,
      isEnabled,
      createdAt,
      avatarUrl,
      isVerified
    } = user;

    let registrationDate = createdAt ? moment(createdAt).format('MM-DD-YYYY') : null;
    let currentRole = USER_ROLES.find((tempRole)=>{
      return (tempRole.id === role.id);
    });
    let roleTitle = '';
    if (currentRole) { roleTitle = currentRole.title; }

    let removeToolTip = null;
    let status = !isEnabled || !isVerified ? 'Pending' : 'Enabled';
    let tupleClasses = classnames('single-user-item-manageable', {
      'pending': !isEnabled || !isVerified,
      'opened': opened,
      'single-user-spetific-last-child': isSpecial
    });

    if (isUserRemoveDialogOpen) {
       removeToolTip = (
        <RemoveBlockToolTip 
          sectionTitle={'user'}
          isFetching={isFetching} 
          name={name} 
          onClose={this.handleCloseRemoveToolTipDialog} 
          onRemove={this.handleRemoveToolTipDialog}/>
       );
    } 

    return (
      <div className={tupleClasses} onClick={() => onUserSelect(id)}>
        {removeToolTip}
        <div className="single-user-item-manageable__section_first">
          <div className="single-user-item-manageable__section_first__left_part">
            <div className="single-user-item-manageable__section_first__left_part__img_container">
              {avatarUrl && <img className="single-user-item-manageable__section_first__left_part__img" src={avatarUrl} />}
              {!avatarUrl && <span className="single-user-item-manageable__section_first__left_part__img icon-user-avatar-invite" />}
            </div>
          </div>
          <div className="single-user-item-manageable__section_first__right_part">
            <div className="single-user-item-manageable__section_first__right_part__role">
              {roleTitle}
            </div>
            <div className="single-user-item-manageable__section_first__right_part__name_lastname_container">
              <div className="single-user-item-manageable__section_first__right_part__name_lastname_container_name">
                {name}
              </div>
            </div>
          </div>
        </div>
        <div className="single-user-item-manageable__hidden_wrapper">
          <div className="single-user-item-manageable__section_second">
            <div className="single-user-item-manageable__section_second__email">e-mail:</div>
            <div className="single-user-item-manageable__section_second__current_email">
              <p>{email}</p>
            </div>
          </div>
          <div className="single-user-item-manageable__section_third">
            <div className="single-user-item-manageable__section_third__registration_date">
              <div className="single-user-item-manageable__section_third__registration_date__name">
                registration date:
              </div>
              <div className="single-user-item-manageable__section_third__registration_date__current_date">
                {registrationDate}
              </div>
            </div>
            <div className="single-user-item-manageable__section_third__status">
              <div className="single-user-item-manageable__section_third__status_name">
                status:
              </div>
              <div className="single-user-item-manageable__section_third__status__current_status">
                <p>{status}</p>
              </div>
            </div>
          </div>
          <div className="single-user-item-manageable__section_fourth">
            <p>{roleTitle}</p>
          </div>
          <div className="single-user-item-manageable__section_fifth">
            <div className="single-user-item-manageable__section_fifth__left_part">
              actions
            </div>
            <div className="single-user-item-manageable__section_fifth__right_part">
              <div className="single-user-item-manageable__section_fifth__right_part__delete" 
              onClick={this.handleToogleRemoveToolTipDialog}>
                <span className="icons ic-user-delete-user"/>
              </div>
              <Link to={`/admin/users/${id}`}
                className="single-user-item-manageable__section_fifth__right_part__edit">
                <span className="icons ic-edit-inactive"/>
              </Link>
              <div className="single-user-item-manageable__section_fifth__right_part__resend" 
                onClick={() => onUserResendInvite(id)}>
                {(isFetching && opened) ? <Spinner/> : <span className="icons ic-send-invite"/>}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SingleUserItemManageableComponent;
