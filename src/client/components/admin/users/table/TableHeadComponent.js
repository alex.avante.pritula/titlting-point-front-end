import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {SORTING_FIELDS} from '../../../../constants/General';

class TableHeadComponent extends Component{
  
  static propTypes = {
      sortType: PropTypes.string.isRequired,
      sortOrder: PropTypes.string.isRequired,
      handleSort: PropTypes.func.isRequired
  };

  constructor(props){
      super(props);
  }

  render(){
    const {sortType, sortOrder, handleSort} = this.props;
    let sortingByRegistrationClasses = "single-user-item-table-head__section_third__button ";
    let sortingByStatusClasses = "single-user-item-table-head__section_fourth__button ";
    let sortingByRoleClasses = "single-user-item-table-head__section_fifth__button ";

    switch(sortType){
      case SORTING_FIELDS['ROLE']: {
        if(sortOrder == 'asc'){sortingByRoleClasses+= "asc-sorting-direction";}
        else {sortingByRoleClasses+= "desc-sorting-direction";}
        break;
      }

      case SORTING_FIELDS['DATE']: {
        if(sortOrder == 'asc'){sortingByRegistrationClasses+= "asc-sorting-direction";}
        else {sortingByRegistrationClasses+= "desc-sorting-direction";}
        break;
      }

      case SORTING_FIELDS['STATUS']: {
        if(sortOrder == 'asc'){sortingByStatusClasses+= "asc-sorting-direction";}
        else {sortingByStatusClasses+= "desc-sorting-direction";}
        break;
      }
    }

    return (
      <div className="single-user-item-table-head">
        <div className="single-user-item-table-head__section_first">
          <p>username</p>
        </div>
        <div className="single-user-item-table-head__section_second">
          <p>e-mail</p>
        </div>
        <div className="single-user-item-table-head__section_third">
          <div className="single-user-item-table-head__section_third__name">
            <p>registration date</p>
          </div>
          <div onClick={()=>{handleSort(SORTING_FIELDS['DATE']);}} 
               className={sortingByRegistrationClasses}>
            <p>sort</p>
          </div>
        </div>
        <div className="single-user-item-table-head__section_fourth">
          <div className="single-user-item-table-head__section_fourth__name">
            <p>status</p>
          </div>
          <div onClick={()=>{handleSort(SORTING_FIELDS['STATUS']);}} 
            className={sortingByStatusClasses}>
            <p>sort</p>
          </div>
        </div>
        <div className="single-user-item-table-head__section_fifth">
          <div className="single-user-item-table-head__section_fifth__name">
            <p>role</p>
          </div>
          <div onClick={()=>{handleSort(SORTING_FIELDS['ROLE']);}} 
            className={sortingByRoleClasses}>
            <p>sort</p>
          </div>
        </div>
        <div className="single-user-item-table-head__section_sixth">
          <p>actions</p>
        </div>
      </div>
    );
  }
}

export default TableHeadComponent;