//import DatePicker from 'react-datetime';
import React from 'react';
import DateTimePicker from 'react-widgets/lib/DateTimePicker';
import 'react-widgets/lib/scss/react-widgets.scss';

import moment from 'moment';
import momentLocaliser from 'react-widgets-moment';
momentLocaliser(moment);

const renderDateTimePicker = ({ input: { onChange, value }, showTime, disabled }) => {
  return (<DateTimePicker
    onChange={onChange}
    format="DD/MM/YYYY"
    time={showTime}
    disabled={disabled}
    value={!value ? null : new Date(value)}
  />);
};

export default renderDateTimePicker;