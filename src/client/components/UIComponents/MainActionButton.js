import React from 'react';
import PropTypes from 'prop-types';

/**
 * Main action button component for all application pages
 * @param props
 * @return Component
 * @constructor
 */
const MainActionButton = (props) => {
  const {
    action,
    children
  } = props;
  return (
    <button className="main-action-button btn primary" onClick={action} {...props}>
      {children}
    </button>
  );
};

MainActionButton.propTypes = {
  action: PropTypes.func.isRequired,
  children: PropTypes.object.isRequired
};

export default MainActionButton;
