import React from 'react';
import { Textfield } from 'react-mdl';

const Input = props => (
  <Textfield
    floatingLabel={props.floatingLabel}
    name={props.name}
    autoComplete={props.autocomplete}
    disabled={props.disabled}
    label={props.label}
    type={props.type}
    autoComplete={props.autoComplete}
    error={props.meta.error}
    rows={props.rows}
    {...props.input}
  />
);
export default Input;
