import React from 'react';
import PropTypes from 'prop-types';
/**
 * Breadcrumb component for showing
 * @param props
 * @returns {XML}
 * @constructor
 */
const Breadcrumb = (props) => {
  const {pageTitle, components} = props;
  // Used for render additional components in container head
  const actions = components ? components : null;

  return (
    <div className="tgpoint-breadcrumb">
      <div className="breadcrumb-text">
        <p className="welcome">Welcome to the</p>
        <p className="dashboard-name">{pageTitle}</p>
      </div>
      <div className="breadcrumb-actions">
        {actions}
      </div>
    </div>
  );
};

Breadcrumb.propTypes = {
  pageTitle: PropTypes.object.isRequired,
  components: PropTypes.object.isRequired
};

export default Breadcrumb;
