"use strict";

import React, { Component } from 'react';
import classNames from 'classnames';

import {VIEW_LIST, VIEW_GRID} from '../../constants/General';
/**
 * TODO: Remove it when component is done
 * Old one action component
 *
 <div className="action-item sort-view">
   <button onClick={()=>{ this.handleViewType(0); }}>
    <span className={blockClasses} />
   </button>
   <button onClick={()=>{ this.handleViewType(1); }}>
    <span className={gridClasses} />
   </button>
 </div>
 */

class SortViewChanger extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { type, toggleSwitchViewMode } = this.props;

    return (
      <div className="tgpoint--sort-view-block">
        <div className="">
          <span
            className={classNames('icon-list-view', type === VIEW_GRID ? 'inactive': null)}
            onClick={() => toggleSwitchViewMode(VIEW_LIST)}>
          </span>
        </div>
        <div className="">
          <span
            className={classNames('icon-grid-view', type === VIEW_LIST ? 'inactive': null)}
            onClick={() => toggleSwitchViewMode(VIEW_GRID)}>
          </span>
        </div>
      </div>
    );
  }
}

export default SortViewChanger;
