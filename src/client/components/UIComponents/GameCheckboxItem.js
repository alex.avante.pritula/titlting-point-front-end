import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Checkbox} from 'react-mdl';

class GameCheckboxItem extends Component {

  static propTypes = {
    id: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    index: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    disabled: PropTypes.boolean.isRequired,
    isChecked: PropTypes.boolean.isRequired
  };

  constructor(props){
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(){
    const {index, onChange, isChecked} = this.props;
    onChange(index, !isChecked);
  }

  render(){
    const {
      id,
      image,
      name,
      disabled,
      isChecked
    } = this.props;
    
    const ifImage = image ? image : "assets/images/titan.png";
    const ifName = name ? name : "Game";

    return (  
      <div id={id} className={"game_checkbox_item " + (isChecked ? "selected" : "")}  >
        <label>
          <div className="game_checkbox_item__image">
            <img src={ifImage} alt={ifName} />
          </div>
          <div className="game_checkbox_item__name">{ifName}</div>
          <Checkbox
            ripple={false}
            disabled={disabled}
            checked={isChecked}
            onChange={this.handleChange} />
          <div className="game_item_background" />
        </label>
      </div>  
    );
  }
}

export default GameCheckboxItem;