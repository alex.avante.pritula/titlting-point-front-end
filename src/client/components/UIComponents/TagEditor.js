import React, {Component} from 'react';
import PropTypes from 'prop-types';
//import ReactDOM from 'react-dom';
import {AutoComplete} from 'react-mdl-extra';

class TagInput extends Component {

  static propTypes = {
    title: PropTypes.string.isRequired,  
    existItems: PropTypes.array.isRequired,
    disabled: PropTypes.boolean.isRequired,
    dataIndex: PropTypes.string.isRequired,
    onAddNewTag: PropTypes.func.isRequired,
    onRemoveTag: PropTypes.func.isRequired,
    valueIndex: PropTypes.string.isRequired,
    itemsForAutoComplete: PropTypes.array.isRequired,
  }; 

  constructor(props) {
    super(props);

    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.handleRemoveTag = this.handleRemoveTag.bind(this);
    this.handleSelectTag = this.handleSelectTag.bind(this);
  }

  componentDidMount(){
    /*document.getElementById('textfield-Writenewgamecategory').addEventListener('keydown', ()=>{
        console.warn('wwwwwwwwwwwwwwwwww');
    });*/
  }

  componentWillUnmount(){
     //this.refs['autoComplete'].input.inputRef; //.removeEventListener('keydown', this.handleKeyDown);
  }

  handleRemoveTag(index){
    const {
     disabled,
     existItems,
     onRemoveTag
    } = this.props;

    if (existItems.length > 1 && !disabled){ onRemoveTag(index); }
  }

  handleKeyDown(){
    //console.warn(e, "!1111111");
  }

  handleSelectTag(data){
    if(typeof data === 'number'){
      const {
      //  valueIndex,
        onAddNewTag, 
      //  itemsForAutoComplete
      } = this.props;

     /* const existIndex = itemsForAutoComplete.findIndex((item)=>{
        return (item[valueIndex] === data);
      });*/
      onAddNewTag(data);
     //onAddNewTag(existIndex);
    }
  }

  render() {
    const {
      title,
      disabled,
      dataIndex,
      valueIndex,
      existItems,
      itemsForAutoComplete
    } = this.props; 

    let existTags = existItems.map((item, index)=>{
      let tagNavigation = null;
      if(!disabled){
         tagNavigation = (<span onClick={()=>{ 
           this.handleRemoveTag(index); 
         }}>x</span>);
      }
      return (
        <li key={index} >
            {item[dataIndex]}
            {tagNavigation}
        </li>
      );
    });
    
    console.warn(itemsForAutoComplete);

    return (
        <ul className="tag-editor-container">
          {existTags}
          <AutoComplete
            ref="autoComplete"
            //ref={(node)=>{ this.Input = node.input.inputRef; }} 
            label={title}
            disabled={disabled}
            dataIndex={dataIndex}
            valueIndex={valueIndex}
            items={itemsForAutoComplete}
            onChange={this.handleSelectTag}
          />
        </ul>
    );
  }
}

export default TagInput;
