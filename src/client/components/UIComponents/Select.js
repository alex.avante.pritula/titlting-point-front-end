import React from 'react';
import PropTypes from 'prop-types';
import { SelectField } from 'react-mdl-extra';

/**
 * MDL Selectfild wrapper for redux-form Field
 * @param input
 * @param label
 * @param children
 * @param custom
 * @param error
 * @constructor
 */
const Select = ({input, label, children, meta: {error}, ...props}) =>
  <SelectField
    // bsStyle={'link'}
    noCaret
    label={label}
    error={error}
    value={input.value}
    children={children}
    {...input}
    {...props}
  />;

Select.propTypes = {
  label: PropTypes.string.isRequired,
  input: PropTypes.shape({
    onChange: PropTypes.func.isRequired,
    value: PropTypes.string.isRequired
  }),
  children: PropTypes.object,
  meta: PropTypes.object
};

export default Select;
