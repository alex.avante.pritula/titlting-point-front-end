import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import InfiniteScroll from 'redux-infinite-scroll';
import Breadcrumb from '../UIComponents/Breadcrumb';
import UserDirectoryItem from './UserDirectoryItem';
import {SelectField, Option} from 'react-mdl-extra';

import {LIMIT} from '../../constants/General';
import {DEPARTMENTS_LIST} from '../../constants/Departments';

import {getUsersListCompleteAPICall} from '../../actions/User/Profile';

const DEPARTMENTS = [{ id: null, name: 'All' }].concat(DEPARTMENTS_LIST);

class UserDirectory extends Component {

  static propTypes = {
    users: PropTypes.array.isRequired,
    totalCount: PropTypes.number.isRequired,
    getUsersListCompleteAPICall: PropTypes.func.isRequired
  };

  constructor(props){
    super(props);
    this.state = {sortType: null};
    this.handleScrollDown = this.handleScrollDown.bind(this);
    this.handleSortingType = this.handleSortingType.bind(this);
  }

  componentWillMount(){
    const {getUsersListCompleteAPICall} = this.props;
    getUsersListCompleteAPICall(LIMIT, 0);
  }

  handleScrollDown(){
    const {sortType} = this.state;
    const {users, getUsersListCompleteAPICall} = this.props;
    getUsersListCompleteAPICall(LIMIT, users.length, sortType);
  }

  handleSortingType(newSortType){
    const {sortType} = this.state;
    if (newSortType === sortType){ return; }
    this.setState({sortType: newSortType});
    const {users, getUsersListCompleteAPICall} = this.props;
    let limit = users.length;
    if (limit === 0) { limit = LIMIT; }
    getUsersListCompleteAPICall(limit, 0, newSortType, true);
  }
  
  render(){ 
    const {sortType} = this.state;
    const {users, totalCount} = this.props;
    const currentLength = users.length;
    
    let currentType = '';
    let needToLoad = (totalCount <= currentLength);
    let sortItems = DEPARTMENTS.map((type, index)=>{
      const {id, name} = type;
      if(sortType === id){ currentType = name; }
      return (
        <Option key={index} value={id}>
          <label className={"user_directory_filter_container_label"} >{name}</label>
        </Option>
      );
    });

    let userItems = users.map((user, index)=>{

      const {
        id,
        biography,
        name,
        email,
        phone,
        avatarUrl,
        departmentId
      } = user;

      let department = DEPARTMENTS_LIST.find((department)=>{
        return (department.id === departmentId);
      });

      return (
          <UserDirectoryItem
            id={id}
            key={index}
            name={name}
            phone={phone}
            email={email}
            bio={biography}
            image={avatarUrl}
            department={department.name}
            zindex={currentLength - index}
         />
      );
    });
    
    return(
      <div className="tgpoint-content-body user_directory-container">
        <Breadcrumb pageTitle="USER DIRECTORY" components={null} />
        <div className="user_directory_filter_container">
          <div className="actions">
            <div className="action-item sortable-filter">
              <p className="sort-by">Department</p>
              <SelectField
                noCaret
                bsStyle="link"
                id="departments-sorting"
                label={currentType}
                onChange={type => { this.handleSortingType(type); }}>
                {sortItems}
              </SelectField>
            </div>
          </div>  
        </div>
        <InfiniteScroll 
            loader={null}
            loadingMore={needToLoad}
            elementIsScrollable={true}
            loadMore={this.handleScrollDown}
            className="user_directory_items_container">
            {userItems}
        </InfiniteScroll>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const {userDirectory} = state;
  const {totalCount, list} = userDirectory;
  return {
    totalCount,
    users: list  
  };
}


function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getUsersListCompleteAPICall
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(UserDirectory);