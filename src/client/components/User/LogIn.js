import React, {Component} from 'react';
import {connect} from 'react-redux';
import {reduxForm, Field, reset} from 'redux-form';
import {browserHistory, Link} from 'react-router';
import PropTypes from 'prop-types';

/* --- CUSTOM COMPONENTS --- */
import Input from '../UIComponents/Input';
import {Spinner} from 'react-mdl';

/* --- CONSTANTS --- */
import {
  START_PAGE,
  VALIDATION,
  PASSWORD_RECOVERY_PAGE
} from '../../constants/General';

/* --- ACTIONS --- */
import {logIn} from '../../actions/User/Auth';
import 'react-toastify/dist/ReactToastify.min.css' ;

const {EMAIL, PASSWORD} = VALIDATION;

class LogIn extends Component {
  static propTypes = {
    logIn: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired,
    shouldRedirect: PropTypes.bool.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    submitting: PropTypes.bool.isRequired,
    pristine: PropTypes.bool.isRequired,
    isHasErrors: PropTypes.object.isRequired,
  };

  render() {
    const {handleSubmit, submitting, pristine} = this.props;
    return (
      <div className="noauth-popup-modal login">
        <div className="noauth-popup-modal-box">
          <label className="title">LOG IN</label>
          <hr className="title-delimiter"/>
          <form className="form-box" onSubmit={handleSubmit}>
            <section className="noauth-popup-modal-body">
              <Field name="email"
                label="Email:"
                floatingLabel
                type="text"
                autoComplete="off"
                component={Input}
              />
              <Field name="password"
                label="Password:"
                floatingLabel
                type="password"
                autoComplete="off"
                component={Input}
              />
              <div className="pass-recovery">
                <Link className="pass-recovery-link" to={PASSWORD_RECOVERY_PAGE}>Forgot the password?</Link>
              </div>
            </section>
            <section className="noauth-popup-modal-footer">
              <button className="btn primary" type="submit" disabled={submitting || pristine}>
                { submitting ? <Spinner singleColor /> : <p>LOG IN</p> }
              </button>
            </section>
          </form>
        </div>
      </div>
    );
  }
}

/* --------- REDUX PROPS MAP --------- */
const mapStateToProps = (state) => {
  const {error} = state.user;
  return {isHasErrors: error};
};
const mapDispatchToProps = ({logIn});
/* --------- REDUX LOGIN FORM --------- */
const loginReduxForm = {
  form: 'login-form',
  validate: ({email, password}, props) => {
    const {dirty} = props;
    const errors = {};

    if (!dirty) {
      return errors;
    }

    if(!email) {
      errors.email = 'Invalid email';
    } else {
      if (!email.match(EMAIL)) {
        errors.email = 'Invalid email';
      }
    }

    if(!password) {
      errors.password = 'Password can\'t be empty';
    } else {
      if (!password.match(PASSWORD)) {
        errors.password = 'Password should contain uppercase, lowercase and numeric characters';
      }
    }
    return errors;
  },
  onSubmit: logIn,
  onSubmitSuccess: (result, dispatch) => {
    browserHistory.push(START_PAGE);
    dispatch(reset('login-form'));
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(reduxForm(loginReduxForm)(LogIn));
