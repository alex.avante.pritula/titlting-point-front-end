import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {Link} from 'react-router';

class UserDirectoryItem extends Component {

  static propTypes = {
    id: PropTypes.number.isRequired,
    bio: PropTypes.string.isRequired,
    role: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    phone: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,
    zindex: PropTypes.number.isRequired,
    department: PropTypes.string.isRequired
  };

  constructor(props){
    super(props);

    this.state = {
      bioIsOpen: false,
      contactIsOpen: false,
      bioMaxHeight: 'none'
    };
    
    this.toggleClass = this.toggleClass.bind(this);
  }
  
  componentDidMount() {
    window.addEventListener('resize', this.resizeBioSectionContainer);
  }
  
  componentWillUnmount() {
    window.removeEventListener('resize', this.resizeBioSectionContainer);
  }
  
  resizeBioSectionContainer() {
    this.setState({bioMaxHeight: 'none'});
  }
  
  toggleClass(param) {
    if (param == 'bio') {
      if (this.state.bioIsOpen) {
        this.setState({bioMaxHeight: getComputedStyle(this.BioContainer).height});
      }
      this.setState({bioIsOpen: !this.state.bioIsOpen});
    }
    if (param == 'contact') {
      this.setState({contactIsOpen: !this.state.contactIsOpen});
    }
  }
  
  render(){
    const {
      id, 
      name, 
      email, 
      bio, 
      phone, 
      image, 
      department,
      zindex
    } = this.props;

    const zindexItem = {zIndex: zindex};
    const userImage = image ? image : '/assets/images/no_image.png';
    const {contactIsOpen} = this.state;
    const {bioIsOpen} = this.state;
    
    let contactClass = 'user_directory_contact_section';
    let bioClass = 'user_directory_bio_section';
    
    let maxHeight;
    if (bioIsOpen) {
      maxHeight = { maxHeight: this.state.bioMaxHeight};
    } else {
      maxHeight = { maxHeight: '0px'};
    }
    if (bioIsOpen) { bioClass += ' opened';}
    if (contactIsOpen) { contactClass += ' opened'; }
    
    return(
      <div className="user_directory_items_container__item" style={zindexItem}>
        <div className="user_directory_items_container__item__profile_container">
          <div className="user_directory_items_container__item__profile_container__photo_section">
            <img src={userImage} alt={name} />
            <Link to={`admin/users/${id}`} >
              <button className="edit_link" >
                <span className="icons ic-edit-active"/>
              </button>
            </Link>
            <div className="user_directory_name_container">
              <p>{name}</p>
            </div>
          </div>
          <div className="user_directory_items_container__item__profile_container__info_section">
            <div className="user_directory_items_container__item__profile_container__info_section__inner_container">
              <div className="user_directory_role_section">
                <p>{department}</p>
              </div>
              <div className={bioClass}>
                <div className="user_directory_bio_section__header" onClick={()=>this.toggleClass('bio')}>
                  <p>Bio<span> :</span></p>
                </div>
                <div className="user_directory_bio_section__body" style={maxHeight} 
                     ref={(div) => { this.BioContainer = div; }}>
                  <div className="user_directory_bio_section__body__padding_container">
                    {bio}
                  </div>
                </div>
              </div>
              <div className={contactClass}>
                <div className="user_directory_contact_section__header" onClick={()=>this.toggleClass('contact')}>
                  <p>Contact<span> :</span></p>
                </div>
                <div className="user_directory_contact_section__body">
                  <div className="user_directory_contact_section__body__email">
                    <p>Email:</p>
                    <a href={`mailto:${email}`}>{email}</a>
                  </div>
                  <div className="user_directory_contact_section__body__phone">
                    <p>Phone:</p>
                    <a href={`tel:${phone}`}>{phone}</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default UserDirectoryItem;