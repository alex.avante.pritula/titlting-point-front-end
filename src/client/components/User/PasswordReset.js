import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {reduxForm, Field, reset} from 'redux-form';
import {/*browserHistory,*/ Link} from 'react-router';

/* --- CUSTOM COMPONENTS --- */
import Input from '../UIComponents/Input';
import MainActionButton from '../UIComponents/MainActionButton';
import {Spinner, Tooltip} from 'react-mdl';
import {
 /* getFormValues,
  getFormInitialValues,*/
  getFormSyncErrors,
 /* getFormMeta,
  getFormAsyncErrors,
  getFormSyncWarnings,
  getFormSubmitErrors,
  getFormNames,
  isDirty,
  isPristine,
  isValid,
  isInvalid,
  isSubmitting,
  hasSubmitSucceeded,
  hasSubmitFailed*/
} from 'redux-form';
/* --- CONSTANTS --- */
import {
  LOGIN_PAGE,
  VALIDATION
} from '../../constants/General';

/* --- ACTIONS --- */
import { userPasswordChange } from '../../actions/User/passwordChangeActions';

import 'react-toastify/dist/ReactToastify.min.css' ;
const {PASSWORD, TOKEN} = VALIDATION;

class PasswordReset extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const { routeParams } = this.props;
    this.props.initialize({ resetToken: routeParams.id});
  }

  componentWillReceiveProps() {
    const {syncErrors} = this.props;

    console.log('HEY', syncErrors)
  }

  render() {
    const { handleSubmit, submitting, pristine, error, /*statusMessage,*/ isFetching, routeParams/*, syncErrors*/} = this.props;
    const passwordResetToken = routeParams.id;
    const isTokenValid = TOKEN.test(passwordResetToken);



    return (
      <div className="noauth-popup-modal change-pass">
        <div className="noauth-popup-modal-box">
          <label className="title">
            <Link to={LOGIN_PAGE}>
              <Tooltip label="Return to login page" position="top">
                <span className="icons clickable ic-back-navigation-black"></span>
              </Tooltip>
            </Link>
            Password reset
          </label>
          <hr className="title-delimiter"/>
          <form className="form-box" onSubmit={handleSubmit}>
            <section className="noauth-popup-modal-body">
              <Field name="resetToken" value={passwordResetToken} type="text" hidden component="input" readOnly/>
              { isTokenValid ?
                <div>
                  <Field name="password"
                        label="New password:"
                        floatingLabel
                        type="password"
                        autoComplete="off"
                        component={Input}
                  />
                  <Field name="confirmPassword"
                        label="Confirm password:"
                        floatingLabel
                        type="password"
                        autoComplete="off"
                        component={Input}
                  />
                </div>
              :
              <p className="pass-recovery-message">Sorry, but you provided not valid token! Try again!</p>
            }
            </section>
            <section className="noauth-popup-modal-footer">
              {
                isTokenValid ?
                  <MainActionButton className="btn primary" type="submit" disabled={submitting || pristine || isFetching || error}>
                    {submitting || isFetching ? <Spinner singleColor/> : <p>CHANGE PASSWORD</p>}
                  </MainActionButton>
                :
                  <MainActionButton className="btn primary" type="submit">
                    <p>RETURN TO LOGIN PAGE</p>
                  </MainActionButton>
              }
            </section>
          </form>
        </div>
      </div>
    );
  }
}

PasswordReset.propTypes = {
  error: PropTypes.object()
};

/* --------- REDUX PROPS MAP --------- */
const mapStateToProps = (state) => {
  const { isFetching } = state.recoveryPassword;
  const syncErrors = getFormSyncErrors('password-reset-form')(state);
  return { isFetching, syncErrors };
};

const mapDispatchToProps = ({userPasswordChange});

const resetPasswordReduxForm = {
  form: 'password-reset-form',
  validate: ({resetToken, password, confirmPassword}, props) => {
    const errors = {};
    const {dirty} = props;
    if (!dirty) {
      return errors;
    }
    if (!password) {
      errors.password = 'Invalid password';
    } else {
      if (!password.match(PASSWORD)) {
        errors.password = 'Password should contain uppercase, lowercase and numeric characters';
      }
    }

    if (!confirmPassword) {
      errors.confirmPassword = 'Invalid password';
    } else {
      if (!confirmPassword.match(PASSWORD)) {
        errors.confirmPassword = 'Invalid password';
      }
      if(confirmPassword !== password) {
        errors.confirmPassword = 'Passwords not match';
      }
    }
    return errors;
  },
  onSubmit: ({resetToken, password, confirmPassword}, dispatch) => {
    dispatch(userPasswordChange(resetToken, password));
  },
  onSubmitSuccess: (result, dispatch) => {
    dispatch(reset('password-reset-form'));
  }
};


export default connect(mapStateToProps, mapDispatchToProps)(reduxForm(resetPasswordReduxForm)(PasswordReset));
