import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Dropzone from 'react-dropzone';
import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import Input from '../UIComponents/Input';
import {reduxForm, Field, submit} from 'redux-form';
import Breadcrumb from '../UIComponents/Breadcrumb';
import MainActionButton from '../UIComponents/MainActionButton';

//import GameCheckboxItem from '../UIComponents/GameCheckboxItem';

import {
  getCurrentUserProfile,
  updateUserByIdCompleteAPICall,
  userAvatarChangeCompleteAPICall,
  currentUserUpdateCompleteAPICall,
  updateCurrentUserCompleteAPICall,
  getSelectedUserProfileCompleteAPICall,
  selectedUserAvatarChangeCompleteAPICall,
  updateCurrentUserPasswordCompleteAPICall
} from '../../actions/User/Profile';

import {USER_ROLES,VALIDATION} from '../../constants/General';
import {DEPARTMENTS_LIST} from '../../constants/Departments';


const {EMAIL, PASSWORD} = VALIDATION;

class Profile extends Component {

    static propTypes = {
        userId: PropTypes.number.isRequired,
        profile: PropTypes.object.isRequired,
        params: PropTypes.object.isRequired,
        submit: PropTypes.func.isRequired,
        selectedUser: PropTypes.object.isRequired,
        isFetching: PropTypes.boolean.isRequired,
        getCurrentUserProfile: PropTypes.func.isRequired,
        userAvatarChangeCompleteAPICall: PropTypes.func.isRequired, 
        currentUserUpdateCompleteAPICall: PropTypes.func.isRequired,
        getSelectedUserProfileCompleteAPICall: PropTypes.func.isRequired,
        selectedUserAvatarChangeCompleteAPICall: PropTypes.func.isRequired
    };

    constructor(props){
       super(props);

       this.state = {
           file: null,
           editField: null,
           bioIsOpen: false,
           gameIsOpen: false,
           contactIsOpen: false,
           passwordIsOpen: false,
           isOpenSecuritySettings: false,
           isOpenGamesPermission: false,
           gameMaxHeight: '5000px'
       };

       this.toggleClass = this.toggleClass.bind(this);
       this.handleOnDrop = this.handleOnDrop.bind(this);
       this.handleEditField = this.handleEditField.bind(this);
       this.handleUpdateField = this.handleUpdateField.bind(this);
       this.resizeGameSectionContainer = this.resizeGameSectionContainer.bind(this);
       this.handleBlockPermissionsToogle = this.handleBlockPermissionsToogle.bind(this);
       this.handleSequritySettingsToogle = this.handleSequritySettingsToogle.bind(this);
    }

    componentWillMount(){
      const {params, getSelectedUserProfileCompleteAPICall} = this.props;
      const {id} = params;
      if (id) { getSelectedUserProfileCompleteAPICall(id); }
    }

    componentDidMount() {
       window.addEventListener('resize', this.resizeGameSectionContainer);
    }
    
    componentWillUnmount() {
       window.removeEventListener('resize', this.resizeGameSectionContainer);
    }

    resizeGameSectionContainer() {
       this.setState({ gameMaxHeight: '5000px' }); // WTF? 
    }

    handleOnDrop(acceptedFiles) {
       const file = acceptedFiles[0];

       const {
         //params,
         getCurrentUserProfile,
         userAvatarChangeCompleteAPICall,
         //selectedUserAvatarChangeCompleteAPICall
       } = this.props;

       this.setState({
        file: file,
        editField: null
       });

      /* const {id} = params;
       
       if(id !== null){
        selectedUserAvatarChangeCompleteAPICall(id, file)
          .then(()=>{ return getSelectedUserProfileCompleteAPICall(id); });
       } else { */

        userAvatarChangeCompleteAPICall(file)
          .then(()=>{ return getCurrentUserProfile(); });
      // }
    }

    handleEditField(field){
      const {editField} = this.state;
      if (field === editField) this.setState({editField: null}); 
      else this.setState({editField: field}); 
    }    

    handleUpdateField(field, value){
      currentUserUpdateCompleteAPICall(field, value)
        .then(()=>{
            return getCurrentUserProfile();
        });
    }
    
    handleSequritySettingsToogle(){
      const {isOpenSecuritySettings} = this.state;
      this.setState({ isOpenSecuritySettings: !isOpenSecuritySettings});
    }

    handleBlockPermissionsToogle(){
      const {isOpenGamesPermission} = this.state;
      this.setState({ isOpenGamesPermission: !isOpenGamesPermission});
    }

    toggleClass(param) {
      if (param == 'bio') { 
        this.setState({ bioIsOpen: !this.state.bioIsOpen }); 
      }

      if (param == 'contact') {
         this.setState({ contactIsOpen: !this.state.contactIsOpen });
      }

      if (param == 'password') { 
        this.setState({ passwordIsOpen: !this.state.passwordIsOpen }); 
      }

      if (param == 'game') {
        if (this.state.gameIsOpen) {
          this.setState({ gameMaxHeight: getComputedStyle(this.GamesContainer).height });
        }
        this.setState({ gameIsOpen: !this.state.gameIsOpen });
      }
    }

    render(){
       const {
         file,
         editField,
         bioIsOpen,
         gameIsOpen,
         contactIsOpen,
         passwordIsOpen,
       } = this.state;

       const {
         profile,
         isFetching,
         submit,
         params
       } = this.props;

       let maxHeight = { maxHeight: '0px'};
       if (gameIsOpen) { 
         maxHeight = { maxHeight: this.state.gameMaxHeight }; 
       } 

       let preview = null;
       let avatarImage = "/assets/svg/human_ico.svg";

       if (file) { preview = file.preview; } 
       if (preview) { avatarImage = preview; } 
       else if(profile.avatarUrl) { avatarImage = profile.avatarUrl; }

       let bioClass = 'user_profile__main_info__bio';
       let contactClass = 'user_profile__contact_info';
       let passwordClass = 'user_profile_security_settings_section';
       let gameClass = 'user_profile_game_accessibility_section';

       let roleItems = USER_ROLES.map((user, index)=>{
          const {id, title} = user;
          return (<option key={index} value={id}>{title}</option>);
       });

       let deparmentItems = DEPARTMENTS_LIST.map((department, index)=>{
          const {id, name} = department;
          return (<option key={index} value={id}>{name}</option>);
       });

       if (bioIsOpen) {  bioClass += ' opened'; }
       if (contactIsOpen) { contactClass += ' opened';}
       if (passwordIsOpen) { passwordClass += ' opened';}

       return (
         <div className="tgpoint-content-body">
           <Breadcrumb pageTitle="USER PROFILE SCREEN" components={null} />
           <div className="user_profile__container">
                <form ref={(node)=>{ this.form = node; }} >
                <div className="user_profile__header">
                  <div className="user_profile__header__icon">
                    <img src="/assets/svg/human_ico.svg" alt="User profile" />
                  </div>
                </div>
                <div className="user_profile__main_info">
                  <div className="user_profile__main_info__left_part">
                    <div 
                     onClick={()=>{ if(!isFetching){ this.dropZone.open(); }}} 
                     className="register_profile__main_info__photo__container">
                      <Dropzone disabled={isFetching} 
                        accept={'image/*'} 
                        disableClick={true}
                        onDrop={this.handleOnDrop} 
                        ref={(node) => { this.dropZone = node; }}
                        className="register_profile__main_info__photo">
                        <img src={avatarImage} alt="Your profile"/>
                        <p className="mobile_photo_text">Tap to set photo</p>
                        <p className="desktop_photo_text">Click to set photo</p>
                      </Dropzone>
                    </div>
                    <div className="register_profile__main_info__select_container">
                       <Field name="roleId" 
                          disabled={isFetching || !params.id}
                          onChange={()=>{ submit('remoteSubmit'); }} 
                          component="select">
                         {roleItems}
                       </Field>
                    </div>
                  </div>
                  <div className="user_profile__main_info__inputs_container">
                    <div className="user_profile__main_info__job">
                      <div className="temporary_input">
                        <Field name="job"
                            placeholder="Enter your position"
                            label="Job:"
                            floatingLabel
                            type="text"
                            disabled={isFetching}
                            autoComplete="off"
                            component={Input}
                            onFocus={()=>{ this.handleEditField('job'); }}
                            onBlur={()=>{
                              this.handleEditField('job');
                              submit('remoteSubmit'); 
                            }}
                          />
                      </div>
                      <div className="edit_icon">
                        <span className={"icons ic-edit-" + (editField === 'job' ? "" :"in") + "active"} />
                      </div>
                    </div>
                    <div className="user_profile__main_info__full_name">
                      <div className="temporary_input">
                        <Field name="name"
                          placeholder="Please enter your name"
                          label="Full name:"
                          floatingLabel
                          type="text"
                          disabled={isFetching}
                          autoComplete="off"
                          component={Input}
                          onFocus={()=>{ this.handleEditField('name'); }}
                          onBlur={()=>{
                              this.handleEditField('name');
                              submit('remoteSubmit'); 
                          }}
                        />
                      </div>
                      <div className="edit_icon">
                        <span className={"icons ic-edit-" + (editField === 'name' ? "" :"in") + "active"} />
                      </div>
                    </div>
                    <div className="user_profile__main_info__department">
                      <div className="user_profile__main_info__department__name_container">
                        <p>Department:</p>
                      </div>
                      <div className="user_profile__main_info__department__select_container">
                        <Field name="departmentId" 
                          disabled={isFetching}
                          onChange={()=>{ submit('remoteSubmit'); }} 
                          component="select">
                          {deparmentItems}
                        </Field>
                      </div>
                    </div>
                    <div className={bioClass}>
                      <div className="user_profile__main_info__bio__name_container"  
                        onClick={()=>this.toggleClass('bio')} >
                        <div className="user_profile__main_info__bio__name">bio</div>
                      </div>
                      <div className="user_profile__main_info__bio__textarea">
                        <div className="temporary_input">
                          <Field name="biography"
                            placeholder="Some words about user"
                            label="Bio:"
                            disabled={isFetching}
                            floatingLabel
                            type="text"
                            autoComplete="off"
                            component={Input}
                            onFocus={()=>{ this.handleEditField('bio'); }}
                            onBlur={()=>{
                              this.handleEditField('bio');
                              submit('remoteSubmit'); 
                            }}
                            rows={5}
                          />
                        </div>
                        <div className="edit_icon">
                        <span className={"icons ic-edit-" + (editField === 'bio' ? "" :"in")  + "active"} />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className={contactClass}>
                  <div className="user_profile__contact_info__left_part" onClick={()=>this.toggleClass('contact')} >
                    <div className="text">contact info</div>
                  </div>
                  <div className="user_profile__contact_info__info_container">
                    <div className="user_profile__contact_info_email">
                      <div className="temporary_input">
                        <Field name="email"
                            placeholder="Enter your e-mail"
                            label="E-mail:"
                            floatingLabel
                            type="email"
                            disabled={isFetching}
                            autoComplete="off"
                            component={Input}
                            onFocus={()=>{ this.handleEditField('email'); }}
                            onBlur={()=>{
                              this.handleEditField('email');
                              submit('remoteSubmit'); 
                            }}
                          />
                      </div>
                      <div className="edit_icon">
                        <span className={"icons ic-edit-" + (editField === 'email' ? "in" :"") + "active"} />
                      </div>
                    </div>
                    <div className="user_profile__contact_info_phone">
                      <div className="temporary_input">
                        <Field name="phone"
                            placeholder="Enter your phone number"
                            label="Phone number:"
                            floatingLabel
                            type="text"
                            disabled={isFetching}
                            autoComplete="off"
                            onFocus={()=>{ this.handleEditField('phone'); }}
                            onBlur={()=>{
                              this.handleEditField('phone');
                              submit('remoteSubmit'); 
                            }}
                            component={Input} />
                      </div>
                      <div className="edit_icon">
                        <span className={"icons ic-edit-" + (editField === 'phone' ? "in" :"") + "active"} />
                      </div>
                    </div>
                  </div>
                </div>
                <div className={passwordClass}>
                  <div className="user_profile_security_settings_section__heading" 
                       onClick={()=>this.toggleClass('password')}>
                    <span className="icons ic-settings" />
                    <p>Security settings</p>
                  </div>
                  <div className="user_profile_security_settings_section__password_container">
                    <div>
                      <div className="temporary_input">
                        <Field 
                          disabled={isFetching}
                          name="oldpassword"
                          placeholder="Enter curent password"
                          label="Current password:"
                          floatingLabel
                          type="password"
                          autoComplete="off"
                          component={Input}
                          onFocus={()=>{ this.handleEditField('oldpassword'); }}
                          onBlur={()=>{ this.handleEditField('oldpassword'); }} />                        
                      </div>
                      <span className={"icons ic-edit-" + (editField === 'oldpassword' ? "" :"in") + "active"} />
                    </div>
                    <div>
                      <div className="temporary_input">
                        <Field 
                          disabled={isFetching}
                          name="newpassword"
                          placeholder="Enter new password"
                          label="New password:"
                          floatingLabel
                          type="password"
                          autoComplete="off"
                          component={Input}
                          onFocus={()=>{ this.handleEditField('newpassword'); }}
                          onBlur={()=>{
                            this.handleEditField('newpassword');
                          }} />                        
                      </div>
                      <span className={"icons ic-edit-" + (editField === 'newpassword' ? "" :"in") + "active"} />
                    </div>
                    <div>
                      <div className="temporary_input">
                        <Field 
                          disabled={isFetching}
                          name="confirmpassword"
                          placeholder="Confirm new password"
                          label="Confirm new password:"
                          floatingLabel
                          type="password"
                          autoComplete="off"
                          component={Input}
                          onFocus={()=>{ this.handleEditField('confirmpassword'); }}
                          onBlur={()=>{ this.handleEditField('confirmpassword'); }} />                        
                      </div>
                      <span className={"icons ic-edit-" + (editField === 'confirmpassword' ? "" :"in") + "active"} />
                    </div>
                   <MainActionButton type="button" action={()=>{submit('remoteSubmit');}}>
                     <p>Update password</p>
                   </MainActionButton>
                   </div>
                </div>
                <div className={gameClass}>
                  <div className="user_profile_game_accessibility_section__header" 
                       onClick={()=>this.toggleClass('game')}>
                    <p>Game access</p>
                  </div>
                  <div style={maxHeight} ref={(div) => { this.GamesContainer = div; }} 
                       className="user_profile_game_accessibility_section__game_checkbox_container" />
                </div>
                </form>
              </div>
         </div>
        );
    }
}

const registrationReduxForm = {
  form: 'user-profile-form',
  enableReinitialize: true,
  validate: (values, props) => {
    const {dirty} = props;
    const errors = {};

    if (!dirty) {
      return errors;
    }
    const {
      email, biography, job, name, phone,
      confirmpassword, newpassword, oldpassword
   } = values;

    if(confirmpassword || newpassword || oldpassword) {

      if(oldpassword.length < 1){
        errors.oldpassword = 'Current password is empty';
      } else {
        if (!oldpassword.match(PASSWORD)) {
            errors.oldpassword = 'Invalid current password';
        }
      }

      if(newpassword.length < 1){
        errors.newpassword = 'New password is empty';
      } else {
         if (!newpassword.match(PASSWORD)) {
            errors.newpassword = 'Invalid new password';
        }
      }

      if(confirmpassword.length < 1){
        errors.confirmpassword = 'Confirm password is empty';
      } else {
        if (!confirmpassword.match(PASSWORD)) {
            errors.confirmpassword = 'Invalid confirm password';
        }
      }

      if(!errors.confirmpassword && !errors.newpassword && confirmpassword !== newpassword){
          errors.confirmpassword = 'The entered passwords do not match';
      }
      return errors;
    }

    if(!email) {
      errors.email = 'Invalid email';
    } else {
      if (!email.match(EMAIL)) {
        errors.email = 'Invalid email';
      }
    }

    if(!job) {
      errors.job = 'Job can\'t be empty';
    } 
   
    if(!biography) {
      errors.biography = 'BIO can\'t be empty';
    } 

    if(!name) {
      errors.name = 'Fullname can\'t be empty';
    } 

    if(!phone){
      errors.phone = 'Phone can\'t be empty';
    }

    return errors;
  },
  onSubmit:(values, dispatch)=>{

    const {
      isOwnProfile, 
      confirmpassword, 
      newpassword, 
      oldpassword,
      departmentId,
      roleId
    } = values;

    values.departmentId =  parseInt(departmentId);
    values.roleId = parseInt(roleId);  

    if (isOwnProfile){ 
      if(confirmpassword.length > 0 && newpassword.length > 0 && oldpassword.length > 0){
        updateCurrentUserPasswordCompleteAPICall({
          password: newpassword,
          oldPassword: oldpassword
        }, dispatch);
      } else {
        updateCurrentUserCompleteAPICall(values, dispatch); 
      }
    } 
   else { updateUserByIdCompleteAPICall(values, dispatch);}
  }
};

function mapStateToProps(state, ownProps) {
  const {user, userDirectory} = state;
  const {selectedUser} = userDirectory;  
  const {profile} = user;
  const {params} = ownProps;

  if(params.id){
     return {
        profile,
        selectedUser,
        isFetching: userDirectory.isFetching,
        initialValues: {
          id: selectedUser.id,
          isOwnProfile: false,
          biography: selectedUser.biography,
          departmentId: selectedUser.departmentId,
          email: selectedUser.email,
          job: selectedUser.job,
          name: selectedUser.name,
          phone: selectedUser.phone,
          roleId: selectedUser.roleId,
          confirmpassword: '',
          newpassword: '',
          oldpassword: ''
        }
    };
  }
  return {
    profile,
    selectedUser,
    isFetching: user.isFetching,
    initialValues: {
      id: profile.id,
      isOwnProfile: true,
      biography: profile.biography,
      departmentId: profile.departmentId,
      email: profile.email,
      job: profile.job,
      name: profile.name,
      phone: profile.phone,
      roleId: profile.roleId,
      confirmpassword: '',
      newpassword: '',
      oldpassword: ''
    }
  };  
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    submit,
    getCurrentUserProfile,
    userAvatarChangeCompleteAPICall,
    getSelectedUserProfileCompleteAPICall,
    selectedUserAvatarChangeCompleteAPICall,
    updateCurrentUserPasswordCompleteAPICall
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(reduxForm(registrationReduxForm)(Profile));
