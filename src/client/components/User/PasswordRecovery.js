import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {reduxForm, Field, reset} from 'redux-form';
import {/*browserHistory,*/ Link} from 'react-router';

/* --- CUSTOM COMPONENTS --- */
import Input from '../UIComponents/Input';
import MainActionButton from '../UIComponents/MainActionButton';
import {Spinner, Tooltip} from 'react-mdl';

/* --- CONSTANTS --- */
import {
  LOGIN_PAGE,
  VALIDATION
} from '../../constants/General';

/* --- ACTIONS --- */
import { userPasswordReset } from '../../actions/User/passwordRecoveryActions';

import 'react-toastify/dist/ReactToastify.min.css' ;
const {EMAIL} = VALIDATION;

class PasswordRecovery extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {handleSubmit, submitting, pristine/*, error,*/, statusMessage, isFetching, isEmailSended} = this.props;

    return (
      <div className="noauth-popup-modal recovery">
        <div className="noauth-popup-modal-box">
          <label className="title">
            <Link to={LOGIN_PAGE}>
              <Tooltip label="Return to login page" position="top">
                <span className="icons clickable ic-back-navigation-black"></span>
              </Tooltip>
            </Link>
            Password recovery
          </label>
          <hr className="title-delimiter"/>
          <form className="form-box" onSubmit={handleSubmit}>
            <section className="noauth-popup-modal-body">
              <p className="pass-recovery-message">{statusMessage}</p>
              { !isEmailSended ? <Field name="email"
                     label="Email:"
                     floatingLabel
                     type="text"
                     autoComplete="off"
                     component={Input}
              /> : null }
            </section>
            <section className="noauth-popup-modal-footer">
            {
              !isEmailSended ?
              <MainActionButton className="btn primary" type="submit" disabled={ submitting || pristine || isFetching }>
                {submitting || isFetching ? <Spinner singleColor/> : <p>RECOVER</p>}
              </MainActionButton>
              :
              <MainActionButton type="button">
                <Link className="btn primary" to={LOGIN_PAGE}>
                  <p>TO LOGIN PAGE</p>
                </Link>
              </MainActionButton>
              }
            </section>
          </form>
        </div>
      </div>
    );
  }
}

PasswordRecovery.propTypes = {
  shouldRedirect: PropTypes.bool.isRequired,
  statusMessage: PropTypes.string,
  isFetching: PropTypes.bool,
  isEmailSended: PropTypes.bool,
  error: PropTypes.object()
};

/* --------- REDUX PROPS MAP --------- */
const mapStateToProps = ({recoveryPassword}) => {
  const { statusMessage, isFetching, isEmailSended, error } = recoveryPassword;
  return {
    statusMessage,
    isFetching,
    isEmailSended,
    error
  };
};

const mapDispatchToProps = ({userPasswordReset});

const recoveryReduxForm = {
  form: 'password-recovery-form',
  validate: ({email}, props) => {
    const errors = {};
    const {dirty} = props;
    if (!dirty) {
      return errors;
    }
    if (!email) {
      errors.email = 'Invalid email';
    } else {
      if (!email.match(EMAIL)) {
        errors.email = 'Invalid email';
      }
    }
    return errors;
  },
  onSubmit: ({email}, dispatch) => {
    dispatch(userPasswordReset({email}));
  },
  onSubmitSuccess: (result, dispatch) => {
    dispatch(reset('password-recovery-form'));
  }
};


export default connect(mapStateToProps, mapDispatchToProps)(reduxForm(recoveryReduxForm)(PasswordRecovery));
