import {
    SET_IS_MOBILE_STATUS,
    LEFT_NAVIGATION_MENU_TOOGLE
} from '../constants/Navigation';

export function navigationMenuToogle() {
    return (dispatch) => {
        dispatch({type: LEFT_NAVIGATION_MENU_TOOGLE});
    };
}

export function setMobileSizeStatus(isMobile) {
    return (dispatch) => {
        dispatch({type: SET_IS_MOBILE_STATUS, isMobile});
    };
}
