import { 
  GET_GAMES_START,
  GET_GAMES_FAIL,
  GET_GAMES_SUCCESS,

  GAME_OPEN_MODAL,
  GAME_CLOSE_MODAL, 

  GET_ALL_GENRES_REQUEST,
  GET_ALL_GENRES_SUCCESS,
  GET_ALL_GENRES_FAIL,

  GET_SELECTED_GAME_FAIL,
  GET_SELECTED_GAME_REQUEST,
  GET_SELECTED_GAME_SUCCESS,

  REMOVE_SELECTED_GAME_FAIL,
  REMOVE_SELECTED_GAME_REQUEST,
  REMOVE_SELECTED_GAME_SUCCESS,

  GET_ALL_EXIST_GAMES_FOR_SIMILAR_FAIL,
  GET_ALL_EXIST_GAMES_FOR_SIMILAR_REQUEST,
  GET_ALL_EXIST_GAMES_FOR_SIMILAR_SUCCESS,

  TOOGLE_SELECTED_GAME_EDITING_MODE,
  UPLOAD_SELECTED_GAME_PREVIEW,
  UPDATE_SIMILAR_GAME_STATUS,

  REMOVE_SELECTED_GAME_GENRE,
  ADD_SELECTED_GAME_GENRE,

  CREATE_NEW_GAME_FAIL,
  CREATE_NEW_GAME_REQUEST,
  CREATE_NEW_GAME_SUCCESS,

  UPDATE_SELECTED_GAME_FAIL,
  UPDATE_SELECTED_GAME_REQUEST,
  UPDATE_SELECTED_GAME_SUCCESS
  
} from '../../constants/Game';

import Game from '../../services/Game';

export function getGames(direction, offset, limit, reload){
    return (dispatch) => {
      dispatch({ type: GET_GAMES_START });
      return Game.getGames({direction, offset, limit})
          .then((result)=>{
             const {games, count}  = result;
             dispatch({ type: GET_GAMES_SUCCESS, games, count, reload });
          }).catch((error)=>{
             dispatch({ type: GET_GAMES_FAIL, error });
          });
    };
}

export const openModalDialog = () => (dispatch) => {
  dispatch({type: GAME_OPEN_MODAL});
};

export const closeModalDialog = () => (dispatch) => {
  dispatch({type: GAME_CLOSE_MODAL});
};

function getAllGenresRequest(){
  return {
    type: GET_ALL_GENRES_REQUEST
  };
}

function getAllGenresSuccess(genres){
  return {
    type: GET_ALL_GENRES_SUCCESS,
    genres
  };
}

function getAllGenresFail(error){
  return {
    type: GET_ALL_GENRES_FAIL,
    error
  };
}

export const getAllGenresCompleteAPICall = () => (dispatch) => {
  dispatch(getAllGenresRequest()); 
  return Game.getAllGenres()
    .then((res)=>{
      dispatch(getAllGenresSuccess(res));
    }).catch((error)=>{
        dispatch(getAllGenresFail(error));
    });
};

function getSelectedGameRequest(){
  return {
    type: GET_SELECTED_GAME_REQUEST
  };
}

function getSelectedGameSuccess(game){
  return {
    type: GET_SELECTED_GAME_SUCCESS,
    game
  };
}

function getSelectedGameFail(error){
  return {
    type: GET_SELECTED_GAME_FAIL,
    error
  };
}

export const getSelectedGameCompleteAPICall = (id) => (dispatch) => {
  dispatch(getSelectedGameRequest()); 
  return Game.getGameById(id)
    .then((res)=>{
      dispatch(getSelectedGameSuccess(res));
    }).catch((error)=>{
        dispatch(getSelectedGameFail(error));
    });
};

function toogleGameEditingMode(){
  return {
    type: TOOGLE_SELECTED_GAME_EDITING_MODE
  };
}

export const toogleGameEditingModeCall = () => (dispatch) => {
  dispatch(toogleGameEditingMode());
};

function removeSelectedGameRequest(){
  return {
    type: REMOVE_SELECTED_GAME_REQUEST
  };
}

function removeSelectedGameSuccess(){
  return {
    type: REMOVE_SELECTED_GAME_SUCCESS
  };
}

function removeSelectedGameFail(error){
  return {
    type: REMOVE_SELECTED_GAME_FAIL,
    error
  };
}

export const removeSelectedGameCompleteAPICall = (id) => (dispatch) => {
  dispatch(removeSelectedGameRequest());
  return Game.removeGame(id)
    .then(()=>{
      dispatch(removeSelectedGameSuccess());

    }).catch((error)=>{
      dispatch(removeSelectedGameFail(error));
    });
};

function getAllExistGamesRequest(){
  return {
    type: GET_ALL_EXIST_GAMES_FOR_SIMILAR_REQUEST
  };
}

function getAllExistGamesSuccess(games){
  return {
    type: GET_ALL_EXIST_GAMES_FOR_SIMILAR_SUCCESS,
    games
  };
}

function getAllExistGamesFail(error){
  return {
    type: GET_ALL_EXIST_GAMES_FOR_SIMILAR_FAIL,
    error
  };
}

export const getAllExistGamesCompleteAPICall = (count) => (dispatch) => {
  dispatch(getAllExistGamesRequest());
  return Game.getGames({offset: 0, count})
    .then((res)=>{
      const {games} = res;
      dispatch(getAllExistGamesSuccess(games));
    }).catch((error)=>{
      dispatch(getAllExistGamesFail(error));
    });
};

function uploadGamePreview(file){
  return {
    type: UPLOAD_SELECTED_GAME_PREVIEW,
    file
  };
}

export const uploadGamePreviewCall = (file) => (dispatch) => {
  dispatch(uploadGamePreview(file));
};


function updateSimilarGameStatus(index, status){
  return {
    type: UPDATE_SIMILAR_GAME_STATUS,
    status,
    index
  };
}

export const updateSimilarGameStatusCall = (index, status) => (dispatch) => {
  dispatch(updateSimilarGameStatus(index, status));
};

function removeSelectedGameGenre(index){
  return {
    type: REMOVE_SELECTED_GAME_GENRE,
    index
  };
}

function addSelectedGameGenre(index){
  return {
    type: ADD_SELECTED_GAME_GENRE,
    index
  };
}

export const removeSelectedGameGenreCall = (index) => (dispatch) => {
  dispatch(removeSelectedGameGenre(index));
};

export const addSelectedGameGenreCall = (index) => (dispatch) => {
  dispatch(addSelectedGameGenre(index));
};


function createGameRequest(){
  return {
    type: CREATE_NEW_GAME_REQUEST
  };
}

function createGameSuccess(){
  return {
    type: CREATE_NEW_GAME_SUCCESS
  };
}

function creteNewGameFail(error){
  return {
    type: CREATE_NEW_GAME_FAIL,
    error
  };
}

export const createNewGameCompleteAPICall = (data, file, dispatch) => {
    dispatch(createGameRequest());
    return Game.createGame(data)
      .then((game)=>{
        const {id} = game;
        let formData = new FormData();
        formData.append('files', file);
        formData.append('gameId', id);
        return Game.createGameImage(formData);
      }).then(()=>{
        dispatch(createGameSuccess());
      }).catch((error)=>{
        dispatch(creteNewGameFail(error));
      });
};

function updateGameRequest(){
  return {
    type: UPDATE_SELECTED_GAME_REQUEST
  };
}

function updateGameSuccess(){
  return {
    type: UPDATE_SELECTED_GAME_SUCCESS
  };
}

function updateGameFail(error){
  return {
    type: UPDATE_SELECTED_GAME_FAIL,
    error
  };
}

export const updateGameCompleteAPICall = (gameId, data, file, genresForAdd, genresForRemove, similarGamesForAdd, similarGamesForRemove, dispatch) => {
  dispatch(updateGameRequest());
  return Game.updateGame(gameId, data)
    .then(()=>{
      if(file){
        let formData = new FormData();
        formData.append('files', file);
        formData.append('gameId', gameId);
        return Game.createGameImage(formData);
      }
    }).then(()=>{
      if(genresForAdd.length > 0){
        let params = {
          gameId,
          genres: genresForAdd
        };
        return Game.addGameGenres(params);
      }
    }).then(()=>{  
      if(genresForRemove.length > 0){
        let params = {
          gameId,
          genres: genresForRemove
        };
        return Game.removeGameGenres(params);
      }
    }).then(()=>{  
      if(similarGamesForAdd.length > 0){  
        return Game.deleteSimilarGames(gameId, {ids: similarGamesForAdd});
      }
    }).then(()=>{    
      if(similarGamesForRemove.length > 0){
        return Game.deleteSimilarGames(gameId, similarGamesForRemove);
      }
    }).then(()=>{  
      dispatch(updateGameSuccess());
    }).catch((error)=>{
      dispatch(updateGameFail(error));
    });
};

function closeModalDialogCall(){
  return {
    type: GAME_CLOSE_MODAL
  };
}


function getGamesRequest(){
  return {
    type: GET_GAMES_START
  };
}

function getGamesSuccess(games, count){
  return {
    type: GET_GAMES_SUCCESS,
    reload: true,
    games,
    count
  };
}

function getGamesFail(error){
  return { 
    type: GET_GAMES_FAIL, 
    error 
  };
}

export function closeModalDialogCompleteApiCall(direction, offset, limit, dispatch){
  dispatch(closeModalDialogCall());
  dispatch(getGamesRequest());
  return Game.getGames({direction, offset, limit})
    .then((res)=>{
      const {games, count}  = res;
      dispatch(getGamesSuccess(games, count));
    }).catch((error)=>{
      dispatch(getGamesFail(error));
    });
}
