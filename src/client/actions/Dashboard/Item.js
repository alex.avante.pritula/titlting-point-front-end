import {
  MOVE_ITEM,
  START_DRAGGING_ITEM,

  ADD_ITEM_START,

  CHANGE_SELECTED_ITEM_TYPE,
  HANDLE_OPEN_ITEM_DIALOG,
  HANDLE_CLOSE_ITEM_DIALOG,

  GET_ITEM_DESCRIPTION_FAIL,
  GET_ITEM_DESCRIPTION_START,
  GET_ITEM_DESCRIPTION_SUCCESS,

  CHANGE_ITEM_POSITION_START,
  CHANGE_ITEM_POSITION_SUCCESS,
  CHANGE_ITEM_POSITION_FAIL,

  CHANGE_DATA_ITEM_FIELD_VALUE,
  UPLOAD_TEMP_ITEM_FILE,
  CREATE_DEFAULT_ITEM,

  ADD_LINK_TO_LIST_OF_LINKS,
  REMOVE_LINK_FROM_LIST_OF_LINKS,
  UPDATE_LINK_FROM_LIST_OF_LINKS,
  UPLOAD_TEMP_NEWS_ITEM_FILE,

  CREATE_LIST_OF_LINKS_START,
  CREATE_LIST_OF_LINKS_SUCCESS,
  CREATE_LIST_OF_LINKS_FAIL,

  CREATE_DEFAULT_LINK_START,
  CREATE_DEFAULT_LINK_SUCCESS,
  CREATE_DEFAULT_LINK_FAIL,

  CREATE_NEWS_START,
  CREATE_NEWS_SUCCESS,
  CREATE_NEWS_FAIL,

  REMOVE_ITEM_START,
  REMOVE_ITEM_SUCCESS,
  REMOVE_ITEM_FAIL,

  SET_EDITING_STATUS,
  SET_VALIDATION_ERRORS,

  UPDATE_DEFAULT_LINK_START,
  UPDATE_DEFAULT_LINK_SUCCESS,
  UPDATE_DEFAULT_LINK_FAIL,

  UPDATE_NEWS_START,
  UPDATE_NEWS_SUCCESS,
  UPDATE_NEWS_FAIL,

  UPDATE_LIST_OF_LINKS_START,
  UPDATE_LIST_OF_LINKS_SUCCESS,
  UPDATE_LIST_OF_LINKS_FAIL

} from '../../constants/Dashboard';

import FormData from 'form-data';
import Dashboard from '../../services/Dashboard';

const VIDEO_EMBEDED_TYPE_ID = 5;

export function setEditingStatus(isEditing){
   return (dispatch) => {
    dispatch({type: SET_EDITING_STATUS, isEditing});
  }; 
}

export function addItem(){
  return (dispatch) => {
    dispatch({type: ADD_ITEM_START});
  };
}

export function moveItem(dragIndex, hoverIndex, blockIndex){
    return (dispatch) => {
      dispatch({type: MOVE_ITEM, dragIndex, hoverIndex, blockIndex});
    };
}

export function startDraggingItem(position){
  return (dispatch) => {
    dispatch({type: START_DRAGGING_ITEM, position});
  };
}

export function endDraggingItem(id, newPosition, needToChange){
  return (dispatch) => {
    dispatch({type: CHANGE_ITEM_POSITION_START});
    if(needToChange){
      return Dashboard.changeItemPosition(id,{newPosition})
        .then(()=>{
          dispatch({type: CHANGE_ITEM_POSITION_SUCCESS});
        }).catch(()=>{
          dispatch({type: CHANGE_ITEM_POSITION_FAIL});
        });
    }
    dispatch({type: CHANGE_ITEM_POSITION_SUCCESS});
    return Promise.resolve();
  };
}

export function getItemDescription(id){
    return (dispatch) => {
      dispatch({ type: GET_ITEM_DESCRIPTION_START });
      return Dashboard.getItemDescription(id)
      .then((data)=>{
         dispatch({ type: GET_ITEM_DESCRIPTION_SUCCESS, data });
      }).catch((error)=>{
        dispatch({ type: GET_ITEM_DESCRIPTION_FAIL, error });
      });
    };
}

export function handleOpenItemDialog(blockId, itemTypeId, itemId, blockIndex, itemIndex){
    return (dispatch) => {
        dispatch({type: HANDLE_OPEN_ITEM_DIALOG, blockId, itemTypeId, itemId, blockIndex, itemIndex});
        if(itemId){
            dispatch({type: GET_ITEM_DESCRIPTION_START});
            return Dashboard.getItemDescription(itemId)
              .then((data)=>{
                dispatch({ type: GET_ITEM_DESCRIPTION_SUCCESS, data });
              }).catch((error)=>{
                dispatch({ type: GET_ITEM_DESCRIPTION_FAIL, error });
              });    
        }
        dispatch({ type: CREATE_DEFAULT_ITEM });
        return Promise.resolve();
    };
}

export function handleCloseItemDialog(){
    return (dispatch) => {
        dispatch({type: HANDLE_CLOSE_ITEM_DIALOG });
    };
}

export function changeItemType(typeId){
    return (dispatch) => {
        dispatch({type: CHANGE_SELECTED_ITEM_TYPE, typeId });
    };
}

export function changeItemValue(field, value){
    return (dispatch) => {
        dispatch({type: CHANGE_DATA_ITEM_FIELD_VALUE, field, value});
    };
}

export function uploadItemFile(file){
  return (dispatch) => {
      dispatch({type: UPLOAD_TEMP_ITEM_FILE, file});
  };
}

export function addLinkToListOfLinks(){
  return (dispatch)=>{
      dispatch({type: ADD_LINK_TO_LIST_OF_LINKS});
  };
}

export function removeLinkFromListOfLinks(index){
  return (dispatch)=>{
      dispatch({type: REMOVE_LINK_FROM_LIST_OF_LINKS, index});
  };
}

export function updateLinkFromListOfLinks(index, field, value) {
  return (dispatch)=>{
      dispatch({type: UPDATE_LINK_FROM_LIST_OF_LINKS, index, field, value});
  };
}

export function uploadNewsItemFile(file, position) {
  return (dispatch)=>{
      dispatch({type: UPLOAD_TEMP_NEWS_ITEM_FILE, position, file});
  };
}

export function createDefaultLink(link, files){
  return (dispatch) => {
    dispatch({ type: CREATE_DEFAULT_LINK_START });
    let itemId = null;
    return Dashboard.createDefaultLink(link)
      .then((res)=>{
         itemId = res.itemId;
         if(files) {
            let formData = new FormData();
            formData.append('files', files);
            formData.append('itemId', itemId);
            const {itemTypeId} = link;
            let method = ((itemTypeId !== VIDEO_EMBEDED_TYPE_ID) ? 'createDocumentLink' : 'createVideoLink');
            return Dashboard[method](formData);
         }
      }).then(()=>{
          dispatch({type: CREATE_DEFAULT_LINK_SUCCESS});
          return itemId;
      }).catch((error)=>{
         dispatch({type: CREATE_DEFAULT_LINK_FAIL, error });
      });
  };
}


export function createNews(news, files){
  return (dispatch) => {
    dispatch({type: CREATE_NEWS_START});
    let itemId = null;
    return Dashboard.createNews(news)
      .then((res)=>{
         itemId = res.itemId;
         const first = files.get('files[0]');
         const last = files.get('files[1]');
         if(first || last) {
            itemId = res.itemId;
            let formData = new FormData();
            if(first){ formData.append('files[0]', first); }
            if (last){ formData.append('files[1]', last); }
            formData.append('itemId', itemId);
            return Dashboard.createNewsImages(formData);
         }
      }).then(()=>{
          dispatch({type: CREATE_NEWS_SUCCESS});
          return itemId;
      }).catch((error)=>{
         dispatch({type: CREATE_NEWS_FAIL, error });
      });
  };
}

export function removeItem(id){
  return (dispatch) => {
    dispatch({type: REMOVE_ITEM_START});
    return Dashboard.removeItem(id)
      .then(()=>{
         dispatch({type: REMOVE_ITEM_SUCCESS});
      }).catch((error)=>{
         dispatch({type: REMOVE_ITEM_FAIL, error });
      });
  };
}

export function createListOfLinks(links){
  return (dispatch) => {
    dispatch({type: CREATE_LIST_OF_LINKS_START});
    let itemId = null;
    return Dashboard.createListOfLinks(links)
      .then((res)=>{
         itemId = res[0].itemId;
         dispatch({type: CREATE_LIST_OF_LINKS_SUCCESS});
         return itemId;
      }).catch((error)=>{
         dispatch({type: CREATE_LIST_OF_LINKS_FAIL, error});
      });
  };
}

export function updateDefaultLink(itemId, link, files){
   return (dispatch) => {
    dispatch({type: UPDATE_DEFAULT_LINK_START});
    const {id, name, linkDescription, url} = link;
    let existLinkId = id;
    return Dashboard.updateItemName(itemId, {name})
      .then(()=>{
        if(files){
            let formData = new FormData();
            formData.append('files', files);
            formData.append('itemId', itemId);
            return Dashboard['createDocumentLink'](formData);
        }
      }).then((res)=>{
        let linkToPatch = {
          title: name,
          description: linkDescription
        };

        if(res){ existLinkId = res.id; }
        else { linkToPatch['url'] = url; }

        return Dashboard.updateDefaultLink(existLinkId, linkToPatch);
      }).then(()=>{
          dispatch({type: UPDATE_DEFAULT_LINK_SUCCESS});
      }).catch((error)=>{
         dispatch({type: UPDATE_DEFAULT_LINK_FAIL, error});
      });
  };
}

export function updateNews(itemId, news, file){
   return (dispatch) => {
    dispatch({type: UPDATE_NEWS_START});
    const firstFile = file.get('files[0]');
    const lastFile  = file.get('files[1]');
    const {
      id, 
      name,
      description, 
      firstImageId, 
      lastImageId
    } = news;

    return Dashboard.updateItemName(itemId, {name})
      .then(()=>{
        return Dashboard.updateNews(id, {name, description});
      }).then(()=>{
        if(typeof firstFile === 'object' && firstFile !== null  && firstImageId !== null){
          return Dashboard.removeNewsImage(firstImageId);
        }
      }).then(()=>{
        if(typeof lastFile === 'object' && firstFile !== null && lastImageId !== null) {
          return Dashboard.removeNewsImage(lastImageId);
        }
      }).then(()=>{
         if(typeof firstFile === 'object' && firstFile !== null) {
            let formData = new FormData();
            formData.append('files', firstFile);
            formData.append('itemId', itemId);
            formData.append('position', 1);
            return Dashboard.createNewsImages(formData);
         }
      }).then(()=>{
         if(typeof lastFile === 'object' && firstFile !== null) {
            let formData = new FormData();
            formData.append('files', lastFile);
            formData.append('itemId', itemId);
            formData.append('position', 2);
            return Dashboard.createNewsImages(formData);
         }
      }).then(()=>{    
         dispatch({type: UPDATE_NEWS_SUCCESS});
      }).catch((error)=>{
         dispatch({type: UPDATE_NEWS_FAIL, error});
      });
  };
}

export function updateListOfLinks(itemId, name, removedLinks, addedLinks, updatedLinks){
   return (dispatch) => {
      dispatch({type: UPDATE_LIST_OF_LINKS_START});
      return Dashboard.updateItemName(itemId, {name})
        .then(()=>{
          if (addedLinks['links'].length > 0) {
            return Dashboard.createListOfLinks(addedLinks);
          }
        }).then(()=>{
          if (updatedLinks.length > 0){
            return Dashboard.updateListOfLinks({links: updatedLinks});
          } 
        }).then(()=>{
           if (removedLinks.length > 0) {
             return Dashboard.removeListOfLinks(removedLinks);
           }
        }).then(()=>{
           dispatch({type: UPDATE_LIST_OF_LINKS_SUCCESS});
        }).catch((error)=>{
           dispatch({type: UPDATE_LIST_OF_LINKS_FAIL, error});
        });
  };
}

export function setValidationErrors(errors){
   return (dispatch) => {
      dispatch({type: SET_VALIDATION_ERRORS, errors});
   };
}
