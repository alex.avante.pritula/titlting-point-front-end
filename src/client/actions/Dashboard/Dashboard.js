import {
  GET_DAHBOARD_SUCCESS,
  GET_DAHBOARD_FAIL,
  GET_DAHBOARD_START,

  SEARCH_IN_DASHBOARD_REQUEST,
  SEARCH_IN_DASHBOARD_SUCCESS,
  SEARCH_IN_DASHBOARD_FAIL,

  SET_DASHBOARD_QUERY_TEXT
} from '../../constants/Dashboard';

import Dashboard from '../../services/Dashboard';

export function getDashboard(dashboardId, reload, offset, limit) {
  return (dispatch) => {
    dispatch({type: GET_DAHBOARD_START});
    return Dashboard.getDashboard(dashboardId, {offset, limit})
      .then((dashboard)=>{
        dispatch({type: GET_DAHBOARD_SUCCESS, dashboard, reload});
      }).catch((error)=>{
         dispatch({type: GET_DAHBOARD_FAIL, error});
      });
  };
}

function setDashboardQuery(text){
  return {
    type: SET_DASHBOARD_QUERY_TEXT,
    text
  };
}

export const setDashboardQueryCall = (text) => (dispatch)=> {
  dispatch(setDashboardQuery(text));
};

function searchInDashboardRequest(){
  return {
    type: SEARCH_IN_DASHBOARD_REQUEST
  };
}

function searchInDashboardSuccess(items, count){
  return {
     type: SEARCH_IN_DASHBOARD_SUCCESS,
     items, 
     count
  };
}

function searchInDashboardFail(error){
  return {
    type: SEARCH_IN_DASHBOARD_FAIL,
    error
  };
}

export const searchInDashboardCompleteApiCall = (text, limit, offset) => (dispatch) => {
  dispatch(searchInDashboardRequest());
  return Dashboard.search({text, limit, offset})
    .then((res)=>{
      const {items, count} = res;
      dispatch(searchInDashboardSuccess(items, count));
    }).catch((error)=>{
      dispatch(searchInDashboardFail(error));
    });
};