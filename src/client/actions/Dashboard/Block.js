import {
  REMOVE_BLOCK_START,
  REMOVE_BLOCK_FAIL,
  REMOVE_BLOCK_SUCCESS,

  RENAME_BLOCK_START,
  RENAME_BLOCK_FAIL,
  RENAME_BLOCK_SUCCESS,

  ADD_BLOCK_START,
  ADD_BLOCK_FAIL,
  ADD_BLOCK_SUCCESS,

  MOVE_BLOCK,
  START_DRAGGING_BLOCK,

  CHANGE_BLOCK_POSITION_START,
  CHANGE_BLOCK_POSITION_SUCCESS,
  CHANGE_BLOCK_POSITION_FAIL,

  UPDATE_BLOCK_TITLE
} from '../../constants/Dashboard';

import Dashboard from '../../services/Dashboard';

export function moveBlock(dragIndex, hoverIndex) {
  return (dispatch) => {
    dispatch({ type: MOVE_BLOCK, dragIndex, hoverIndex });
  };
}

export function startDraggingBlock(position){
  return (dispatch) => {
    dispatch({ type: START_DRAGGING_BLOCK, position });
  };
}

export function endDraggingBlock(id, newPosition, needToChange){
  return (dispatch) => {
    dispatch({ type: CHANGE_BLOCK_POSITION_START });
    if(needToChange){
      return Dashboard.changeBlockPosition(id,{ newPosition })
        .then(()=>{
          dispatch({ type: CHANGE_BLOCK_POSITION_SUCCESS });
        }).catch((error)=>{
          dispatch({ type: CHANGE_BLOCK_POSITION_FAIL, error });
        });
    }
    dispatch({ type: CHANGE_BLOCK_POSITION_SUCCESS });
    return Promise.resolve();
  };
}

export function removeBlock(blockId){
    return (dispatch) => {
      dispatch({ type: REMOVE_BLOCK_START });
      return Dashboard.removeBlock(blockId)
      .then(()=>{
         dispatch({ type: REMOVE_BLOCK_SUCCESS });
      }).catch((error)=>{
        dispatch({ type: REMOVE_BLOCK_FAIL, error });
      });
    };
}

export function updateBlock(id, name, dashboardId, blockTypeId){
    return (dispatch) => {
      dispatch({ type: RENAME_BLOCK_START });
      return Dashboard.updateBlock(id, {name, dashboardId, blockTypeId })
        .then(()=>{
          dispatch({ type: RENAME_BLOCK_SUCCESS });
        }).catch((error)=>{
          dispatch({ type: RENAME_BLOCK_FAIL, error });
        });
      };
}

export function addBlock(name, dashboardId, blockTypeId){
    return (dispatch) => {
      dispatch({ type: ADD_BLOCK_START });
      return Dashboard.addBlock({ name, dashboardId, blockTypeId })
        .then(()=>{
          dispatch({ type: ADD_BLOCK_SUCCESS });
        }).catch((error)=>{
          dispatch({ type: ADD_BLOCK_FAIL, error });
        });
    };
}

export function updateBlockTitle(blockIndex, name){
   return (dispatch) => {
      dispatch({type: UPDATE_BLOCK_TITLE, blockIndex, name});
   };
}