"use strict";

import UserService from '../../services/User';

/**
 * Sends request for password recovery
 * @type {string}
 */
export const ACTION_PASSWORD_RECOVERY_REQUEST = 'ACTION_PASSWORD_RECOVERY_REQUEST';
export function actionPasswordRecoveryRequest() {
  return {
    type: ACTION_PASSWORD_RECOVERY_REQUEST
  };
}

/**
 * Called when password recovery request succeed
 * @type {string}
 */
export const ACTION_PASSWORD_RECOVERY_RECEIVE = 'ACTION_PASSWORD_RECOVERY_RECEIVE';
export function actionPasswordRecoveryReceive() {
  return {
    type: ACTION_PASSWORD_RECOVERY_RECEIVE
  };
}
/**
 * Called when password recovery request fails
 * @type {string}
 */
export const ACTION_PASSWORD_RECOVERY_FAIL = 'ACTION_PASSWORD_RECOVERY_FAIL';
export function actionPasswordRecoveryFail(error) {
  return {
    type: ACTION_PASSWORD_RECOVERY_FAIL,
    error
  };
}

/**
 * Resets user password
 * @param email
 */
export const userPasswordReset = (email) => (dispatch) => {
  dispatch(actionPasswordRecoveryRequest());

  return UserService.recoverPassword(email)
  .then(() => {
    dispatch(actionPasswordRecoveryReceive());
  })
  .catch((error) => {
    dispatch(actionPasswordRecoveryFail(error));
  });
};
