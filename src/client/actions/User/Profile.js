import {browserHistory} from 'react-router';
import {SubmissionError} from 'redux-form';

import {
    GET_PROFILE_FAIL,
    GET_PROFILE_START,
    GET_PROFILE_SUCCESS,

    GET_SELECTED_USER_PROFILE_FAIL,
    GET_SELECTED_USER_PROFILE_REQUEST,
    GET_SELECTED_USER_PROFILE_SUCCESS,

    GET_USERS_LIST_REQUEST,
    GET_USERS_LIST_SUCCESS,
    GET_USERS_LIST_FAIL,

    USER_PROFILE_GET_BY_CODE_REQUEST,
    USER_PROFILE_GET_BY_CODE_SUCCESS,
    USER_PROFILE_GET_BY_CODE_FAIL,

    USER_PROFILE_REGISTRATION_REQUEST,
    USER_PROFILE_REGISTRATION_SUCCESS,
    USER_PROFILE_REGISTRATION_FAIL,

    USER_PROFILE_CHANGE_AVATAR_REQUEST,
    USER_PROFILE_CHANGE_AVATAR_SUCCESS,
    USER_PROFILE_CHANGE_AVATAR_FAIL,

    CURRENT_USER_UPDATE_REQUEST,
    CURRENT_USER_UPDATE_FAIL,
    CURRENT_USER_UPDATE_SUCCESS,

    UPDATE_SELECTED_USER_REQUEST,
    UPDATE_SELECTED_USER_SUCCESS,
    UPDATE_SELECTED_USER_FAIL,

    UPDATE_CURRENT_USER_REQUEST,
    UPDATE_CURRENT_USER_SUCCESS,
    UPDATE_CURRENT_USER_FAIL,

    UPDATE_CURRENT_USER_PASSWORD_REQUEST,
    UPDATE_CURRENT_USER_PASSWORD_SUCCESS,
    UPDATE_CURRENT_USER_PASSWORD_FAIL,

    SEELCTED_USER_PROFILE_CHANGE_AVATAR_REQUEST,
    SELECTED_USER_PROFILE_CHANGE_AVATAR_SUCCESS,
    SELECTED_USER_PROFILE_CHANGE_AVATAR_FAIL
} from '../../constants/User';

import {USER_ROLES} from '../../constants/General';
import {logIn} from '../../actions/User/Auth';
import UserService from '../../services/User';

function getCurrentUserProfileStart(){
  return {
    type: GET_PROFILE_START
  };
}

function getCurrentUserProfileSuccess(profile){
  return {
    type: GET_PROFILE_SUCCESS,
    profile
  };
}

function getCurrentUserProfileFail(error){
  return {
    type: GET_PROFILE_FAIL,
    error
  };
}

export const getCurrentUserProfile = () => (dispatch) => {
  dispatch(getCurrentUserProfileStart());
    return UserService.getCurrentUserProfile()
      .then((data)=>{
          const {role} = data;
          const currentRoleId = role.id;
          let currentRole = USER_ROLES.find((item)=>{
            return (currentRoleId === item.id);
          });
          let profile = Object.assign({}, data, { 
              role: currentRole.title 
          });
          dispatch(getCurrentUserProfileSuccess(profile));
      }).catch((error)=>{
          dispatch(getCurrentUserProfileFail(error));
      });
};
export function userProfileGetByCodeRequest() {
  return {
    type: USER_PROFILE_GET_BY_CODE_REQUEST
  };
}

export function userProfileGetByCodeSuccess(profile) {
  return {
    type: USER_PROFILE_GET_BY_CODE_SUCCESS,
    payload: profile
  };
}

export function userProfileGetByCodeFail(error) {
  return {
    type: USER_PROFILE_GET_BY_CODE_FAIL,
    error
  };
}

export const userProfileGetByCodeAPICall = (code) => (dispatch) => {
  dispatch(userProfileGetByCodeRequest());

  return UserService.retrieveUserProfileByCode(code)
  .then(profile => {
    dispatch(userProfileGetByCodeSuccess(profile));
  })
  .catch(error => {
    dispatch(userProfileGetByCodeFail(error));
  });
};

export function userProfileRegistrationRequest() {
  return {
    type: USER_PROFILE_REGISTRATION_REQUEST
  };
}

export function userProfileRegistrationSuccess() {
  return {
    type: USER_PROFILE_REGISTRATION_SUCCESS
  };
}

export function userProfileRegistrationFail(error) {
  return {
    type: USER_PROFILE_REGISTRATION_FAIL,
    error
  };
}

export function actionUserAvatarChangeRequest() {
  return {
    type: USER_PROFILE_CHANGE_AVATAR_REQUEST
  };
}

export function actionUserAvatarChangeSuccess(){
  return {
    type: USER_PROFILE_CHANGE_AVATAR_SUCCESS
  };
}

export function actionUserAvatarChangeFail(error){
  return {
    type: USER_PROFILE_CHANGE_AVATAR_FAIL,
    error
  };
}

export const userAvatarChangeCompleteAPICall = ( avatar) => (dispatch) => {
  dispatch(actionUserAvatarChangeRequest());
  return UserService.changeAvatar(null, avatar)
    .then(() => {
      dispatch(actionUserAvatarChangeSuccess());
      return Promise.resolve();
    }).catch((error) => {
      dispatch(actionUserAvatarChangeFail(error));
    });
};

export function actionSelectedUserAvatarChangeRequest() {
  return {
    type: SEELCTED_USER_PROFILE_CHANGE_AVATAR_REQUEST
  };
}

export function actionSelectedUserAvatarChangeSuccess(){
  return {
    type: SELECTED_USER_PROFILE_CHANGE_AVATAR_SUCCESS
  };
}

export function actionSelectedUserAvatarChangeFail(error){
  return {
    type: SELECTED_USER_PROFILE_CHANGE_AVATAR_FAIL,
    error
  };
}

export const selectedUserAvatarChangeCompleteAPICall = (userId, avatar) => (dispatch) => {
  dispatch(actionSelectedUserAvatarChangeRequest());
  return UserService.changeAvatar(userId, avatar)
    .then(() => {
      dispatch(actionSelectedUserAvatarChangeSuccess());
      return Promise.resolve();
    }).catch((error) => {
      dispatch(actionSelectedUserAvatarChangeFail(error));
    });
};

export function currentUserUpdateRequest(){
  return {
    type: CURRENT_USER_UPDATE_REQUEST
  };
}

export function currentUserUpdateSuccess(){
  return {
    type: CURRENT_USER_UPDATE_SUCCESS
  };
}

export function currentUserUpdateFail(error){
  return {
    type: CURRENT_USER_UPDATE_FAIL,
    error
  };
}

export const currentUserUpdateCompleteAPICall = (field, value) => (dispatch) => {
  dispatch(currentUserUpdateRequest());
  return UserService.updateCurrentUser({field: value})
    .then(() => {
      dispatch(currentUserUpdateSuccess());
      return Promise.resolve();
    }).catch((error) => {
      dispatch(currentUserUpdateFail(error));
    });
};

export const userProfileRegistrationCompleteAPICall = (code, profile) => (dispatch) => {
  dispatch(userProfileRegistrationRequest());

  return UserService.completeRegistration(code, profile)
  .then(() => {
    dispatch(userProfileRegistrationSuccess());
    const  {email, password} = profile;
    return logIn({email, password}, dispatch);
  }).then(response => {
    const {accessToken} = response.payload;
    const {avatar} = profile;
    return UserService.uploadAvatar(avatar, accessToken);
  }).then(() => {
    browserHistory.push('/dashboard/1');
  }).catch(error => {
    dispatch(userProfileRegistrationFail(error));
  });
};

function getUsersListRequest(){
  return {
    type: GET_USERS_LIST_REQUEST
  };
}

function getUsersListSuccess(users, count, reload){
  return {
    type: GET_USERS_LIST_SUCCESS,
    reload,
    count, 
    users
  };
}

function getUsersListFail(error){
  return {
    type: GET_USERS_LIST_FAIL,
    error
  };
}

export const getUsersListCompleteAPICall = (limit, offset, departmentId, reload) => (dispatch) => {
  dispatch(getUsersListRequest());
  let query = {limit, offset};
  if(departmentId !== null) { query.departmentId = departmentId; }
  
  return UserService.getUsersList(query)
    .then((res) => {
      const {users, count} = res;
      dispatch(getUsersListSuccess(users, count, reload));
      return Promise.resolve();
    }).catch((error) => {
      dispatch(getUsersListFail(error));
    });
};

function getSelectedUserRequest(){
  return {
    type: GET_SELECTED_USER_PROFILE_REQUEST
  };
}

function getSelectedUserSuccess(profile){
  return {
    type: GET_SELECTED_USER_PROFILE_SUCCESS,
    profile
  };
}

function getSelectedUserFail(error){
  return {
    type: GET_SELECTED_USER_PROFILE_FAIL,
    error
  };
}

export const getSelectedUserProfileCompleteAPICall = (id) => (dispatch) => {
  dispatch(getSelectedUserRequest());
  return UserService.getSelectedUserProfile(id)
    .then((res) => {
      dispatch(getSelectedUserSuccess(res));
      return Promise.resolve();
    }).catch((error) => {
      dispatch(getSelectedUserFail(error));
    });
};

function updateSelectedUserRequest(){
  return {
    type: UPDATE_SELECTED_USER_REQUEST
  };
}

function updateSelectedUserSuccess(){
  return {
    type: UPDATE_SELECTED_USER_SUCCESS
  };
}

function updateSelectedUserFail(error){
  return {
    type: UPDATE_SELECTED_USER_FAIL,
    error
  };
}

export function updateUserByIdCompleteAPICall(data, dispatch){
  dispatch(updateSelectedUserRequest());
  const {id} = data;
  return UserService.updateUser(id, data)
    .then((res) => {
      dispatch(updateSelectedUserSuccess(res));
      return getSelectedUserProfileCompleteAPICall(id);
    }).catch((error) => {
      dispatch(updateSelectedUserFail(error));
    });
}

function updateCurrentUserRequest(){
  return {
    type: UPDATE_CURRENT_USER_REQUEST
  };
}

function updateCurrentUserSuccess(){
  return {
    type: UPDATE_CURRENT_USER_SUCCESS
  };
}

function updateCurrentUserFail(error){
  return {
    type: UPDATE_CURRENT_USER_FAIL,
    error
  };
}

export function updateCurrentUserCompleteAPICall(data, dispatch){
  dispatch(updateCurrentUserRequest());
  return UserService.updateCurrentUser(data)
    .then((res) => {
      dispatch(updateCurrentUserSuccess(res));
      return getCurrentUserProfile();
    }).catch((error) => {
      dispatch(updateCurrentUserFail(error));
    });
}

function updateCurrentUserPasswordRequest(){
  return {
      type: UPDATE_CURRENT_USER_PASSWORD_REQUEST
  };
}

function updateCurrentUserPasswordSuccess(){
  return {
      type: UPDATE_CURRENT_USER_PASSWORD_SUCCESS
  };
}


function updateCurrentUserPasswordFail(error){
  return {
      type: UPDATE_CURRENT_USER_PASSWORD_FAIL,
      error
  };
}

export function updateCurrentUserPasswordCompleteAPICall(data, dispatch){
    dispatch(updateCurrentUserPasswordRequest());
    return UserService.updateCurrentUserPassword(data)
      .then((res) => {
        dispatch(updateCurrentUserPasswordSuccess(res));
        return getCurrentUserProfile();
      }).catch((error) => {
        dispatch(updateCurrentUserPasswordFail(error));
        const {code, message} = error;
        if(code === 400){
          const reduxError = {oldpassword: message};
          throw new SubmissionError(reduxError);
        }
      });
}