"use strict";
import { browserHistory } from 'react-router';
import UserService from '../../services/User';


import { LOG_IN_SUCCESS } from '../../constants/User';
/**
 * Sends request for password change
 * @type {string}
 */
export const ACTION_PASSWORD_CHANGE_REQUEST = 'ACTION_PASSWORD_CHANGE_REQUEST';
export function actionPasswordChangeRequest() {
  return {
    type: ACTION_PASSWORD_CHANGE_REQUEST
  };
}

/**
 * Called when password change request succeed
 * @type {string}
 */
export const ACTION_PASSWORD_CHANGE_RECEIVE = 'ACTION_PASSWORD_CHANGE_RECEIVE';
export function actionPasswordChangeReceive() {
  return {
    type: ACTION_PASSWORD_CHANGE_RECEIVE
  };
}
/**
 * Called when password change request fails
 * @type {string}
 */
export const ACTION_PASSWORD_CHANGE_FAIL = 'ACTION_PASSWORD_CHANGE_FAIL';
export function actionPasswordChangeFail(error) {
  return {
    type: ACTION_PASSWORD_CHANGE_FAIL,
    error
  };
}

/**
 * Changes user password
 * @param {*} token
 * @param {*} password
 */
export const userPasswordChange = (token, password) => (dispatch) => {
  dispatch(actionPasswordChangeRequest());

  return UserService.resetPassword(token, password)
    .then(result => {
      const { accessToken } = result;
      dispatch(actionPasswordChangeReceive());
      dispatch({ type: LOG_IN_SUCCESS , payload: accessToken });
      localStorage.setItem('feathers-jwt', accessToken);
      return Promise.resolve();
    })
    .then(() => {
      browserHistory.push('/dashboard/1'); // ?
    })
    .catch((error) => {
      dispatch(actionPasswordChangeFail(error));
    });
};
