import {browserHistory} from 'react-router';
import {SubmissionError} from 'redux-form';

import {
    LOG_IN_FAIL,
    LOG_OUT_FAIL,
    LOG_IN_START,
    LOG_OUT_START,
    LOG_IN_SUCCESS,
    LOG_OUT_SUCCESS,
    CHECK_AUTHORIZATION
} from '../../constants/User';

import {LOGIN_PAGE} from '../../constants/General';
import UserService from '../../services/User';

export function checkAuth(){
    return (dispatch) => {
      return UserService.isAuthenticated()
        .then((loggedIn)=>{
            dispatch({ type: CHECK_AUTHORIZATION, loggedIn });
        }).catch(()=>{
            dispatch({
                type: CHECK_AUTHORIZATION,
                loggedIn: false
            });
        });
    };
}

export function logIn(values, dispatch) {
  const {email, password} = values;
  dispatch({type: LOG_IN_START});
  return UserService.logIn(email, password)
    .then(token => {
      return dispatch({ type: LOG_IN_SUCCESS , payload: token });
    })
    .catch((error) => {
      dispatch({type: LOG_IN_FAIL, error});
      if(error.code === 401 || error.code === 400){
        const reduxError = { 
            email: 'Server error: Invalid email or password', 
            password: 'Server error: Invalid email or password' 
        };
        throw new SubmissionError(reduxError);
      }
    });
}

export function logOut(){
    return (dispatch) => {
      dispatch({ type: LOG_OUT_START });
      return UserService.logOut()
        .then(()=>{
            dispatch({ type: LOG_OUT_SUCCESS });
            browserHistory.push(LOGIN_PAGE);
        }).catch(()=>{
            dispatch({ type: LOG_OUT_FAIL });
        });
    };
}
