import {SubmissionError} from 'redux-form';
import UserService from '../../../services/User';

export const USER_MANAGEMENT_GET_USERS_REQUEST = 'USER_MANAGEMENT_GET_USERS_REQUEST';
export function userManagementGetUsersRequest() {
  return {
    type: USER_MANAGEMENT_GET_USERS_REQUEST
  };
}

export const USER_MANAGEMENT_GET_USERS_RECEIVE = 'USER_MANAGEMENT_GET_USERS_RECEIVE';
export function userManagementGetUsersReceive(count, users) {
  return {
    type: USER_MANAGEMENT_GET_USERS_RECEIVE,
    count,
    users
  };
}

export const USER_MANAGEMENT_GET_USERS_FAIL = 'USER_MANAGEMENT_GET_USERS_FAIL';
export function userManagementGetUsersFail(error) {
  return {
    type: USER_MANAGEMENT_GET_USERS_FAIL,
    error
  };
}

export const USER_MANAGEMEN_INVITE_DIALOG_TOGGLE = 'USER_MANAGEMEN_INVITE_DIALOG_TOGGLE';
export function userManagementInviteDialogToggle(isOpened) {
  return {
    type: USER_MANAGEMEN_INVITE_DIALOG_TOGGLE,
    isOpened
  };
}

export const USER_MANAGEMENT_CREATE_USER_REQUEST = 'USER_MANAGEMENT_CREATE_USER_REQUEST';
export function userManagementCreateUserRequest() {
  return {
    type: USER_MANAGEMENT_CREATE_USER_REQUEST
  };
}

export const USER_MANAGEMENT_CREATE_USER_RECEIVE = 'USER_MANAGEMENT_CREATE_USER_RECEIVE';
export function userManagementCreateUserReceive(user) {
  return {
    type: USER_MANAGEMENT_CREATE_USER_RECEIVE,
    user
  };
}

export const USER_MANAGEMENT_CREATE_USER_FAIL = 'USER_MANAGEMENT_CREATE_USER_FAIL';
export function userManagementCreateUserFail(error) {
  return {
    type: USER_MANAGEMENT_CREATE_USER_FAIL,
    error
  };
}

export const USER_MANAGEMENT_DELETE_USER_REQUEST = 'USER_MANAGEMENT_DELETE_USER_REQUEST';
export function userManagementDeleteUSerRequest() {
  return {
    type: USER_MANAGEMENT_DELETE_USER_REQUEST
  };
}

export const USER_MANAGEMENT_DELETE_USER_RECEIVE = 'USER_MANAGEMENT_DELETE_USER_RECEIVE';
export function userManagementDeleteUserReceive() {
  return {
    type: USER_MANAGEMENT_DELETE_USER_RECEIVE
  };
}

export const USER_MANAGEMENT_DELETE_USER_FAIL = 'USER_MANAGEMENT_DELETE_USER_FAIL';
export function userManagementDeleteUserFail(error) {
  return {
    type: USER_MANAGEMENT_DELETE_USER_FAIL,
    error
  };
}

export const USER_MANAGEMEN_INVITE_USER_REQUEST = 'USER_MANAGEMEN_INVITE_USER_REQUEST';
export function userManagementInviteUserRequest() {
  return {
    type: USER_MANAGEMEN_INVITE_USER_REQUEST
  };
}

export const USER_MANAGEMEN_INVITE_USER_FAIL = 'USER_MANAGEMEN_INVITE_USER_FAIL';
export function userManagementInviteUserFail(error) {
  return {
    type: USER_MANAGEMEN_INVITE_USER_FAIL,
    error
  };
}

export const USER_MANAGEMEN_INVITE_USER_SUCCESS = 'USER_MANAGEMEN_INVITE_USER_SUCCESS';
export function userManagementInviteUserSuccess() {
  return {
    type: USER_MANAGEMEN_INVITE_USER_SUCCESS
  };
}


export const userManagementGetUsers = (limit, offset, sortField, sortOrder) => (dispatch) => {
  dispatch(userManagementGetUsersRequest());
  const query = {limit, offset};
  if(sortField) { query[sortField] = sortOrder; }
  return UserService.getUsersList(query)
    .then((result) => {
      const {count, users} = result;
      dispatch(userManagementGetUsersReceive(count, users));
    })
    .catch((err) => {
      dispatch(userManagementGetUsersFail(err));
    });
};

export const userManagementCreateUser = (userData, dispatch) => {
  dispatch(userManagementCreateUserRequest());
  return UserService.createUser(userData)
    .then((response) => {
      dispatch(userManagementCreateUserReceive(response));
    })
    .catch((response) => {
      const {errors} = response;
      const reduxError = {};
      errors && errors.forEach((error) => {
        reduxError[error.path] = error.message;
      });

      dispatch(userManagementCreateUserFail(response));
      throw new SubmissionError(reduxError);
    });
};

export const userManagementDeleteUser = (id) => (dispatch) => {
  dispatch(userManagementDeleteUSerRequest());
  return UserService.deleteUser(id)
    .then(() => {
      dispatch(userManagementDeleteUserReceive());
      dispatch(userManagementGetUsers());
    })
    .catch((response) => {
      dispatch(userManagementCreateUserFail(response));
    });
};

export const userManagementInviteSend = (id) => (dispatch) => {
  dispatch(userManagementInviteUserRequest());

  return UserService.resendInvite(id)
    .then(() => {
      dispatch(userManagementInviteUserSuccess());
    })
    .catch(error => {
      dispatch(userManagementInviteUserFail(error));
    });
};
