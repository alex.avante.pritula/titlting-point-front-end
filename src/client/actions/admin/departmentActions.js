import DepartmentsService from '../../services/Departments';

export const DEPARTMENTS_GET_LIST_REQUEST = 'DEPARTMENTS_GET_LIST_REQUEST';
export function departmensGetListRequest() {
  return {
    type: DEPARTMENTS_GET_LIST_REQUEST
  };
}

export const DEPARTMENTS_GET_LIST_RECEIVE = 'DEPARTMENTS_GET_LIST_RECEIVE';
export function departmentsGetListReceive(departments, count) {
  return {
    type: DEPARTMENTS_GET_LIST_RECEIVE,
    departments,
    count
  };
}
export const DEPARTMENTS_GET_LIST_FAIL = 'DEPARTMENTS_GET_LIST_FAIL';
export function departmentsGetListFail(error) {
  return {
    type: DEPARTMENTS_GET_LIST_FAIL,
    error
  };
}

export const getDepartments = () => (dispatch) => {
  dispatch(departmensGetListRequest());
  return DepartmentsService.getAllDepartments()
    .then(({data, total}) => {
      dispatch(departmentsGetListReceive(data, total));
    })
    .catch((error) => {
      dispatch(departmentsGetListFail(error));
    });
};
