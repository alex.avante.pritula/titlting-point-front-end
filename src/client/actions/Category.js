import { 
  GET_ALL_CATEGORIES_SUCCESS,
  GET_ALL_CATEGORIES_START, 
  GET_ALL_CATEGORIES_FAIL 
} from '../constants/Category';

import Category from '../services/Category';

export function getAllCategories(){
    return (dispatch) => {
      dispatch({ type: GET_ALL_CATEGORIES_START });
      return Category.getAllCategories()
          .then((categories)=>{
             dispatch({ type: GET_ALL_CATEGORIES_SUCCESS, categories });
          }).catch((error)=>{
             dispatch({ type: GET_ALL_CATEGORIES_FAIL, error });
          });
    };
}

