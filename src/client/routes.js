import React from 'react';
import {
  Route,
  IndexRedirect,
  browserHistory
} from 'react-router';

import App from './components/App/App';
import UserService from './services/User';
import LogIn from './components/User/LogIn';
import Profile from './components/User/Profile';
import PasswordRecovery from './components/User/PasswordRecovery';
import PasswordReset from './components/User/PasswordReset';
import Dashboard from './components/Dashboard/Dashboard';
import GameGallery from './components/Game/GameGallery';
import Registration from './components/User/Registration';
import UsersManagementComponent from './components/admin/users/UsersManagementComponent';
import NotFoundPageComponent from './components/App/NotFoundPageComponent';
import UserDirectory from './components/User/UserDirectory';

import {
  LOGIN_PAGE,
  START_PAGE,
  PASSWORD_RECOVERY_PAGE,
  DEFAULT_DASHBOARD_PAGE
} from './constants/General';

function requireAuthMiddleware(nextState, replace) {
    const pathname = window.location.pathname;
    const isLogin = (pathname == LOGIN_PAGE);
    const isRecovery = (pathname == PASSWORD_RECOVERY_PAGE);
    UserService.getToken().then(console.warn);
    UserService.isAuthenticated()
    .then((isAuth) => {
        if(!isAuth && (!isLogin && !isRecovery)) {
          browserHistory.push(LOGIN_PAGE);
        }

        if(isAuth && (isLogin || isRecovery)){
          browserHistory.push(START_PAGE);
        }
    }).catch(() => {
      replace({ pathname: LOGIN_PAGE, query: { return_to: nextState.location.pathname }});
    });
}

export default (
  <Route path={START_PAGE} component={App}>
    <IndexRedirect to={`dashboard/${DEFAULT_DASHBOARD_PAGE}`} />
    <Route path={LOGIN_PAGE} component={LogIn} />
    <Route path={'password-recovery'} component={PasswordRecovery} />
    <Route path={'recovery/:id'} component={PasswordReset} />
    <Route path={'registration/:id'} component={Registration} />
    <Route path={'dashboard/:id'} params={{ id: DEFAULT_DASHBOARD_PAGE }}  component={Dashboard} onEnter={requireAuthMiddleware} />
    <Route path={'creatives'} component={GameGallery} onEnter={requireAuthMiddleware}/>
    <Route path={'profile'} component={Profile} onEnter={requireAuthMiddleware} />
    <Route path={'admin'} onEnter={requireAuthMiddleware}>
      <Route path={'users'} component={UsersManagementComponent} onEnter={requireAuthMiddleware}/>
      <Route path={'users/:id'} component={Profile} onEnter={requireAuthMiddleware}/>
    </Route>
    <Route path={'user-directory'} component={UserDirectory} onEnter={requireAuthMiddleware} />
    <Route path={'*'} component={NotFoundPageComponent} />
  </Route>
);
