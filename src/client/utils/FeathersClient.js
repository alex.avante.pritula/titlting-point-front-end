import feathers from 'feathers/client';
import rest from 'feathers-rest/client';
import superagent from 'superagent';
import hooks from 'feathers-hooks';
import auth from 'feathers-authentication-client';
import { API_PREFIX } from '../constants/General';

const feathersClient = feathers();

feathersClient.configure(hooks())
  .configure(rest().superagent(superagent))
  .configure(auth({ storage: window.localStorage }));


function authHook(hook) {
  let index = (`/${hook.path}`).indexOf(API_PREFIX);
  if(index != -1){
    feathersClient.passport.getJWT()
      .then((accessToken)=>{
          hook.params.headers = Object.assign({}, {
            Authorization: accessToken
          }, hook.params.headers);
      });
  }
}

feathersClient.mixins.push((service) => {
  service.before(authHook);
});

export default feathersClient;
