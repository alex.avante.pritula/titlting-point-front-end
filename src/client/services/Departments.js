import FeathersClient from '../utils/FeathersClient';
import { API_PREFIX } from '../constants/General';

class DepartmentsService {
  constructor(){
    this.service = FeathersClient.service(`${API_PREFIX}/departments`);
  }

  getAllDepartments() {
    return this.service.find();
  }
}

export default new DepartmentsService();
