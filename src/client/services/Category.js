import FeathersClient from '../utils/FeathersClient';
import { API_PREFIX } from '../constants/General';

class CategoryService {
    constructor(){
        this.service = FeathersClient.service(`${API_PREFIX}categories`);
    }

    getAllCategories(){
        return this.service.find();
    }
}

export default new CategoryService();