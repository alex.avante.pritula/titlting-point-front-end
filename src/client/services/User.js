import FeathersClient from '../utils/FeathersClient';
import { API_PREFIX } from '../constants/General';

const UPLOAD_HEADERS = {
    'Content-Type': undefined,
    'Accept': '*/*'
};

class UserService {
    constructor(){
        this.service = {
            user: FeathersClient.service(`${API_PREFIX}users`),
            currentUser:  FeathersClient.service(`${API_PREFIX}users/current`),
            getProfileByCode: FeathersClient.service(`${API_PREFIX}users/code`),
            updateUserAvatar: FeathersClient.service(`${API_PREFIX}users/avatar`),
            completeRegistration: FeathersClient.service(`${API_PREFIX}users/code`),
            resendInvitation: FeathersClient.service(`${API_PREFIX}users/:id/resend`),
            passwordRecovery: FeathersClient.service(`${API_PREFIX}users/password/reset`),
            updatePassword: FeathersClient.service(`${API_PREFIX}users/profile/password`)
        };
    }

    logIn(email, password){
      return FeathersClient.authenticate({
          strategy: 'local',
          email: email,
          password: password
      });
    }

    logOut(){
      document.cookie = 'feathers-jwt=;';
      return FeathersClient.logout();
    }

    isAuthenticated(){
      return FeathersClient.passport.getJWT()
      .then((accessToken)=>{
          return (!!accessToken);
      });
    }

    getToken(){
      return FeathersClient.passport.getJWT();
    }

    getCurrentUserProfile(){
      const {currentUser} = this.service;
      return currentUser.find();
    }

     getUserProfile(userId){
       const {user} = this.service;
       return user.find(userId);
    }

    createUser(userData) {
      const {user} = this.service;
      return user.create(userData);
    }

    deleteUser(id) {
      const {user} = this.service;
      return user.remove(id);
    }

    /**
     * Used for user password recovery
     * @param email - string
     */
    recoverPassword(email) {
      const {passwordRecovery} = this.service;
      return passwordRecovery.create(email);
    }

    /**
     * Used for assign new password to user
     * @param token - string
     * @param newPassword - string
     */
    resetPassword(resetToken, password) {
      const { passwordRecovery } = this.service;
      return passwordRecovery.patch(null, {resetToken, password});
    }

    /**
      * Returns user profile by registration token code
      * @param code - string
      */
    retrieveUserProfileByCode(code) {
      const { getProfileByCode } = this.service;
      return getProfileByCode.get(code);
    }

    /**
     * Resends invite to user
     * //TODO: Problem with parameters replace in url!
     */
    resendInvite(id) {
      const {resendInvitation} = this.service;

      /** TODO: Bug in feathers.js, we canno't replace parameter in url when we have a route like that: '/user/:id/resend'. Wait for resolve */

      const regex = /(:id)/g;
      resendInvitation.base = resendInvitation.base.replace(regex, id);

      return resendInvitation.patch(null, {});
    }

    completeRegistration(code, profile) {
      const { completeRegistration } = this.service;
      return completeRegistration.patch(code, profile);
    }

    updateCurrentUser(data){
      const {currentUser} = this.service;
      return currentUser.patch(null, data);
    }
    
    updateUser(id, data){
      const {user} = this.service;
      return user.patch(id, data);
    }

    changeAvatar(userId, file){
      const {updateUserAvatar} = this.service;
      const formData = new FormData();
      formData.append('files', file);
      return updateUserAvatar.patch(userId, formData, {
        headers: UPLOAD_HEADERS
      });
    }

    /**
     * Uploads avatar to server
     * @param {*} file
     * @param {*} token
     */
    uploadAvatar(file, token) {
      const {updateUserAvatar} = this.service;

      const formData = new FormData();
      formData.append('files', file);

      const headers = {
        'Authorization': token,
        'Content-Type': undefined,
        'Accept': '*/*'
      };

      return updateUserAvatar.patch(null, formData, {headers});
    }

    getUsersList(query){
       const {user} = this.service;
       return user.find({query});
    }

    getSelectedUserProfile(id){
      const {user} = this.service;
      return user.get(id);
    }

    updateCurrentUserPassword(data){
        const {updatePassword} = this.service;
        return updatePassword.patch(null, data);
    }

}

export default new UserService();
