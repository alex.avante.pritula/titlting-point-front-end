import FeathersClient from '../utils/FeathersClient';
import { API_PREFIX } from '../constants/General';

const UPLOAD_HEADERS = {
    'Content-Type': undefined,
    'Accept': '*/*'
};

class GameService {
    constructor(){
        this.services = {
           game: FeathersClient.service(`${API_PREFIX}games`),
           genre: FeathersClient.service(`${API_PREFIX}genres`),
           gameImage: FeathersClient.service(`${API_PREFIX}games/images`),
           gameGenre: FeathersClient.service(`${API_PREFIX}/games/genres`),
           similarGame: FeathersClient.service(`${API_PREFIX}games/{gameId}/similar`)
        };
    }

    addGameGenres(data){
        const {gameGenre} = this.services;
        return gameGenre.create(data);
    }

    removeGameGenres(params){
        const {gameGenre} = this.services;
        return gameGenre.remove(null, params);
    }

    getAllGenres(){
        const {genre} = this.services;
        return genre.find();
    }

    getGameById(id) {
        const {game} = this.services;
        return game.get(id);
    }

    updateSimilarGames(id, data){
        const {similarGame} = this.services;
        const regex = /(:gameId)/g;
        similarGame.base = similarGame.base.replace(regex, id);
        return similarGame.patch(null,data);
    }

    deleteSimilarGames(id, ids){
        const {similarGame} = this.services;
        const regex = /(:gameId)/g;
        similarGame.base = similarGame.base.replace(regex, id);
        return similarGame.remove(null,{query:{ids}});
    }

    getGames(query){
        const {game} = this.services;
        return game.find({query});
    }

    createGame(data){
        const {game} = this.services;
        return game.create(data);
    }

    removeGame(id){
        const {game} = this.services;
        return game.remove(id);
    }

    createGameImage(form){
        const {gameImage} = this.services;
        return gameImage.create(form, {headers: UPLOAD_HEADERS});   
    }

    removeGenres(id, query){
        const {genre} = this.services;
        return genre.remove(id,{query});
    }

    getGenres(query){
        const {genre} = this.services; 
        return genre.find({query});
    }

    createGenres(data){
        const {genre} = this.services; 
        return genre.create(data);
    }

    updateGame(id, data){
        const {game} = this.services;
        return game.patch(id, data);
    }
}

export default new GameService();