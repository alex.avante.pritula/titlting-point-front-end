import { API_PREFIX } from '../constants/General';
import FeathersClient from '../utils/FeathersClient';

const UPLOAD_HEADERS = {
    'Content-Type': undefined,
    'Accept': '*/*'
};

class DashboardService {
    constructor(){
        this.services = {
           news: FeathersClient.service(`${API_PREFIX}news`),
           item: FeathersClient.service(`${API_PREFIX}items`),
           block: FeathersClient.service(`${API_PREFIX}blocks`),
           defaultLink: FeathersClient.service(`${API_PREFIX}links`),
           dashboard: FeathersClient.service(`${API_PREFIX}dashboards`),
           videoLink: FeathersClient.service(`${API_PREFIX}links/video`),
           listOfLinks: FeathersClient.service(`${API_PREFIX}links/base`),
           newsImages: FeathersClient.service(`${API_PREFIX}news-images`),
           documentLink: FeathersClient.service(`${API_PREFIX}links/docs`),
           itemPosition: FeathersClient.service(`${API_PREFIX}items/position`),
           blockPosition: FeathersClient.service(`${API_PREFIX}blocks/position`)
        };
    }

    getDashboard(id,query){
       const {dashboard} = this.services;
       return dashboard.get(id,{query});
    }

    removeBlock(id){
       const {block} = this.services;
       return block.remove(id);
    }

    addBlock(id, data){
       const {block} = this.services;
       return block.create(id, data);
    }
    
    changeBlockPosition(id, data){
       const {blockPosition} = this.services;
       return blockPosition.patch(id, data);
    }

    changeItemPosition(id, data){
       const {itemPosition} = this.services;
       return itemPosition.patch(id, data);
    }

    updateBlock(id, data){
       const {block} = this.services;
       return block.patch(id, data);
    }

    getItemDescription(id){
       const {item} = this.services;
       return item.get(id);
    }

    createDefaultLink(data){
        const {defaultLink} = this.services;
        return defaultLink.create(data);
    }   

    createDocumentLink(form){
        const {documentLink} = this.services;
        return documentLink.create(form, {headers: UPLOAD_HEADERS});
    }

    createVideoLink(form){
        const {videoLink} = this.services;
        return videoLink.create(form, {headers: UPLOAD_HEADERS});
    }

    createNews(data){
        const {news} = this.services;
        return news.create(data);
    }

    createNewsImages(form){
        const {newsImages} = this.services;
        return newsImages.create(form, {headers: UPLOAD_HEADERS});   
    }

    removeItem(id){
        const {item} = this.services;
        return item.remove(id);
    }

    createListOfLinks(data){
        const {listOfLinks} = this.services;
        return listOfLinks.create(data);
    }

    updateItemName(id, data){
        const {item} = this.services;
        return item.patch(id,data);
    }

    updateDefaultLink(id, data){
        const {defaultLink} = this.services;
        return defaultLink.patch(id,data);
    }

    updateNews(id, data){
        const {news} = this.services;
        return news.patch(id,data);
    }

    removeNewsImage(id){
        const {newsImages} = this.services;
        return newsImages.remove(id);
    }

    updateListOfLinks(data){
        const {listOfLinks} = this.services;
        return listOfLinks.patch(null, data);
    }

    removeListOfLinks(ids){
        const {listOfLinks} = this.services;
        return listOfLinks.remove(null,{query: {ids}});
    }

    search(query){
        const {items} = this.search;
        return items.find({query});
    }
}

export default new DashboardService();