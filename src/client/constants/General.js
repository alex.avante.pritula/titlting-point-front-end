export const ENTER = 13;
export const LIMIT = 10;

export const SORT_TYPES = {
  desc: 'Newest-Oldest',
  asc:  'Oldest-Newest'
};

export const VIEW_LIST = 'VIEW_LIST';
export const VIEW_GRID = 'VIEW_GRID';
export const UPLOAD_TYPES = {
  ALL: { accept: '*' },
  IMAGE: { accept: 'image/*' },
  VIDEO: { accept: 'video/*' },
  NEW_DOCUMENT: { accept: null }
};

export const ROLE_TITLES = {};
export const MOBILE_WIDTH = 864;
export const TOASTER_DELAY = 10000;
export const DEFAULT_DASHBOARD_PAGE = 1;
export const UPLOAD_MAX_SIZE = 52428800;

export const API_PREFIX = '/api/v1/';
export const PASSWORD_RECOVERY_PAGE = '/password-recovery';
export const LOGIN_PAGE = '/login';
export const START_PAGE = '/';

export const VALIDATION = {
    EMAIL: /\S+@\S+\.\S+/,
    PASSWORD: /^((?=\S*?[A-Z])(?=\S*?[a-z])(?=\S*?[0-9]).{6,})\S$/,
    TOKEN: /^\w{8}\-\w{4}\-\w{4}\-\w{4}\-\w{12}$/
};

export const DASHBOARD_BLOCK_TYPES = {
   DEFAULT: 1,
   EVENT: 2
};
export const SORTING_FIELDS = {
  ROLE: 'sortRoleId',
  DATE: 'sortCreatedAt',
  STATUS: 'sortIsVerified'
};

export const DASHBOARD_BLOCK_ITEM_TYPES = {
    NEWS_BLOCK: {
       id: 1,
       name: 'Add News Block',
       iconActive: 'ic-news-active',
       iconDefault: 'ic-news-inactive',
       preview: 'default-preview news'
    },
    NEW_DOCUMENT: {
       id: 2,
       name: 'Add New Document',
       iconActive: 'ic-image-link-active',
       iconDefault: 'ic-image-link-inactive',
       preview: {
         png: 'ic-file-png',
         jpeg: 'ic-file-jpg',
         jpg: 'ic-file-jpg',
         psd: 'ic-file-psd',
         ppt: 'ic-file-ppt',
         pptx: 'ic-file-ppt',
         default: 'ic-file-new'
       }
    },
    TEXTAREA: {
       id: 3,
       name: 'Textarea',
       iconActive: 'ic-textarea-active',
       iconDefault: 'ic-textarea-inactive',
       preview: 'default-preview textarea'
    },
    LIST_OF_LINKS: {
       id: 4,
       name: 'List of Links',
       iconActive: 'ic-list-of-links-active',
       iconDefault: 'ic-list-of-links-inactive',
       preview: 'default-preview listOflinks'
    },
    VIDEO: {
       id: 5,
       name: 'Video Embed',
       iconActive: 'ic-video-active',
       iconDefault: 'ic-video-inactive'
    },
    IMAGE_LINK: {
       id: 6,
       name: 'Image Link',
       iconActive: 'ic-image-link-active',
       iconDefault: 'ic-image-link-inactive'
    }
};

export const USER_ROLES = [{
    id: 1, 
    title: 'Super Admin'
  },{
    id: 2, 
    title: 'Admin'
  },{
    id: 3, 
    title: 'Editor'
  },{
    id: 4, 
    title: 'User'
}];

export const TILTING_POINT_AWS_URL = 'https://tiltingpoint.s3.amazonaws.com/';
