export const DEPARTMENTS_LIST = [{
  id: 1,
  name: "User Acquisition"
},{
  id: 2,
  name: "Marketing"
},{
  id: 3,
  name: "Design & Analysis"
},{
  id: 4,
  name: "Executive"
},{
  id: 5,
  name: "Creative Services"
},{
  id: 6,
  name: "Business Development"
},{
  id: 7,
  name: "Talent"
},{
  id: 8,
  name: "Live Operations"
},{
  id: 9,
  name: "Product Support"
}];
