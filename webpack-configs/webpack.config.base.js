// Common Webpack configuration used by webpack.config.development and webpack.config.production

const path = require('path');
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');

module.exports = {
  output: {
    filename: 'js/[name].js',
    path: path.resolve(__dirname, '../dist'),
    publicPath: '/'
  },
  resolve: {
    modules: [
      path.join(__dirname, '../src'),
      path.join(__dirname, '../src/resources/assets'),
      path.join(__dirname, '../src/scripts'),
      'node_modules'
    ],
    extensions: ['.js', '.jsx', '.json', '.scss'],
    alias: { moment: 'moment/moment.js'}
  },
  plugins: [
    new webpack.ProvidePlugin({
      'fetch': 'imports?this=>global!exports?global.fetch!whatwg-fetch'  // fetch API
    }),
    // Shared code
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      filename: 'js/vendor.bundle.js',
      minChunks: Infinity
    })
  ],
  module: {
    loaders: [
      // JavaScript / ES6
      {
        test: /\.jsx?$/,
        include: path.resolve(__dirname, '../src/client'),
        loader: 'babel'
      },
      // Images
      // Inline base64 URLs for <=8k images, direct URLs for the rest
    // {
    //   test: /\.(png|jpg|jpeg|gif|svg|ico)$/i,
    //   loader: 'file-loader',
    //   options: {
    //     name: '[name]-[hash:8].[ext]',
    //     publicPath: '/images/',
    //     outputPath: '/images/'
    //   }
    // },
      {
        test: /\.(png|jpg|jpeg|gif|svg)$/,
        loader: 'file'
      },
      {
        test: /\.svg$/,
        loader: 'svg-inline-loader'
      },
      { 
        test: /\\.scss$/, 
        use: ['style-loader', 'css-loader', 'scss-loader'] 
      },
      {
        test: /favicon\.ico$/,
        loader: 'url',
        query: {
          limit: 1,
          name: '[name].[ext]',
        },
      },
      // Fonts
      {
        test: /\.(woff|woff2|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url',
        query: {
          limit: 8192,
          name: 'fonts/[name].[ext]?[hash]'
        }
      }
    ]
  },
  postcss: function () {
    return [
      autoprefixer({
        browsers: ['last 2 versions']
      })
    ];
  }
};
